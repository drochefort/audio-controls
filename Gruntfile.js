module.exports = function(grunt) {
  require('load-grunt-tasks')(grunt);

  grunt.initConfig({
    eslint: {
      target: ['src/**/*.js']
    },
    'jsbeautifier': {
      files: ["src/**/*.js"],
      options: {
        js: {
          indentWithTabs: true,
          indentSize: 1
        }
      }
    },
    browserify: {
      debug: {
        files: {
          'dist/hmh-widget-audio-controls.js': ['src/index.js']
        },
        options: {
          transform: ['babelify'],
          watch: true,
          browserifyOptions: {
            debug: true
          }
        }
      },
      release: {
        files: {
          'dist/hmh-widget-audio-controls.js': ['src/index.js']
        },
        options: {
          transform: ['babelify']
        }
      }
    },
    uglify: {
      app: {
        files: {
          'dist/hmh-widget-audio-controls.min.js': ['dist/hmh-widget-audio-controls.js']
        }
      }
    },
    sass: {
      options: {
        sourceMap: true
      },
      dist: {
        files: {
          'playground/css/audio.css': 'sass/audio.scss'
        }
      }
    },
    watch: {
      app: {
        options: {
          livereload: true
        },
        tasks: ['jsbeautifier', 'sass'],
        files: [
          'dist/*.js',
          'playground/*.html',
          'playground/js/*.js',
          'playground/sass/*.scss'
        ]
      }
    },
    connect: {
      app: {
        options: {
          port: 9500,
          livereload: true,
          base: ['playground', '.']
        }
      }
    },
    open: {
      app: {
        path: 'http://localhost:9500'
      }
    }
  });

  // Update the common library
  grunt.registerTask('debug', ['jsbeautifier', 'eslint', 'browserify:debug', 'sass'])
  grunt.registerTask('release', ['eslint', 'browserify:release', 'uglify'])
  grunt.registerTask('serve', ['connect', 'open', 'watch']);
  grunt.registerTask('build', ['release']);
  grunt.registerTask('default', ['debug', 'serve']);
}