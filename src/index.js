/* global WidgetsCommon */

// The global audio state object
const state = {
	// The path of audio file to play
	file: '',
	// Disable audio controls altogether
	disabled: false,
};

class AudioControls {
	constructor(widgetsCommon, filename = '') {
		this.common = widgetsCommon;
		this.node = document.querySelector('nav.audio-controls');

		state.file = filename;

		if (this.node) {
			this.create();
		}
	}

	get audioInterface() {
		return this.common.audio;
	}

	get emitter() {
		return this.common.emitter;
	}

	get audio() {
		return state.file;
	}

	set audio(filename) {
		state.file = filename;
		if (this.audioInterface.isPaused()) {
			this.load();
		} else {
			this.play();
		}
	}

	get disabled() {
		return state.disabled;
	}

	set disabled(value) {
		if (value === true) {
			this.pause();
		}
		state.disabled = value;
		this.update();
	}

	create() {
		this.addButton('rewind', () => this.rewind());
		this.addButton('play', () => this.play());
		this.addButton('pause', () => this.pause());

		this.emitter.on('audio:stops', () => this.update());
		this.emitter.on('audio:set', (filename) => this.audio = filename);
	}

	addButton(className, clickHandler) {
		const button = document.createElement('button');
		button.classList.add(className);
		this.node.appendChild(button);

		if (typeof clickHandler === 'function') {
			button.addEventListener('click', clickHandler);
		}
	}

	update() {
		if (state.disabled) {
			this.node.classList.add('disabled');
			this.node.classList.remove('playing');
		} else {
			this.node.classList.remove('disabled');
			if (this.audioInterface.isPaused()) {
				this.node.classList.remove('playing');
			} else {
				this.node.classList.add('playing');
			}
		}
	}

	load() {
		this.audioInterface.load(state.file);
		this.update();
	}

	rewind() {
		if (!state.disabled) {
			this.audioInterface.seek(0);
		}
		this.update();
	}

	play() {
		if (!state.disabled) {
			this.audioInterface.play(this.audio);
		}
		this.update();
	}

	pause() {
		this.audioInterface.pause();
		this.update();
	}
}

window.AudioControls = AudioControls;
