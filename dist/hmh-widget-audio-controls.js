(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

var _createClass = function () { function defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } } return function (Constructor, protoProps, staticProps) { if (protoProps) defineProperties(Constructor.prototype, protoProps); if (staticProps) defineProperties(Constructor, staticProps); return Constructor; }; }();

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

/* global WidgetsCommon */

// The global audio state object
var state = {
	// The path of audio file to play
	file: '',
	// Disable audio controls altogether
	disabled: false
};

var AudioControls = function () {
	function AudioControls(widgetsCommon) {
		var filename = arguments.length <= 1 || arguments[1] === undefined ? '' : arguments[1];

		_classCallCheck(this, AudioControls);

		this.common = widgetsCommon;
		this.node = document.querySelector('nav.audio-controls');

		state.file = filename;

		if (this.node) {
			this.create();
		}
	}

	_createClass(AudioControls, [{
		key: 'create',
		value: function create() {
			var _this = this;

			this.addButton('rewind', function () {
				return _this.rewind();
			});
			this.addButton('play', function () {
				return _this.play();
			});
			this.addButton('pause', function () {
				return _this.pause();
			});

			this.emitter.on('audio:stops', function () {
				return _this.update();
			});
			this.emitter.on('audio:set', function (filename) {
				return _this.audio = filename;
			});
		}
	}, {
		key: 'addButton',
		value: function addButton(className, clickHandler) {
			var button = document.createElement('button');
			button.classList.add(className);
			this.node.appendChild(button);

			if (typeof clickHandler === 'function') {
				button.addEventListener('click', clickHandler);
			}
		}
	}, {
		key: 'update',
		value: function update() {
			if (state.disabled) {
				this.node.classList.add('disabled');
				this.node.classList.remove('playing');
			} else {
				this.node.classList.remove('disabled');
				if (this.audioInterface.isPaused()) {
					this.node.classList.remove('playing');
				} else {
					this.node.classList.add('playing');
				}
			}
		}
	}, {
		key: 'load',
		value: function load() {
			this.audioInterface.load(state.file);
			this.update();
		}
	}, {
		key: 'rewind',
		value: function rewind() {
			if (!state.disabled) {
				this.audioInterface.seek(0);
			}
			this.update();
		}
	}, {
		key: 'play',
		value: function play() {
			if (!state.disabled) {
				this.audioInterface.play(this.audio);
			}
			this.update();
		}
	}, {
		key: 'pause',
		value: function pause() {
			this.audioInterface.pause();
			this.update();
		}
	}, {
		key: 'audioInterface',
		get: function get() {
			return this.common.audio;
		}
	}, {
		key: 'emitter',
		get: function get() {
			return this.common.emitter;
		}
	}, {
		key: 'audio',
		get: function get() {
			return state.file;
		},
		set: function set(filename) {
			state.file = filename;
			if (this.audioInterface.isPaused()) {
				this.load();
			} else {
				this.play();
			}
		}
	}, {
		key: 'disabled',
		get: function get() {
			return state.disabled;
		},
		set: function set(value) {
			if (value === true) {
				this.pause();
			}
			state.disabled = value;
			this.update();
		}
	}]);

	return AudioControls;
}();

window.AudioControls = AudioControls;

},{}]},{},[1])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJzcmMvaW5kZXguanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7Ozs7Ozs7Ozs7QUNHQSxJQUFNLFFBQVE7O0FBRWIsT0FBTSxFQUFOOztBQUVBLFdBQVUsS0FBVjtDQUpLOztJQU9BO0FBQ0wsVUFESyxhQUNMLENBQVksYUFBWixFQUEwQztNQUFmLGlFQUFXLGtCQUFJOzt3QkFEckMsZUFDcUM7O0FBQ3pDLE9BQUssTUFBTCxHQUFjLGFBQWQsQ0FEeUM7QUFFekMsT0FBSyxJQUFMLEdBQVksU0FBUyxhQUFULENBQXVCLG9CQUF2QixDQUFaLENBRnlDOztBQUl6QyxRQUFNLElBQU4sR0FBYSxRQUFiLENBSnlDOztBQU16QyxNQUFJLEtBQUssSUFBTCxFQUFXO0FBQ2QsUUFBSyxNQUFMLEdBRGM7R0FBZjtFQU5EOztjQURLOzsyQkE2Q0k7OztBQUNSLFFBQUssU0FBTCxDQUFlLFFBQWYsRUFBeUI7V0FBTSxNQUFLLE1BQUw7SUFBTixDQUF6QixDQURRO0FBRVIsUUFBSyxTQUFMLENBQWUsTUFBZixFQUF1QjtXQUFNLE1BQUssSUFBTDtJQUFOLENBQXZCLENBRlE7QUFHUixRQUFLLFNBQUwsQ0FBZSxPQUFmLEVBQXdCO1dBQU0sTUFBSyxLQUFMO0lBQU4sQ0FBeEIsQ0FIUTs7QUFLUixRQUFLLE9BQUwsQ0FBYSxFQUFiLENBQWdCLGFBQWhCLEVBQStCO1dBQU0sTUFBSyxNQUFMO0lBQU4sQ0FBL0IsQ0FMUTtBQU1SLFFBQUssT0FBTCxDQUFhLEVBQWIsQ0FBZ0IsV0FBaEIsRUFBNkIsVUFBQyxRQUFEO1dBQWMsTUFBSyxLQUFMLEdBQWEsUUFBYjtJQUFkLENBQTdCLENBTlE7Ozs7NEJBU0MsV0FBVyxjQUFjO0FBQ2xDLE9BQU0sU0FBUyxTQUFTLGFBQVQsQ0FBdUIsUUFBdkIsQ0FBVCxDQUQ0QjtBQUVsQyxVQUFPLFNBQVAsQ0FBaUIsR0FBakIsQ0FBcUIsU0FBckIsRUFGa0M7QUFHbEMsUUFBSyxJQUFMLENBQVUsV0FBVixDQUFzQixNQUF0QixFQUhrQzs7QUFLbEMsT0FBSSxPQUFPLFlBQVAsS0FBd0IsVUFBeEIsRUFBb0M7QUFDdkMsV0FBTyxnQkFBUCxDQUF3QixPQUF4QixFQUFpQyxZQUFqQyxFQUR1QztJQUF4Qzs7OzsyQkFLUTtBQUNSLE9BQUksTUFBTSxRQUFOLEVBQWdCO0FBQ25CLFNBQUssSUFBTCxDQUFVLFNBQVYsQ0FBb0IsR0FBcEIsQ0FBd0IsVUFBeEIsRUFEbUI7QUFFbkIsU0FBSyxJQUFMLENBQVUsU0FBVixDQUFvQixNQUFwQixDQUEyQixTQUEzQixFQUZtQjtJQUFwQixNQUdPO0FBQ04sU0FBSyxJQUFMLENBQVUsU0FBVixDQUFvQixNQUFwQixDQUEyQixVQUEzQixFQURNO0FBRU4sUUFBSSxLQUFLLGNBQUwsQ0FBb0IsUUFBcEIsRUFBSixFQUFvQztBQUNuQyxVQUFLLElBQUwsQ0FBVSxTQUFWLENBQW9CLE1BQXBCLENBQTJCLFNBQTNCLEVBRG1DO0tBQXBDLE1BRU87QUFDTixVQUFLLElBQUwsQ0FBVSxTQUFWLENBQW9CLEdBQXBCLENBQXdCLFNBQXhCLEVBRE07S0FGUDtJQUxEOzs7O3lCQWFNO0FBQ04sUUFBSyxjQUFMLENBQW9CLElBQXBCLENBQXlCLE1BQU0sSUFBTixDQUF6QixDQURNO0FBRU4sUUFBSyxNQUFMLEdBRk07Ozs7MkJBS0U7QUFDUixPQUFJLENBQUMsTUFBTSxRQUFOLEVBQWdCO0FBQ3BCLFNBQUssY0FBTCxDQUFvQixJQUFwQixDQUF5QixDQUF6QixFQURvQjtJQUFyQjtBQUdBLFFBQUssTUFBTCxHQUpROzs7O3lCQU9GO0FBQ04sT0FBSSxDQUFDLE1BQU0sUUFBTixFQUFnQjtBQUNwQixTQUFLLGNBQUwsQ0FBb0IsSUFBcEIsQ0FBeUIsS0FBSyxLQUFMLENBQXpCLENBRG9CO0lBQXJCO0FBR0EsUUFBSyxNQUFMLEdBSk07Ozs7MEJBT0M7QUFDUCxRQUFLLGNBQUwsQ0FBb0IsS0FBcEIsR0FETztBQUVQLFFBQUssTUFBTCxHQUZPOzs7O3NCQXJGYTtBQUNwQixVQUFPLEtBQUssTUFBTCxDQUFZLEtBQVosQ0FEYTs7OztzQkFJUDtBQUNiLFVBQU8sS0FBSyxNQUFMLENBQVksT0FBWixDQURNOzs7O3NCQUlGO0FBQ1gsVUFBTyxNQUFNLElBQU4sQ0FESTs7b0JBSUYsVUFBVTtBQUNuQixTQUFNLElBQU4sR0FBYSxRQUFiLENBRG1CO0FBRW5CLE9BQUksS0FBSyxjQUFMLENBQW9CLFFBQXBCLEVBQUosRUFBb0M7QUFDbkMsU0FBSyxJQUFMLEdBRG1DO0lBQXBDLE1BRU87QUFDTixTQUFLLElBQUwsR0FETTtJQUZQOzs7O3NCQU9jO0FBQ2QsVUFBTyxNQUFNLFFBQU4sQ0FETzs7b0JBSUYsT0FBTztBQUNuQixPQUFJLFVBQVUsSUFBVixFQUFnQjtBQUNuQixTQUFLLEtBQUwsR0FEbUI7SUFBcEI7QUFHQSxTQUFNLFFBQU4sR0FBaUIsS0FBakIsQ0FKbUI7QUFLbkIsUUFBSyxNQUFMLEdBTG1COzs7O1FBckNmOzs7QUF1R04sT0FBTyxhQUFQLEdBQXVCLGFBQXZCIiwiZmlsZSI6ImdlbmVyYXRlZC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzQ29udGVudCI6WyIoZnVuY3Rpb24gZSh0LG4scil7ZnVuY3Rpb24gcyhvLHUpe2lmKCFuW29dKXtpZighdFtvXSl7dmFyIGE9dHlwZW9mIHJlcXVpcmU9PVwiZnVuY3Rpb25cIiYmcmVxdWlyZTtpZighdSYmYSlyZXR1cm4gYShvLCEwKTtpZihpKXJldHVybiBpKG8sITApO3ZhciBmPW5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIrbytcIidcIik7dGhyb3cgZi5jb2RlPVwiTU9EVUxFX05PVF9GT1VORFwiLGZ9dmFyIGw9bltvXT17ZXhwb3J0czp7fX07dFtvXVswXS5jYWxsKGwuZXhwb3J0cyxmdW5jdGlvbihlKXt2YXIgbj10W29dWzFdW2VdO3JldHVybiBzKG4/bjplKX0sbCxsLmV4cG9ydHMsZSx0LG4scil9cmV0dXJuIG5bb10uZXhwb3J0c312YXIgaT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2Zvcih2YXIgbz0wO288ci5sZW5ndGg7bysrKXMocltvXSk7cmV0dXJuIHN9KSIsIi8qIGdsb2JhbCBXaWRnZXRzQ29tbW9uICovXG5cbi8vIFRoZSBnbG9iYWwgYXVkaW8gc3RhdGUgb2JqZWN0XG5jb25zdCBzdGF0ZSA9IHtcblx0Ly8gVGhlIHBhdGggb2YgYXVkaW8gZmlsZSB0byBwbGF5XG5cdGZpbGU6ICcnLFxuXHQvLyBEaXNhYmxlIGF1ZGlvIGNvbnRyb2xzIGFsdG9nZXRoZXJcblx0ZGlzYWJsZWQ6IGZhbHNlLFxufTtcblxuY2xhc3MgQXVkaW9Db250cm9scyB7XG5cdGNvbnN0cnVjdG9yKHdpZGdldHNDb21tb24sIGZpbGVuYW1lID0gJycpIHtcblx0XHR0aGlzLmNvbW1vbiA9IHdpZGdldHNDb21tb247XG5cdFx0dGhpcy5ub2RlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcignbmF2LmF1ZGlvLWNvbnRyb2xzJyk7XG5cblx0XHRzdGF0ZS5maWxlID0gZmlsZW5hbWU7XG5cblx0XHRpZiAodGhpcy5ub2RlKSB7XG5cdFx0XHR0aGlzLmNyZWF0ZSgpO1xuXHRcdH1cblx0fVxuXG5cdGdldCBhdWRpb0ludGVyZmFjZSgpIHtcblx0XHRyZXR1cm4gdGhpcy5jb21tb24uYXVkaW87XG5cdH1cblxuXHRnZXQgZW1pdHRlcigpIHtcblx0XHRyZXR1cm4gdGhpcy5jb21tb24uZW1pdHRlcjtcblx0fVxuXG5cdGdldCBhdWRpbygpIHtcblx0XHRyZXR1cm4gc3RhdGUuZmlsZTtcblx0fVxuXG5cdHNldCBhdWRpbyhmaWxlbmFtZSkge1xuXHRcdHN0YXRlLmZpbGUgPSBmaWxlbmFtZTtcblx0XHRpZiAodGhpcy5hdWRpb0ludGVyZmFjZS5pc1BhdXNlZCgpKSB7XG5cdFx0XHR0aGlzLmxvYWQoKTtcblx0XHR9IGVsc2Uge1xuXHRcdFx0dGhpcy5wbGF5KCk7XG5cdFx0fVxuXHR9XG5cblx0Z2V0IGRpc2FibGVkKCkge1xuXHRcdHJldHVybiBzdGF0ZS5kaXNhYmxlZDtcblx0fVxuXG5cdHNldCBkaXNhYmxlZCh2YWx1ZSkge1xuXHRcdGlmICh2YWx1ZSA9PT0gdHJ1ZSkge1xuXHRcdFx0dGhpcy5wYXVzZSgpO1xuXHRcdH1cblx0XHRzdGF0ZS5kaXNhYmxlZCA9IHZhbHVlO1xuXHRcdHRoaXMudXBkYXRlKCk7XG5cdH1cblxuXHRjcmVhdGUoKSB7XG5cdFx0dGhpcy5hZGRCdXR0b24oJ3Jld2luZCcsICgpID0+IHRoaXMucmV3aW5kKCkpO1xuXHRcdHRoaXMuYWRkQnV0dG9uKCdwbGF5JywgKCkgPT4gdGhpcy5wbGF5KCkpO1xuXHRcdHRoaXMuYWRkQnV0dG9uKCdwYXVzZScsICgpID0+IHRoaXMucGF1c2UoKSk7XG5cblx0XHR0aGlzLmVtaXR0ZXIub24oJ2F1ZGlvOnN0b3BzJywgKCkgPT4gdGhpcy51cGRhdGUoKSk7XG5cdFx0dGhpcy5lbWl0dGVyLm9uKCdhdWRpbzpzZXQnLCAoZmlsZW5hbWUpID0+IHRoaXMuYXVkaW8gPSBmaWxlbmFtZSk7XG5cdH1cblxuXHRhZGRCdXR0b24oY2xhc3NOYW1lLCBjbGlja0hhbmRsZXIpIHtcblx0XHRjb25zdCBidXR0b24gPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdidXR0b24nKTtcblx0XHRidXR0b24uY2xhc3NMaXN0LmFkZChjbGFzc05hbWUpO1xuXHRcdHRoaXMubm9kZS5hcHBlbmRDaGlsZChidXR0b24pO1xuXG5cdFx0aWYgKHR5cGVvZiBjbGlja0hhbmRsZXIgPT09ICdmdW5jdGlvbicpIHtcblx0XHRcdGJ1dHRvbi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGNsaWNrSGFuZGxlcik7XG5cdFx0fVxuXHR9XG5cblx0dXBkYXRlKCkge1xuXHRcdGlmIChzdGF0ZS5kaXNhYmxlZCkge1xuXHRcdFx0dGhpcy5ub2RlLmNsYXNzTGlzdC5hZGQoJ2Rpc2FibGVkJyk7XG5cdFx0XHR0aGlzLm5vZGUuY2xhc3NMaXN0LnJlbW92ZSgncGxheWluZycpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHR0aGlzLm5vZGUuY2xhc3NMaXN0LnJlbW92ZSgnZGlzYWJsZWQnKTtcblx0XHRcdGlmICh0aGlzLmF1ZGlvSW50ZXJmYWNlLmlzUGF1c2VkKCkpIHtcblx0XHRcdFx0dGhpcy5ub2RlLmNsYXNzTGlzdC5yZW1vdmUoJ3BsYXlpbmcnKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdHRoaXMubm9kZS5jbGFzc0xpc3QuYWRkKCdwbGF5aW5nJyk7XG5cdFx0XHR9XG5cdFx0fVxuXHR9XG5cblx0bG9hZCgpIHtcblx0XHR0aGlzLmF1ZGlvSW50ZXJmYWNlLmxvYWQoc3RhdGUuZmlsZSk7XG5cdFx0dGhpcy51cGRhdGUoKTtcblx0fVxuXG5cdHJld2luZCgpIHtcblx0XHRpZiAoIXN0YXRlLmRpc2FibGVkKSB7XG5cdFx0XHR0aGlzLmF1ZGlvSW50ZXJmYWNlLnNlZWsoMCk7XG5cdFx0fVxuXHRcdHRoaXMudXBkYXRlKCk7XG5cdH1cblxuXHRwbGF5KCkge1xuXHRcdGlmICghc3RhdGUuZGlzYWJsZWQpIHtcblx0XHRcdHRoaXMuYXVkaW9JbnRlcmZhY2UucGxheSh0aGlzLmF1ZGlvKTtcblx0XHR9XG5cdFx0dGhpcy51cGRhdGUoKTtcblx0fVxuXG5cdHBhdXNlKCkge1xuXHRcdHRoaXMuYXVkaW9JbnRlcmZhY2UucGF1c2UoKTtcblx0XHR0aGlzLnVwZGF0ZSgpO1xuXHR9XG59XG5cbndpbmRvdy5BdWRpb0NvbnRyb2xzID0gQXVkaW9Db250cm9scztcbiJdfQ==
