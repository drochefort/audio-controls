/* global AudioControls, WidgetsCommon */

var common = new WidgetsCommon();

var audioControls = new AudioControls(common, 'audio/track3.mp3');

console.log(audioControls.audioInterface);

audioControls.play();

setTimeout(function() {
	audioControls.disabled = true;

	common.emitter.emit('audio:set', 'audio/track5.mp3');

	setTimeout(function() {
		audioControls.disabled = false;

		common.emitter.emit('audio:set', 'audio/track1.mp3');
	}, 5000);
}, 5000);