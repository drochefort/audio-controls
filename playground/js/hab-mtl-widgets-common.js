(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
'use strict';

//
// We store our EE objects in a plain object whose properties are event names.
// If `Object.create(null)` is not supported we prefix the event names with a
// `~` to make sure that the built-in object properties are not overridden or
// used as an attack vector.
// We also assume that `Object.create(null)` is available when the event name
// is an ES6 Symbol.
//
var prefix = typeof Object.create !== 'function' ? '~' : false;

/**
 * Representation of a single EventEmitter function.
 *
 * @param {Function} fn Event handler to be called.
 * @param {Mixed} context Context for function execution.
 * @param {Boolean} once Only emit once
 * @api private
 */
function EE(fn, context, once) {
  this.fn = fn;
  this.context = context;
  this.once = once || false;
}

/**
 * Minimal EventEmitter interface that is molded against the Node.js
 * EventEmitter interface.
 *
 * @constructor
 * @api public
 */
function EventEmitter() { /* Nothing to set */ }

/**
 * Holds the assigned EventEmitters by name.
 *
 * @type {Object}
 * @private
 */
EventEmitter.prototype._events = undefined;

/**
 * Return a list of assigned event listeners.
 *
 * @param {String} event The events that should be listed.
 * @param {Boolean} exists We only need to know if there are listeners.
 * @returns {Array|Boolean}
 * @api public
 */
EventEmitter.prototype.listeners = function listeners(event, exists) {
  var evt = prefix ? prefix + event : event
    , available = this._events && this._events[evt];

  if (exists) return !!available;
  if (!available) return [];
  if (available.fn) return [available.fn];

  for (var i = 0, l = available.length, ee = new Array(l); i < l; i++) {
    ee[i] = available[i].fn;
  }

  return ee;
};

/**
 * Emit an event to all registered event listeners.
 *
 * @param {String} event The name of the event.
 * @returns {Boolean} Indication if we've emitted an event.
 * @api public
 */
EventEmitter.prototype.emit = function emit(event, a1, a2, a3, a4, a5) {
  var evt = prefix ? prefix + event : event;

  if (!this._events || !this._events[evt]) return false;

  var listeners = this._events[evt]
    , len = arguments.length
    , args
    , i;

  if ('function' === typeof listeners.fn) {
    if (listeners.once) this.removeListener(event, listeners.fn, undefined, true);

    switch (len) {
      case 1: return listeners.fn.call(listeners.context), true;
      case 2: return listeners.fn.call(listeners.context, a1), true;
      case 3: return listeners.fn.call(listeners.context, a1, a2), true;
      case 4: return listeners.fn.call(listeners.context, a1, a2, a3), true;
      case 5: return listeners.fn.call(listeners.context, a1, a2, a3, a4), true;
      case 6: return listeners.fn.call(listeners.context, a1, a2, a3, a4, a5), true;
    }

    for (i = 1, args = new Array(len -1); i < len; i++) {
      args[i - 1] = arguments[i];
    }

    listeners.fn.apply(listeners.context, args);
  } else {
    var length = listeners.length
      , j;

    for (i = 0; i < length; i++) {
      if (listeners[i].once) this.removeListener(event, listeners[i].fn, undefined, true);

      switch (len) {
        case 1: listeners[i].fn.call(listeners[i].context); break;
        case 2: listeners[i].fn.call(listeners[i].context, a1); break;
        case 3: listeners[i].fn.call(listeners[i].context, a1, a2); break;
        default:
          if (!args) for (j = 1, args = new Array(len -1); j < len; j++) {
            args[j - 1] = arguments[j];
          }

          listeners[i].fn.apply(listeners[i].context, args);
      }
    }
  }

  return true;
};

/**
 * Register a new EventListener for the given event.
 *
 * @param {String} event Name of the event.
 * @param {Functon} fn Callback function.
 * @param {Mixed} context The context of the function.
 * @api public
 */
EventEmitter.prototype.on = function on(event, fn, context) {
  var listener = new EE(fn, context || this)
    , evt = prefix ? prefix + event : event;

  if (!this._events) this._events = prefix ? {} : Object.create(null);
  if (!this._events[evt]) this._events[evt] = listener;
  else {
    if (!this._events[evt].fn) this._events[evt].push(listener);
    else this._events[evt] = [
      this._events[evt], listener
    ];
  }

  return this;
};

/**
 * Add an EventListener that's only called once.
 *
 * @param {String} event Name of the event.
 * @param {Function} fn Callback function.
 * @param {Mixed} context The context of the function.
 * @api public
 */
EventEmitter.prototype.once = function once(event, fn, context) {
  var listener = new EE(fn, context || this, true)
    , evt = prefix ? prefix + event : event;

  if (!this._events) this._events = prefix ? {} : Object.create(null);
  if (!this._events[evt]) this._events[evt] = listener;
  else {
    if (!this._events[evt].fn) this._events[evt].push(listener);
    else this._events[evt] = [
      this._events[evt], listener
    ];
  }

  return this;
};

/**
 * Remove event listeners.
 *
 * @param {String} event The event we want to remove.
 * @param {Function} fn The listener that we need to find.
 * @param {Mixed} context Only remove listeners matching this context.
 * @param {Boolean} once Only remove once listeners.
 * @api public
 */
EventEmitter.prototype.removeListener = function removeListener(event, fn, context, once) {
  var evt = prefix ? prefix + event : event;

  if (!this._events || !this._events[evt]) return this;

  var listeners = this._events[evt]
    , events = [];

  if (fn) {
    if (listeners.fn) {
      if (
           listeners.fn !== fn
        || (once && !listeners.once)
        || (context && listeners.context !== context)
      ) {
        events.push(listeners);
      }
    } else {
      for (var i = 0, length = listeners.length; i < length; i++) {
        if (
             listeners[i].fn !== fn
          || (once && !listeners[i].once)
          || (context && listeners[i].context !== context)
        ) {
          events.push(listeners[i]);
        }
      }
    }
  }

  //
  // Reset the array, or remove it completely if we have no more listeners.
  //
  if (events.length) {
    this._events[evt] = events.length === 1 ? events[0] : events;
  } else {
    delete this._events[evt];
  }

  return this;
};

/**
 * Remove all listeners or only the listeners for the specified event.
 *
 * @param {String} event The event want to remove all listeners for.
 * @api public
 */
EventEmitter.prototype.removeAllListeners = function removeAllListeners(event) {
  if (!this._events) return this;

  if (event) delete this._events[prefix ? prefix + event : event];
  else this._events = prefix ? {} : Object.create(null);

  return this;
};

//
// Alias methods names because people roll like that.
//
EventEmitter.prototype.off = EventEmitter.prototype.removeListener;
EventEmitter.prototype.addListener = EventEmitter.prototype.on;

//
// This function doesn't apply anymore.
//
EventEmitter.prototype.setMaxListeners = function setMaxListeners() {
  return this;
};

//
// Expose the prefix.
//
EventEmitter.prefixed = prefix;

//
// Expose the module.
//
if ('undefined' !== typeof module) {
  module.exports = EventEmitter;
}

},{}],2:[function(require,module,exports){
/*!
 * js-logger - http://github.com/jonnyreeves/js-logger
 * Jonny Reeves, http://jonnyreeves.co.uk/
 * js-logger may be freely distributed under the MIT license.
 */
(function (global) {
	"use strict";

	// Top level module for the global, static logger instance.
	var Logger = { };

	// For those that are at home that are keeping score.
	Logger.VERSION = "1.2.0";

	// Function which handles all incoming log messages.
	var logHandler;

	// Map of ContextualLogger instances by name; used by Logger.get() to return the same named instance.
	var contextualLoggersByNameMap = {};

	// Polyfill for ES5's Function.bind.
	var bind = function(scope, func) {
		return function() {
			return func.apply(scope, arguments);
		};
	};

	// Super exciting object merger-matron 9000 adding another 100 bytes to your download.
	var merge = function () {
		var args = arguments, target = args[0], key, i;
		for (i = 1; i < args.length; i++) {
			for (key in args[i]) {
				if (!(key in target) && args[i].hasOwnProperty(key)) {
					target[key] = args[i][key];
				}
			}
		}
		return target;
	};

	// Helper to define a logging level object; helps with optimisation.
	var defineLogLevel = function(value, name) {
		return { value: value, name: name };
	};

	// Predefined logging levels.
	Logger.DEBUG = defineLogLevel(1, 'DEBUG');
	Logger.INFO = defineLogLevel(2, 'INFO');
	Logger.TIME = defineLogLevel(3, 'TIME');
	Logger.WARN = defineLogLevel(4, 'WARN');
	Logger.ERROR = defineLogLevel(8, 'ERROR');
	Logger.OFF = defineLogLevel(99, 'OFF');

	// Inner class which performs the bulk of the work; ContextualLogger instances can be configured independently
	// of each other.
	var ContextualLogger = function(defaultContext) {
		this.context = defaultContext;
		this.setLevel(defaultContext.filterLevel);
		this.log = this.info;  // Convenience alias.
	};

	ContextualLogger.prototype = {
		// Changes the current logging level for the logging instance.
		setLevel: function (newLevel) {
			// Ensure the supplied Level object looks valid.
			if (newLevel && "value" in newLevel) {
				this.context.filterLevel = newLevel;
			}
		},

		// Is the logger configured to output messages at the supplied level?
		enabledFor: function (lvl) {
			var filterLevel = this.context.filterLevel;
			return lvl.value >= filterLevel.value;
		},

		debug: function () {
			this.invoke(Logger.DEBUG, arguments);
		},

		info: function () {
			this.invoke(Logger.INFO, arguments);
		},

		warn: function () {
			this.invoke(Logger.WARN, arguments);
		},

		error: function () {
			this.invoke(Logger.ERROR, arguments);
		},

		time: function (label) {
			if (typeof label === 'string' && label.length > 0) {
				this.invoke(Logger.TIME, [ label, 'start' ]);
			}
		},

		timeEnd: function (label) {
			if (typeof label === 'string' && label.length > 0) {
				this.invoke(Logger.TIME, [ label, 'end' ]);
			}
		},

		// Invokes the logger callback if it's not being filtered.
		invoke: function (level, msgArgs) {
			if (logHandler && this.enabledFor(level)) {
				logHandler(msgArgs, merge({ level: level }, this.context));
			}
		}
	};

	// Protected instance which all calls to the to level `Logger` module will be routed through.
	var globalLogger = new ContextualLogger({ filterLevel: Logger.OFF });

	// Configure the global Logger instance.
	(function() {
		// Shortcut for optimisers.
		var L = Logger;

		L.enabledFor = bind(globalLogger, globalLogger.enabledFor);
		L.debug = bind(globalLogger, globalLogger.debug);
		L.time = bind(globalLogger, globalLogger.time);
		L.timeEnd = bind(globalLogger, globalLogger.timeEnd);
		L.info = bind(globalLogger, globalLogger.info);
		L.warn = bind(globalLogger, globalLogger.warn);
		L.error = bind(globalLogger, globalLogger.error);

		// Don't forget the convenience alias!
		L.log = L.info;
	}());

	// Set the global logging handler.  The supplied function should expect two arguments, the first being an arguments
	// object with the supplied log messages and the second being a context object which contains a hash of stateful
	// parameters which the logging function can consume.
	Logger.setHandler = function (func) {
		logHandler = func;
	};

	// Sets the global logging filter level which applies to *all* previously registered, and future Logger instances.
	// (note that named loggers (retrieved via `Logger.get`) can be configured independently if required).
	Logger.setLevel = function(level) {
		// Set the globalLogger's level.
		globalLogger.setLevel(level);

		// Apply this level to all registered contextual loggers.
		for (var key in contextualLoggersByNameMap) {
			if (contextualLoggersByNameMap.hasOwnProperty(key)) {
				contextualLoggersByNameMap[key].setLevel(level);
			}
		}
	};

	// Retrieve a ContextualLogger instance.  Note that named loggers automatically inherit the global logger's level,
	// default context and log handler.
	Logger.get = function (name) {
		// All logger instances are cached so they can be configured ahead of use.
		return contextualLoggersByNameMap[name] ||
			(contextualLoggersByNameMap[name] = new ContextualLogger(merge({ name: name }, globalLogger.context)));
	};

	// Configure and example a Default implementation which writes to the `window.console` (if present).  The
	// `options` hash can be used to configure the default logLevel and provide a custom message formatter.
	Logger.useDefaults = function(options) {
		options = options || {};

		options.formatter = options.formatter || function defaultMessageFormatter(messages, context) {
			// Prepend the logger's name to the log message for easy identification.
			if (context.name) {
				messages.unshift("[" + context.name + "]");
			}
		};

		// Check for the presence of a logger.
		if (typeof console === "undefined") {
			return;
		}

		// Map of timestamps by timer labels used to track `#time` and `#timeEnd()` invocations in environments
		// that don't offer a native console method.
		var timerStartTimeByLabelMap = {};

		// Support for IE8+ (and other, slightly more sane environments)
		var invokeConsoleMethod = function (hdlr, messages) {
			Function.prototype.apply.call(hdlr, console, messages);
		};

		Logger.setLevel(options.defaultLevel || Logger.DEBUG);
		Logger.setHandler(function(messages, context) {
			// Convert arguments object to Array.
			messages = Array.prototype.slice.call(messages);

			var hdlr = console.log;
			var timerLabel;

			if (context.level === Logger.TIME) {
				timerLabel = (context.name ? '[' + context.name + '] ' : '') + messages[0];

				if (messages[1] === 'start') {
					if (console.time) {
						console.time(timerLabel);
					}
					else {
						timerStartTimeByLabelMap[timerLabel] = new Date().getTime();
					}
				}
				else {
					if (console.timeEnd) {
						console.timeEnd(timerLabel);
					}
					else {
						invokeConsoleMethod(hdlr, [ timerLabel + ': ' +
							(new Date().getTime() - timerStartTimeByLabelMap[timerLabel]) + 'ms' ]);
					}
				}
			}
			else {
				// Delegate through to custom warn/error loggers if present on the console.
				if (context.level === Logger.WARN && console.warn) {
					hdlr = console.warn;
				} else if (context.level === Logger.ERROR && console.error) {
					hdlr = console.error;
				} else if (context.level === Logger.INFO && console.info) {
					hdlr = console.info;
				}

				options.formatter(messages, context);
				invokeConsoleMethod(hdlr, messages);
			}
		});
	};

	// Export to popular environments boilerplate.
	if (typeof define === 'function' && define.amd) {
		define(Logger);
	}
	else if (typeof module !== 'undefined' && module.exports) {
		module.exports = Logger;
	}
	else {
		Logger._prevLogger = global.Logger;

		Logger.noConflict = function () {
			global.Logger = Logger._prevLogger;
			return Logger;
		};

		global.Logger = Logger;
	}
}(this));

},{}],3:[function(require,module,exports){
(function (global){
'use strict';

var stub = require('./stub');
var tracking = require('./tracking');
var ls = 'localStorage' in global && global.localStorage ? global.localStorage : stub;

function accessor (key, value) {
  if (arguments.length === 1) {
    return get(key);
  }
  return set(key, value);
}

function get (key) {
  return JSON.parse(ls.getItem(key));
}

function set (key, value) {
  try {
    ls.setItem(key, JSON.stringify(value));
    return true;
  } catch (e) {
    return false;
  }
}

function remove (key) {
  return ls.removeItem(key);
}

function clear () {
  return ls.clear();
}

accessor.set = set;
accessor.get = get;
accessor.remove = remove;
accessor.clear = clear;
accessor.on = tracking.on;
accessor.off = tracking.off;

module.exports = accessor;

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{"./stub":4,"./tracking":5}],4:[function(require,module,exports){
'use strict';

var ms = {};

function getItem (key) {
  return key in ms ? ms[key] : null;
}

function setItem (key, value) {
  ms[key] = value;
  return true;
}

function removeItem (key) {
  var found = key in ms;
  if (found) {
    return delete ms[key];
  }
  return false;
}

function clear () {
  ms = {};
  return true;
}

module.exports = {
  getItem: getItem,
  setItem: setItem,
  removeItem: removeItem,
  clear: clear
};

},{}],5:[function(require,module,exports){
(function (global){
'use strict';

var listeners = {};
var listening = false;

function listen () {
  if (global.addEventListener) {
    global.addEventListener('storage', change, false);
  } else if (global.attachEvent) {
    global.attachEvent('onstorage', change);
  } else {
    global.onstorage = change;
  }
}

function change (e) {
  if (!e) {
    e = global.event;
  }
  var all = listeners[e.key];
  if (all) {
    all.forEach(fire);
  }

  function fire (listener) {
    listener(JSON.parse(e.newValue), JSON.parse(e.oldValue), e.url || e.uri);
  }
}

function on (key, fn) {
  if (listeners[key]) {
    listeners[key].push(fn);
  } else {
    listeners[key] = [fn];
  }
  if (listening === false) {
    listen();
  }
}

function off (key, fn) {
  var ns = listeners[key];
  if (ns.length > 1) {
    ns.splice(ns.indexOf(fn), 1);
  } else {
    listeners[key] = [];
  }
}

module.exports = {
  on: on,
  off: off
};

}).call(this,typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})

},{}],6:[function(require,module,exports){
var WidgetsCommon = (function () {
    function WidgetsCommon(options) {
        if (options === void 0) { options = { resizeToViewport: false }; }
        this.logger = require('js-logger');
        this.storage = require('local-storage');
        this.EventEmitter = require('eventemitter3');
        this.emitter = new this.EventEmitter();
        this.rce = new RceInterface(this.logger, this.emitter);
        this.initLogger();
        this.configData = new ConfigDataModule(this.logger, this.rce, this.storage);
        this.resizeModule = new ResizeModule(this.logger, this.emitter, this.configData);
        this.audio = new AudioModule(this.logger, this.rce, this.emitter);
        this.fullscreen = new FullscreenModule(this.logger, this.resizeModule, this.configData);
        this.userData = new UserDataModule(this.logger, this.emitter, this.rce);
        this.widgetData = new WidgetDataModule(this.logger);
        this.layout = new LayoutModule(this.logger, this.rce, this.emitter, options);
        this.buildInfoModule = new BuildInfoModule(this.logger);
        this.dialog = new DialogModule(this.logger);
        window.widgetEventBus = this.emitter;
        if (this.configData.isLocal()) {
            document.documentElement.style.overflowY = 'auto';
        }
    }
    WidgetsCommon.prototype.version = function () {
        return '2.0.1';
    };
    WidgetsCommon.prototype.navigateTo = function (uri, callback) {
        if (callback === void 0) { callback = null; }
        if (this.configData.isProduction()) {
            uri = uri.replace(/html$/i, 'xhtml');
        }
        try {
            this.logger.log('Is RCE available?', this.rce.isAvailable());
            if (this.rce.isAvailable()) {
                var match = window.parent.location.href.match(/^(http.+s9ml\/)(.+)$/);
                var url = match && match[1] ? match[1] + uri : uri;
                this.rce.navigateTo(url, callback);
                return;
            }
            else {
                var url = '../../../s9ml/' + uri;
                window.parent.location.href = url;
            }
        }
        catch (e) {
            this.logger.warn(e);
        }
        if (callback) {
            callback();
            return;
        }
    };
    WidgetsCommon.prototype.initLogger = function () {
        var _this = this;
        this.logger.useDefaults();
        this.logger.off = function () { return _this.logger.setLevel(_this.logger.OFF); };
        this.logger.on = function () { return _this.logger.setLevel(_this.logger.DEBUG); };
        if (this.rce.isAvailable()) {
            this.logger.off();
        }
        this.emitter.on('logger:on', function () { return _this.logger.on(); });
        this.emitter.on('logger:off', function () { return _this.logger.off(); });
    };
    return WidgetsCommon;
})();
window.WidgetsCommon = WidgetsCommon;
module.exports = WidgetsCommon;
var RceInterface = (function () {
    function RceInterface(logger, emitter) {
        var _this = this;
        this.isRCE = false;
        this.logger = logger;
        this.emitter = emitter;
        this.detectRCE();
        window.rceEventBusOnLoad = function (bus, csm) {
            if (csm === void 0) { csm = null; }
            return _this.rceEventBusOnLoad(bus, csm);
        };
    }
    RceInterface.prototype.isAvailable = function () {
        return this.isRCE;
    };
    RceInterface.prototype.isInitialized = function () {
        return typeof this.bus !== 'undefined';
    };
    RceInterface.prototype.enableAudio = function (callback) {
        this.trigger('media:enable', '', callback);
    };
    RceInterface.prototype.disableAudio = function (callback) {
        this.trigger('media:disable', '', callback);
    };
    RceInterface.prototype.navigateTo = function (uri, callback) {
        this.trigger('navigate:to', '', callback, uri);
    };
    RceInterface.prototype.saveUserData = function (parameters, callback) {
        this.trigger('xapiPersist:save', 'xapiPersist:saved', callback, parameters);
    };
    RceInterface.prototype.retrieveUserData = function (parameters, callback) {
        this.trigger('xapiPersist:retrieve', 'xapiPersist:retrieved', callback, parameters);
    };
    RceInterface.prototype.detectRCE = function () {
        this.isRCE = false;
        try {
            if (!!window.top.document.getElementById('rce-container')) {
                this.isRCE = true;
                return true;
            }
        }
        catch (err) {
            this.logger.warn('Common Widgets Library', 'AudioModule::detectRCE', err);
        }
        return false;
    };
    RceInterface.prototype.rceEventBusOnLoad = function (bus, csm) {
        var _this = this;
        if (csm === void 0) { csm = null; }
        this.logger.log('RCE is calling me to initialize the bus, great!');
        this.isRCE = true;
        this.bus = bus;
        this.csm = csm;
        this.emitter.on('csm:parse', function (context) { return _this.parseContentScripts(context); });
        this.emitter.emit('rce:ready');
    };
    RceInterface.prototype.trigger = function (busEvent, waitFor, callback, parameters) {
        if (this.bus) {
            this.logger.log('Sending message over the bus', busEvent);
            if (waitFor && typeof callback === 'function') {
                this.bus.once(waitFor, callback);
            }
            this.bus.emit(busEvent, parameters);
        }
        if (!this.bus || !waitFor) {
            if (typeof callback === 'function') {
                callback(parameters);
            }
        }
    };
    RceInterface.prototype.parseContentScripts = function (context) {
        var _this = this;
        if (context === void 0) { context = { doc: document, global: window }; }
        if (this.csm && this.csm.scripts) {
            this.csm.scripts.update().then(function () {
                _this.csm.Helpers.noFlicker(document);
                _this.csm.run(context);
            });
        }
    };
    return RceInterface;
})();
var AudioModule = (function () {
    function AudioModule(logger, rce, emitter) {
        var _this = this;
        this.rce = null;
        this.delay = 500;
        this.isWaitingForFocus = true;
        this.audioReady = false;
        this.rce = rce;
        this.logger = logger;
        this.emitter = emitter;
        var source = document.createElement('source');
        source.setAttribute('type', 'audio/mpeg');
        this.audioElement = document.createElement('audio');
        this.audioElement.appendChild(source);
        this.audioElement.addEventListener('ended', function () { return _this.pause(); }, false);
        this.audioElement.querySelector('source').src = '';
        this.emitter.on('audio:play', function (url) { return _this.play(url); });
        this.emitter.on('audio:pause', function () { return _this.pause(); });
        this.determinePlaybackReadiness();
    }
    AudioModule.prototype.registerEndPlaybackHandler = function (callback) {
        this.audioElement.addEventListener('ended', callback, false);
    };
    AudioModule.prototype.isPaused = function () {
        return this.audioElement.paused;
    };
    AudioModule.prototype.setAudioDelay = function (delay) {
        if (delay >= 0) {
            this.delay = delay;
        }
    };
    AudioModule.prototype.playOnFocus = function (url, node, delay) {
        if (node === void 0) { node = window; }
        if (delay === void 0) { delay = this.delay; }
        node.removeEventListener('focus', node.focusListener);
        window.removeEventListener('blur', window.blurListener);
        node.focusListener = this.focusPlaybackHandler.bind(this, url, delay);
        node.addEventListener('focus', node.focusListener, false);
        window.blurListener = this.blurPlaybackHandler.bind(this, url, delay);
        window.addEventListener('blur', window.blurListener, false);
    };
    AudioModule.prototype.playOnClick = function (url, node, delay) {
        if (node === void 0) { node = window; }
        if (delay === void 0) { delay = this.delay; }
        node.addEventListener('click', this.clickPlaybackHandler.bind(this, url, delay), false);
        window.addEventListener('blur', this.blurPlaybackHandler.bind(this, url, delay), false);
    };
    AudioModule.prototype.load = function (url) {
        if (this.audioElement) {
            this.audioElement.querySelector('source').src = url;
            this.audioElement.load();
        }
    };
    AudioModule.prototype.play = function (url, delay, replay) {
        var _this = this;
        if (delay === void 0) { delay = this.delay; }
        if (replay === void 0) { replay = false; }
        this.emitter.emit('audio:starts');
        this.isWaitingForFocus = false;
        this.rce.disableAudio(function () {
            if (_this.audioElement) {
                if (url && (replay || !_this.matchSource(url))) {
                    _this.logger.log('==RELOADING==');
                    _this.audioElement.querySelector('source').src = url;
                    _this.audioElement.load();
                }
                else if (replay) {
                    _this.audioElement.load();
                }
                else {
                    _this.logger.log('==RESUME==', replay, !_this.matchSource(url));
                }
                _this.audioElement.play();
                _this.logger.log('*** PLAYING');
                if (_this.audioElement.paused) {
                    _this.logger.warn('audio playback was killed');
                    _this.emitter.emit('audio:killed');
                }
            }
            else {
                _this.logger.error('AudioModule:play', 'Unhandled context');
            }
        });
    };
    AudioModule.prototype.pause = function () {
        var _this = this;
        this.emitter.emit('audio:stops');
        this.rce.enableAudio(function () {
            if (_this.audioElement) {
                _this.audioElement.pause();
                _this.logger.log('*** PAUSED');
            }
        });
    };
    AudioModule.prototype.currentTime = function () {
        if (this.audioElement) {
            return this.audioElement.currentTime;
        }
    };
    AudioModule.prototype.duration = function () {
        if (this.audioElement) {
            return this.audioElement.duration;
        }
    };
    AudioModule.prototype.remainingTime = function () {
        if (this.audioElement) {
            return this.audioElement.duration - this.audioElement.currentTime;
        }
    };
    AudioModule.prototype.seek = function (time) {
        if (this.audioElement && typeof time !== 'undefined') {
            this.audioElement.currentTime = time;
        }
    };
    AudioModule.prototype.isReady = function () {
        return this.audioReady;
    };
    AudioModule.prototype.matchSource = function (url) {
        if (url && this.audioElement) {
            var regex = /^.+\//;
            var file = url.replace(regex, '');
            var src = this.audioElement.querySelector('source').src.replace(regex, '');
            return file === src;
        }
        return false;
    };
    AudioModule.prototype.clickPlaybackHandler = function (url, delay) {
        if (delay === void 0) { delay = this.delay; }
        this.logger.log('CLICK RECIEVED');
        this.play(url, delay, true);
    };
    AudioModule.prototype.focusPlaybackHandler = function (url, delay) {
        if (delay === void 0) { delay = this.delay; }
        this.logger.log('RECIEVING FOCUS', this.isWaitingForFocus);
        if (this.isWaitingForFocus) {
            this.play(url, delay);
        }
    };
    AudioModule.prototype.blurPlaybackHandler = function (url, delay) {
        this.logger.log('BLUR', this.isWaitingForFocus);
        if (!this.isWaitingForFocus) {
            this.isWaitingForFocus = true;
            this.pause();
        }
    };
    AudioModule.prototype.setReady = function () {
        this.audioReady = true;
        this.emitter.emit('audio:ready');
    };
    AudioModule.prototype.determinePlaybackReadiness = function () {
        var _this = this;
        if (this.rce.isAvailable()) {
            this.rce.isInitialized() ? this.setReady() : this.emitter.on('rce:ready', function () { return _this.setReady(); });
        }
        else {
            this.setReady();
        }
    };
    return AudioModule;
})();
var BuildInfoModule = (function () {
    function BuildInfoModule(logger) {
        var _this = this;
        this.logger = logger;
        if (window.location.href.indexOf('editor.html') > -1) {
            this.loadVersion(function (buildInfos) { return _this.displayVersion(buildInfos); });
        }
    }
    BuildInfoModule.prototype.displayVersion = function (buildInfos) {
        this.versionDiv = document.createElement('div');
        this.versionDiv.id = 'widget-build-info';
        this.versionDiv.style.right = '0';
        this.versionDiv.style.bottom = '0';
        this.versionDiv.style.position = 'fixed';
        this.versionDiv.style.backgroundColor = '#2E313B';
        this.versionDiv.style.opacity = '1';
        this.versionDiv.style.color = '#FFFFFF';
        this.versionDiv.style.padding = '5px';
        this.versionDiv.style.fontSize = '11px';
        document.body.style.paddingBottom = '15px';
        this.versionDiv.className = 'widget-version';
        var buildNumber = buildInfos.buildNumber;
        var time = buildInfos.time;
        var ciUser = buildInfos.ciUser;
        var gitRepo = buildInfos.gitRepo;
        var gitBranch = buildInfos.gitBranch;
        this.logger.log('Display BuildInfo: ' + buildNumber + ' - ' + time);
        if (time) {
            this.versionDiv.innerHTML = 'Build  ' + buildNumber + ' - ' + time + ' ';
            this.versionDiv.innerHTML += 'By ' + ciUser + ' ' + gitRepo + ' branch: ' + gitBranch;
        }
        else {
            this.versionDiv.innerHTML = '<b>' + buildNumber + '</b>';
        }
        document.body.insertBefore(this.versionDiv, document.body.firstChild);
    };
    BuildInfoModule.prototype.loadVersion = function (callback) {
        var jsonFile = 'build-info.json';
        var request = new XMLHttpRequest();
        request.open('GET', jsonFile, true);
        request.onload = function () {
            if (request.status >= 200 && request.status < 400 && request.responseText) {
                var data = request.responseText;
                var parsedJson = JSON.parse(data);
                callback(parsedJson);
            }
        };
        request.send();
    };
    return BuildInfoModule;
})();
var ConfigDataModule = (function () {
    function ConfigDataModule(logger, rce, storage) {
        this.__simulateProduction = false;
        this.defaultKey = 'config:' + document.querySelector('head title').innerHTML;
        this.attempts = 0;
        this.pingTimeout = 200;
        this.logger = logger;
        this.rce = rce;
        this.storage = storage;
    }
    ConfigDataModule.prototype.isLocal = function () {
        if (this.__simulateProduction) {
            return false;
        }
        if (this.rce && this.rce.isAvailable()) {
            return false;
        }
        if (window.location.hostname && window.location.hostname.indexOf('localhost') > -1) {
            return true;
        }
        if (window.location.href && window.location.href.indexOf('__test__') > -1) {
            return true;
        }
        return false;
    };
    ConfigDataModule.prototype.isHabitat = function () {
        if (window.location.hostname && window.location.hostname.indexOf('inkling') > -1) {
            return true;
        }
        return false;
    };
    ConfigDataModule.prototype.isProduction = function () {
        return !this.isLocal() && !this.isHabitat();
    };
    ConfigDataModule.prototype.setDefaultData = function (data) {
        this.defaultData = data;
    };
    ConfigDataModule.prototype.getDefaultData = function () {
        return this.defaultData;
    };
    ConfigDataModule.prototype.setDefaultKey = function (key) {
        this.defaultKey = key;
    };
    ConfigDataModule.prototype.load = function (callback, key) {
        var _this = this;
        this.logger.log('LOADING CONFIGURATION ... ', {
            production: this.isProduction(),
            habitat: this.isHabitat(),
            local: this.isLocal()
        });
        if (++this.attempts > 5) {
            this.logger.error('General Network Failure');
            this.handleConfigError(callback, key);
            return;
        }
        var jsonFile = this.getHabitatConfig();
        if (this.isLocal() || jsonFile === '') {
            callback(this.loadConfigLocal(key));
        }
        else {
            this.logger.log('JSON FILE name is ', jsonFile);
            var request = new XMLHttpRequest();
            request.open('GET', jsonFile, true);
            request.onload = function () {
                if (request.status >= 200 && request.status < 400 && request.responseText) {
                    _this.logger.log('CONFIG-DATA', 'Load success', request);
                    var parsedJson = JSON.parse(request.responseText);
                    if (parsedJson.config) {
                        callback(parsedJson.config);
                    }
                    else if (parsedJson.json) {
                        callback(parsedJson.json);
                    }
                    else {
                        callback(parsedJson);
                    }
                }
                else {
                    if (_this.isHabitat()) {
                        callback(_this.loadConfigLocal(key));
                    }
                    else {
                        setTimeout(callback(_this.load(callback, key)), _this.pingTimeout);
                    }
                }
            };
            request.onerror = function (err) {
                _this.logger.log('REQUEST ERROR', err);
                if (_this.isHabitat()) {
                    callback(_this.loadConfigLocal(key));
                }
                else {
                    setTimeout(callback(_this.load(callback, key)), _this.pingTimeout);
                }
            };
            request.send();
        }
    };
    ConfigDataModule.prototype.saveAsParameters = function (data, key) {
        this.genericSave(data, 'set', key);
    };
    ConfigDataModule.prototype.save = function (data, key) {
        this.genericSave(data, 'file', key);
    };
    ConfigDataModule.prototype.uploadAsset = function (callback) {
        if (this.isLocal()) {
            this.logger.warn('Try to open asset picker in local context');
        }
        else {
            window.addEventListener('message', callback);
            window.parent.postMessage({
                'type': 'asset',
                'method': 'image',
                'payload': {}
            }, '*');
        }
    };
    ConfigDataModule.prototype.openAssetPicker = function (callback) {
        this.logger.warn('DEPRECTATED: please use uploadAsset() API instead');
        this.uploadAsset(callback);
    };
    ConfigDataModule.prototype.uploadImage = function (image, progress, callback) {
        this.logger.warn('DEPRECTATED: please use uploadAsset() API instead');
        this.logger.warn('UNUSED', image, progress);
        this.uploadAsset(callback);
    };
    ConfigDataModule.prototype.compare = function (data, reference) {
        this.logger.warn('DEPRECTATED: please do not use');
        return true;
    };
    ConfigDataModule.prototype.getHabitatConfig = function () {
        if (s9 && s9.initialParams && s9.initialParams.configFile) {
            return s9.initialParams.configFile;
        }
        return '';
    };
    ConfigDataModule.prototype.saveConfigHabitat = function (payload, method) {
        if (method === void 0) { method = 'file'; }
        this.logger.log('CALLING ME', 'saveConfigHabitat', payload, method);
        window.parent.postMessage({
            'type': 'configuration',
            'method': method,
            'payload': payload
        }, '*');
    };
    ConfigDataModule.prototype.saveConfigLocal = function (data, key) {
        if (this.isLocal()) {
            this.storage.set(key || this.defaultKey, JSON.stringify(data));
        }
    };
    ConfigDataModule.prototype.genericSave = function (data, method, key) {
        this.logger.log('Save data:\n' + JSON.stringify(data));
        if (this.isLocal()) {
            this.saveConfigLocal(data, key);
        }
        else {
            if (method && method === 'set') {
                this.saveConfigHabitat(data, method);
            }
            else {
                this.saveConfigHabitat({ config: data }, method);
            }
        }
    };
    ConfigDataModule.prototype.loadConfigLocal = function (key) {
        if (key === void 0) { key = this.defaultKey; }
        this.logger.log('Calling loadConfigLocal', key, this.storage.get(key));
        var rawData = this.storage.get(key);
        var data = typeof rawData === 'string' ? JSON.parse(rawData) : rawData;
        return data || this.defaultData;
    };
    ConfigDataModule.prototype.handleConfigError = function (callback, key) {
        this.logger.warn('CONFIG-DATA', 'Configuration Data could not be loaded');
        callback(null);
    };
    return ConfigDataModule;
})();
var DialogModule = (function () {
    function DialogModule(logger) {
        this.logger = logger;
    }
    DialogModule.prototype.alert = function (str) {
    };
    DialogModule.prototype.confirm = function (str) {
        return true;
    };
    return DialogModule;
})();
var ResizeModule = (function () {
    function ResizeModule(logger, emitter, config) {
        var _this = this;
        this.delay = 100;
        this.debouncingRate = 250;
        this.logger = logger;
        this.emitter = emitter;
        this.config = config;
        var handler = this.debounce(this.onResize, this);
        window.addEventListener('resize', handler);
        window.addEventListener('orientationchange', handler);
        this.emitter.on('resize', function () { return _this.resize(); });
    }
    ResizeModule.prototype.setFixedDimensions = function (height, width) {
        if (height === void 0) { height = 0; }
        if (width === void 0) { width = 0; }
        this.fixedHeight = height;
        this.fixedWidth = width;
    };
    ResizeModule.prototype.resizeOnEvent = function (eventName) {
        var _this = this;
        document.documentElement.dataset.widgetHeight = String(this.resize());
        this.logger.log('resizeModule', 'Currently saved height', document.documentElement.dataset.widgetHeight);
        this.emitter.once(eventName, function () { return _this.recoverSize(); });
    };
    ResizeModule.prototype.resize = function (height) {
        var _this = this;
        if (s9 && s9.view) {
            this.emitter.emit('resize:before');
            setTimeout(function () {
                var dimensions = {
                    height: _this.fixedHeight || height || _this.getHeight()
                };
                if (_this.fixedWidth) {
                    dimensions.width = _this.fixedWidth;
                }
                _this.logger.log('RESIZING...', dimensions);
                s9.view.size(dimensions);
                _this.emitter.emit('resize:after');
            }, this.delay);
        }
    };
    ResizeModule.prototype.getHeight = function () {
        var body = document.body;
        var html = document.documentElement;
        return Math.max(body.clientHeight, body.offsetHeight, html.offsetHeight);
    };
    ResizeModule.prototype.onResize = function () {
        this.resize();
    };
    ResizeModule.prototype.recoverSize = function () {
        var _this = this;
        var height = parseInt(document.documentElement.dataset.widgetHeight, 10);
        this.logger.log('Recovering size', height);
        setTimeout(function () {
            _this.resize(height);
        }, this.delay);
        return height;
    };
    ResizeModule.prototype.debounce = function (func, context, wait, immediate) {
        if (wait === void 0) { wait = this.debouncingRate; }
        if (immediate === void 0) { immediate = false; }
        var timeout;
        return function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }
            var callNow = immediate && !timeout;
            clearTimeout(timeout);
            timeout = setTimeout(function () {
                timeout = null;
                if (!immediate) {
                    func.apply.apply(func, [context].concat(args));
                }
            }, wait);
            if (callNow) {
                func.apply(context, args);
            }
        };
    };
    return ResizeModule;
})();
var FullscreenModule = (function () {
    function FullscreenModule(logger, resizer, config) {
        this.supportsFullScreen = false;
        this.fullScreenEventName = '';
        this.prefix = '';
        this.browserPrefixes = 'webkit,moz,o,ms,khtml'.split(',');
        this.logger = logger;
        this.resizer = resizer;
        this.config = config;
        this.enableFullScreenOnIframe();
        this.detectFullScreenSupport();
        this.updateFullScreenEventName();
    }
    FullscreenModule.prototype.getEventName = function () {
        return this.fullScreenEventName;
    };
    FullscreenModule.prototype.isFullScreenSupported = function () {
        return this.supportsFullScreen;
    };
    FullscreenModule.prototype.isFullScreen = function () {
        switch (this.prefix) {
            case '':
                return typeof document.fullscreenElement !== 'undefined' && document.fullscreenElement !== null;
            default:
                return document[this.prefix + 'FullscreenElement'] || document[this.prefix + 'FullScreenElement'];
        }
    };
    FullscreenModule.prototype.requestFullScreen = function (el) {
        if (el === void 0) { el = document.documentElement; }
        if (el === null) {
            el = document.documentElement;
        }
        if (this.supportsFullScreen) {
            document.body.classList.add('fullscreen');
            this.resizer.resizeOnEvent(this.fullScreenEventName);
            if (typeof el.requestFullScreen !== 'undefined' ||
                typeof el[this.prefix + 'RequestFullScreen'] !== 'undefined') {
                (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
            }
            else if (typeof el.requestFullscreen !== 'undefined' ||
                typeof el[this.prefix + 'RequestFullscreen'] !== 'undefined') {
                (this.prefix === '') ? el.requestFullscreen() : el[this.prefix + 'RequestFullscreen']();
            }
            return true;
        }
        return false;
    };
    FullscreenModule.prototype.exitFullScreen = function () {
        if (this.supportsFullScreen) {
            document.body.classList.remove('fullscreen');
            if (this.prefix === '') {
                if (typeof document.cancelFullScreen !== 'undefined') {
                    document.cancelFullScreen();
                }
                else if (typeof document.exitFullscreen !== 'undefined') {
                    document.exitFullscreen();
                }
            }
            else if (this.prefix === 'ms') {
                this.logger.warn('FULLSCREEN', 'Exiting');
                document.msExitFullscreen();
            }
            else {
                if (typeof document[this.prefix + 'CancelFullScreen'] !== 'undefined') {
                    document[this.prefix + 'CancelFullScreen']();
                }
                else {
                    document[this.prefix + 'ExitFullscreen']();
                }
            }
        }
    };
    FullscreenModule.prototype.cancelFullScreen = function () {
        this.logger.warn('DEPRECATED: use exitFullScreen() instead');
        this.exitFullScreen();
    };
    FullscreenModule.prototype.detectFullScreenSupport = function () {
        if (typeof document.cancelFullScreen !== 'undefined' ||
            typeof document.exitFullscreen !== 'undefined') {
            this.supportsFullScreen = !this.config.isHabitat();
        }
        else {
            for (var i = 0, il = this.browserPrefixes.length; i < il; i++) {
                this.prefix = this.browserPrefixes[i];
                if (typeof document[this.prefix + 'CancelFullScreen'] !== 'undefined' ||
                    typeof document[this.prefix + 'ExitFullscreen'] !== 'undefined' ||
                    typeof document.exitFullscreen !== 'undefined') {
                    this.supportsFullScreen = !this.config.isHabitat();
                    break;
                }
            }
        }
        if (this.supportsFullScreen === false) {
            document.documentElement.classList.add('unsupported-fullscreen');
        }
    };
    FullscreenModule.prototype.updateFullScreenEventName = function () {
        if (this.supportsFullScreen) {
            this.fullScreenEventName = this.prefix + 'fullscreenchange';
        }
    };
    FullscreenModule.prototype.enableFullScreenOnIframe = function () {
        try {
            var iframeNode = window.frameElement || window.element;
            if (iframeNode) {
                iframeNode.setAttribute('allowfullscreen', 'true');
                iframeNode.setAttribute('webkitallowfullscreen', 'true');
                iframeNode.setAttribute('mozallowfullscreen', 'true');
            }
        }
        catch (err) {
            this.logger.warn('enableFullScreenOnIframe', err);
        }
    };
    return FullscreenModule;
})();
var LayoutModule = (function () {
    function LayoutModule(logger, rce, emitter, options) {
        var _this = this;
        if (options === void 0) { options = { resizeToViewport: false }; }
        this.initialized = false;
        this.expand = false;
        this.logger = logger;
        this.rce = rce;
        this.emitter = emitter;
        this.expand = options.resizeToViewport;
        if (this.rce.isAvailable()) {
            this.scrollingElement = window.parent.parent.document.getElementById('scrolled-content-frame');
            if (!this.scrollingElement) {
                this.scrollingContainer = window.parent;
                this.scrollingElement = window.parent.document.body;
            }
            else {
                this.scrollingContainer = this.scrollingElement;
            }
            this.emitter.once('rce:ready', function () { return _this.parseContent(); });
            this.initialized = true;
            this.expandWidgetToViewport();
            this.emitter.on('resize:before', function () { return _this.expandWidgetToViewport(); });
            this.emitter.on('resize:after', function () { return _this.bubbleResizeUpToRCE(); });
        }
    }
    LayoutModule.prototype.parseContent = function () {
        var _this = this;
        this.logger.log('LAYOUT', 'Pasing the content for specific data attributes');
        var headers = document.querySelectorAll('[data-header]');
        Array.prototype.forEach.call(headers, function (el) { return _this.setFixedHeader(el); });
        this.setFixedFooter(document.querySelector('[data-footer]'));
    };
    LayoutModule.prototype.isInitialized = function () {
        return this.initialized && this.scrollingElement !== null;
    };
    LayoutModule.prototype.expandWidgetToViewport = function () {
        if (this.isInitialized() && this.expand) {
            this.bubbleResizeUpToRCE();
            var viewportHeight = getComputedStyle(this.scrollingElement).height;
            document.body.style.minHeight = viewportHeight;
            var nodes = document.querySelectorAll('[data-full-height]');
            Array.prototype.forEach.call(nodes, function (el) { return el.style.minHeight = viewportHeight; });
        }
    };
    LayoutModule.prototype.bubbleResizeUpToRCE = function () {
        if (this.isInitialized()) {
            var evt;
            try {
                evt = new Event('resize');
            }
            catch (e) {
                evt = window.parent.parent.document.createEvent('UIEvents');
                evt.initUIEvent('resize', true, false, window, 0);
            }
            window.parent.parent.dispatchEvent(evt);
        }
    };
    LayoutModule.prototype.setFixedHeader = function (element) {
        var _this = this;
        this.logger.log('LAYOUT', 'setFixedHeader', element);
        if (this.isInitialized() && element) {
            element.style.position = 'absolute';
            this.repositionHeaderOnResize(element);
            this.emitter.on('resize:after', function () { return _this.repositionHeaderOnResize(element); });
            this.scrollingContainer.addEventListener('scroll', function () { return _this.repositionHeaderOnScroll(element); });
        }
    };
    LayoutModule.prototype.setFixedFooter = function (element) {
        var _this = this;
        this.logger.log('LAYOUT', 'setFixedFooter', element);
        if (this.isInitialized() && element) {
            element.style.position = 'absolute';
            this.repositionFooterOnResize(element);
            this.emitter.on('resize:after', function () { return _this.repositionFooterOnResize(element); });
            this.scrollingContainer.addEventListener('scroll', function () { return _this.repositionFooterOnScroll(element); });
        }
    };
    LayoutModule.prototype.repositionFooterOnResize = function (element) {
        element.style.bottom = '0';
        this.expandWidgetToViewport();
        this.bottomPosition = element.getBoundingClientRect().bottom - window.parent.parent.document.documentElement.clientHeight;
        this.logger.log('LAYOUT', 'bottom position', this.bottomPosition);
        element.style.bottom = Math.max(this.bottomPosition, 0) + 'px';
        this.repositionFooterOnScroll(element);
    };
    LayoutModule.prototype.repositionHeaderOnResize = function (element) {
        this.scrollingElement.scrollTop = 0;
        element.style.top = '0';
        this.expandWidgetToViewport();
        this.topPosition = 0;
        this.logger.log('LAYOUT', 'top position', this.topPosition);
        element.style.top = Math.max(this.topPosition, 0) + 'px';
        this.repositionHeaderOnScroll(element);
    };
    LayoutModule.prototype.repositionHeaderOnScroll = function (element) {
        var delta = this.scrollingElement.scrollTop - this.topPosition;
        this.logger.log('LAYOUT', 'delta position', delta);
        requestAnimationFrame(function () { return element.style.top = Math.max(delta, 0) + 'px'; });
    };
    LayoutModule.prototype.repositionFooterOnScroll = function (element) {
        var delta = this.bottomPosition - this.scrollingElement.scrollTop;
        this.logger.log('LAYOUT', 'delta position', delta);
        requestAnimationFrame(function () { return element.style.bottom = Math.max(delta, 0) + 'px'; });
    };
    return LayoutModule;
})();
var UserDataModule = (function () {
    function UserDataModule(logger, emitter, rce) {
        var _this = this;
        this.isInitialized = false;
        this.warning = 'Missing data access key: call setToken with appropriate parameters';
        this.storage = require('local-storage');
        this.logger = logger;
        this.emitter = emitter;
        this.rce = rce;
        this.initKey();
        this.emitter.on('data:save', function (data) { return _this.save(data); });
    }
    UserDataModule.prototype.load = function (callback) {
        var _this = this;
        this.logger.log('GET user data');
        if (this.isInitialized) {
            if (this.rce.isAvailable()) {
                if (this.rce.isInitialized()) {
                    this.rce.retrieveUserData({ object_id: this.key }, function (obj) {
                        callback(obj && obj.data ? obj.data : null);
                    });
                }
                else {
                    this.emitter.once('rce:ready', function () { return _this.load(callback); });
                }
            }
            else {
                var obj = this.storage.get(this.key);
                callback(obj && obj.data ? obj.data : null);
            }
        }
        else {
            this.logger.error(this.warning);
            callback(null);
        }
    };
    UserDataModule.prototype.save = function (data) {
        this.logger.log('SET user data', data);
        if (typeof data === 'undefined') {
            this.logger.warn('Data is undefined, send null instead if you want to clear user data');
            return;
        }
        if (this.isInitialized) {
            if (this.rce.isInitialized()) {
                this.rce.saveUserData({ object_id: this.key, data: data }, function () { return; });
            }
            else {
                this.storage.set(this.key, { object_id: this.key, data: data });
            }
        }
        else {
            this.logger.error(this.warning);
            return;
        }
    };
    UserDataModule.prototype.initKey = function () {
        try {
            if (s9 && s9.initialParams && s9.initialParams.configFile) {
                this.key = s9.initialParams.configFile.split('/').pop().replace('.json', '');
            }
            else {
                this.key = document.querySelector('head title').innerHTML;
            }
        }
        catch (err) {
            this.logger.error('WidgetsCommon', 'UserDataModule', 'Unable to set user data persistence key');
            this.key = 'unit-test-key';
        }
        this.key = 'userdata:' + this.key;
        this.isInitialized = true;
    };
    return UserDataModule;
})();
var WidgetDataModule = (function () {
    function WidgetDataModule(logger) {
        this.widgetDataPath = '../../widget_data/';
        this.logger = logger;
    }
    WidgetDataModule.prototype.setWidgetDataPath = function (path) {
        this.widgetDataPath = path;
    };
    WidgetDataModule.prototype.load = function (uri, callback, loadAsPlainText) {
        if (loadAsPlainText === void 0) { loadAsPlainText = false; }
        var request = new XMLHttpRequest();
        request.open('GET', this.widgetDataPath + uri, true);
        request.onload = function () {
            if (request.status < 400 && request.responseText) {
                if (loadAsPlainText) {
                    callback(request.responseText);
                }
                else {
                    var parsedJson = JSON.parse(request.responseText);
                    callback(parsedJson);
                }
            }
            else {
                callback(null);
            }
        };
        request.onerror = function (err) { return callback(null); };
        request.send();
    };
    return WidgetDataModule;
})();

},{"eventemitter3":1,"js-logger":2,"local-storage":3}]},{},[6])
//# sourceMappingURL=data:application/json;charset:utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIm5vZGVfbW9kdWxlcy9ncnVudC1icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyaWZ5L25vZGVfbW9kdWxlcy9icm93c2VyLXBhY2svX3ByZWx1ZGUuanMiLCJub2RlX21vZHVsZXMvZXZlbnRlbWl0dGVyMy9pbmRleC5qcyIsIm5vZGVfbW9kdWxlcy9qcy1sb2dnZXIvc3JjL2xvZ2dlci5qcyIsIm5vZGVfbW9kdWxlcy9sb2NhbC1zdG9yYWdlL2xvY2FsLXN0b3JhZ2UuanMiLCJub2RlX21vZHVsZXMvbG9jYWwtc3RvcmFnZS9zdHViLmpzIiwibm9kZV9tb2R1bGVzL2xvY2FsLXN0b3JhZ2UvdHJhY2tpbmcuanMiLCJ0ZW1wL2hhYi1tdGwtd2lkZ2V0cy1jb21tb24uanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7QUNBQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOztBQ3RRQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUMxUEE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7Ozs7QUMxQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7QUNoQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBOzs7O0FDckRBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EiLCJmaWxlIjoiZ2VuZXJhdGVkLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXNDb250ZW50IjpbIihmdW5jdGlvbiBlKHQsbixyKXtmdW5jdGlvbiBzKG8sdSl7aWYoIW5bb10pe2lmKCF0W29dKXt2YXIgYT10eXBlb2YgcmVxdWlyZT09XCJmdW5jdGlvblwiJiZyZXF1aXJlO2lmKCF1JiZhKXJldHVybiBhKG8sITApO2lmKGkpcmV0dXJuIGkobywhMCk7dmFyIGY9bmV3IEVycm9yKFwiQ2Fubm90IGZpbmQgbW9kdWxlICdcIitvK1wiJ1wiKTt0aHJvdyBmLmNvZGU9XCJNT0RVTEVfTk9UX0ZPVU5EXCIsZn12YXIgbD1uW29dPXtleHBvcnRzOnt9fTt0W29dWzBdLmNhbGwobC5leHBvcnRzLGZ1bmN0aW9uKGUpe3ZhciBuPXRbb11bMV1bZV07cmV0dXJuIHMobj9uOmUpfSxsLGwuZXhwb3J0cyxlLHQsbixyKX1yZXR1cm4gbltvXS5leHBvcnRzfXZhciBpPXR5cGVvZiByZXF1aXJlPT1cImZ1bmN0aW9uXCImJnJlcXVpcmU7Zm9yKHZhciBvPTA7bzxyLmxlbmd0aDtvKyspcyhyW29dKTtyZXR1cm4gc30pIiwiJ3VzZSBzdHJpY3QnO1xuXG4vL1xuLy8gV2Ugc3RvcmUgb3VyIEVFIG9iamVjdHMgaW4gYSBwbGFpbiBvYmplY3Qgd2hvc2UgcHJvcGVydGllcyBhcmUgZXZlbnQgbmFtZXMuXG4vLyBJZiBgT2JqZWN0LmNyZWF0ZShudWxsKWAgaXMgbm90IHN1cHBvcnRlZCB3ZSBwcmVmaXggdGhlIGV2ZW50IG5hbWVzIHdpdGggYVxuLy8gYH5gIHRvIG1ha2Ugc3VyZSB0aGF0IHRoZSBidWlsdC1pbiBvYmplY3QgcHJvcGVydGllcyBhcmUgbm90IG92ZXJyaWRkZW4gb3Jcbi8vIHVzZWQgYXMgYW4gYXR0YWNrIHZlY3Rvci5cbi8vIFdlIGFsc28gYXNzdW1lIHRoYXQgYE9iamVjdC5jcmVhdGUobnVsbClgIGlzIGF2YWlsYWJsZSB3aGVuIHRoZSBldmVudCBuYW1lXG4vLyBpcyBhbiBFUzYgU3ltYm9sLlxuLy9cbnZhciBwcmVmaXggPSB0eXBlb2YgT2JqZWN0LmNyZWF0ZSAhPT0gJ2Z1bmN0aW9uJyA/ICd+JyA6IGZhbHNlO1xuXG4vKipcbiAqIFJlcHJlc2VudGF0aW9uIG9mIGEgc2luZ2xlIEV2ZW50RW1pdHRlciBmdW5jdGlvbi5cbiAqXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmbiBFdmVudCBoYW5kbGVyIHRvIGJlIGNhbGxlZC5cbiAqIEBwYXJhbSB7TWl4ZWR9IGNvbnRleHQgQ29udGV4dCBmb3IgZnVuY3Rpb24gZXhlY3V0aW9uLlxuICogQHBhcmFtIHtCb29sZWFufSBvbmNlIE9ubHkgZW1pdCBvbmNlXG4gKiBAYXBpIHByaXZhdGVcbiAqL1xuZnVuY3Rpb24gRUUoZm4sIGNvbnRleHQsIG9uY2UpIHtcbiAgdGhpcy5mbiA9IGZuO1xuICB0aGlzLmNvbnRleHQgPSBjb250ZXh0O1xuICB0aGlzLm9uY2UgPSBvbmNlIHx8IGZhbHNlO1xufVxuXG4vKipcbiAqIE1pbmltYWwgRXZlbnRFbWl0dGVyIGludGVyZmFjZSB0aGF0IGlzIG1vbGRlZCBhZ2FpbnN0IHRoZSBOb2RlLmpzXG4gKiBFdmVudEVtaXR0ZXIgaW50ZXJmYWNlLlxuICpcbiAqIEBjb25zdHJ1Y3RvclxuICogQGFwaSBwdWJsaWNcbiAqL1xuZnVuY3Rpb24gRXZlbnRFbWl0dGVyKCkgeyAvKiBOb3RoaW5nIHRvIHNldCAqLyB9XG5cbi8qKlxuICogSG9sZHMgdGhlIGFzc2lnbmVkIEV2ZW50RW1pdHRlcnMgYnkgbmFtZS5cbiAqXG4gKiBAdHlwZSB7T2JqZWN0fVxuICogQHByaXZhdGVcbiAqL1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5fZXZlbnRzID0gdW5kZWZpbmVkO1xuXG4vKipcbiAqIFJldHVybiBhIGxpc3Qgb2YgYXNzaWduZWQgZXZlbnQgbGlzdGVuZXJzLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBldmVudCBUaGUgZXZlbnRzIHRoYXQgc2hvdWxkIGJlIGxpc3RlZC5cbiAqIEBwYXJhbSB7Qm9vbGVhbn0gZXhpc3RzIFdlIG9ubHkgbmVlZCB0byBrbm93IGlmIHRoZXJlIGFyZSBsaXN0ZW5lcnMuXG4gKiBAcmV0dXJucyB7QXJyYXl8Qm9vbGVhbn1cbiAqIEBhcGkgcHVibGljXG4gKi9cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUubGlzdGVuZXJzID0gZnVuY3Rpb24gbGlzdGVuZXJzKGV2ZW50LCBleGlzdHMpIHtcbiAgdmFyIGV2dCA9IHByZWZpeCA/IHByZWZpeCArIGV2ZW50IDogZXZlbnRcbiAgICAsIGF2YWlsYWJsZSA9IHRoaXMuX2V2ZW50cyAmJiB0aGlzLl9ldmVudHNbZXZ0XTtcblxuICBpZiAoZXhpc3RzKSByZXR1cm4gISFhdmFpbGFibGU7XG4gIGlmICghYXZhaWxhYmxlKSByZXR1cm4gW107XG4gIGlmIChhdmFpbGFibGUuZm4pIHJldHVybiBbYXZhaWxhYmxlLmZuXTtcblxuICBmb3IgKHZhciBpID0gMCwgbCA9IGF2YWlsYWJsZS5sZW5ndGgsIGVlID0gbmV3IEFycmF5KGwpOyBpIDwgbDsgaSsrKSB7XG4gICAgZWVbaV0gPSBhdmFpbGFibGVbaV0uZm47XG4gIH1cblxuICByZXR1cm4gZWU7XG59O1xuXG4vKipcbiAqIEVtaXQgYW4gZXZlbnQgdG8gYWxsIHJlZ2lzdGVyZWQgZXZlbnQgbGlzdGVuZXJzLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBldmVudCBUaGUgbmFtZSBvZiB0aGUgZXZlbnQuXG4gKiBAcmV0dXJucyB7Qm9vbGVhbn0gSW5kaWNhdGlvbiBpZiB3ZSd2ZSBlbWl0dGVkIGFuIGV2ZW50LlxuICogQGFwaSBwdWJsaWNcbiAqL1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5lbWl0ID0gZnVuY3Rpb24gZW1pdChldmVudCwgYTEsIGEyLCBhMywgYTQsIGE1KSB7XG4gIHZhciBldnQgPSBwcmVmaXggPyBwcmVmaXggKyBldmVudCA6IGV2ZW50O1xuXG4gIGlmICghdGhpcy5fZXZlbnRzIHx8ICF0aGlzLl9ldmVudHNbZXZ0XSkgcmV0dXJuIGZhbHNlO1xuXG4gIHZhciBsaXN0ZW5lcnMgPSB0aGlzLl9ldmVudHNbZXZ0XVxuICAgICwgbGVuID0gYXJndW1lbnRzLmxlbmd0aFxuICAgICwgYXJnc1xuICAgICwgaTtcblxuICBpZiAoJ2Z1bmN0aW9uJyA9PT0gdHlwZW9mIGxpc3RlbmVycy5mbikge1xuICAgIGlmIChsaXN0ZW5lcnMub25jZSkgdGhpcy5yZW1vdmVMaXN0ZW5lcihldmVudCwgbGlzdGVuZXJzLmZuLCB1bmRlZmluZWQsIHRydWUpO1xuXG4gICAgc3dpdGNoIChsZW4pIHtcbiAgICAgIGNhc2UgMTogcmV0dXJuIGxpc3RlbmVycy5mbi5jYWxsKGxpc3RlbmVycy5jb250ZXh0KSwgdHJ1ZTtcbiAgICAgIGNhc2UgMjogcmV0dXJuIGxpc3RlbmVycy5mbi5jYWxsKGxpc3RlbmVycy5jb250ZXh0LCBhMSksIHRydWU7XG4gICAgICBjYXNlIDM6IHJldHVybiBsaXN0ZW5lcnMuZm4uY2FsbChsaXN0ZW5lcnMuY29udGV4dCwgYTEsIGEyKSwgdHJ1ZTtcbiAgICAgIGNhc2UgNDogcmV0dXJuIGxpc3RlbmVycy5mbi5jYWxsKGxpc3RlbmVycy5jb250ZXh0LCBhMSwgYTIsIGEzKSwgdHJ1ZTtcbiAgICAgIGNhc2UgNTogcmV0dXJuIGxpc3RlbmVycy5mbi5jYWxsKGxpc3RlbmVycy5jb250ZXh0LCBhMSwgYTIsIGEzLCBhNCksIHRydWU7XG4gICAgICBjYXNlIDY6IHJldHVybiBsaXN0ZW5lcnMuZm4uY2FsbChsaXN0ZW5lcnMuY29udGV4dCwgYTEsIGEyLCBhMywgYTQsIGE1KSwgdHJ1ZTtcbiAgICB9XG5cbiAgICBmb3IgKGkgPSAxLCBhcmdzID0gbmV3IEFycmF5KGxlbiAtMSk7IGkgPCBsZW47IGkrKykge1xuICAgICAgYXJnc1tpIC0gMV0gPSBhcmd1bWVudHNbaV07XG4gICAgfVxuXG4gICAgbGlzdGVuZXJzLmZuLmFwcGx5KGxpc3RlbmVycy5jb250ZXh0LCBhcmdzKTtcbiAgfSBlbHNlIHtcbiAgICB2YXIgbGVuZ3RoID0gbGlzdGVuZXJzLmxlbmd0aFxuICAgICAgLCBqO1xuXG4gICAgZm9yIChpID0gMDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICBpZiAobGlzdGVuZXJzW2ldLm9uY2UpIHRoaXMucmVtb3ZlTGlzdGVuZXIoZXZlbnQsIGxpc3RlbmVyc1tpXS5mbiwgdW5kZWZpbmVkLCB0cnVlKTtcblxuICAgICAgc3dpdGNoIChsZW4pIHtcbiAgICAgICAgY2FzZSAxOiBsaXN0ZW5lcnNbaV0uZm4uY2FsbChsaXN0ZW5lcnNbaV0uY29udGV4dCk7IGJyZWFrO1xuICAgICAgICBjYXNlIDI6IGxpc3RlbmVyc1tpXS5mbi5jYWxsKGxpc3RlbmVyc1tpXS5jb250ZXh0LCBhMSk7IGJyZWFrO1xuICAgICAgICBjYXNlIDM6IGxpc3RlbmVyc1tpXS5mbi5jYWxsKGxpc3RlbmVyc1tpXS5jb250ZXh0LCBhMSwgYTIpOyBicmVhaztcbiAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICBpZiAoIWFyZ3MpIGZvciAoaiA9IDEsIGFyZ3MgPSBuZXcgQXJyYXkobGVuIC0xKTsgaiA8IGxlbjsgaisrKSB7XG4gICAgICAgICAgICBhcmdzW2ogLSAxXSA9IGFyZ3VtZW50c1tqXTtcbiAgICAgICAgICB9XG5cbiAgICAgICAgICBsaXN0ZW5lcnNbaV0uZm4uYXBwbHkobGlzdGVuZXJzW2ldLmNvbnRleHQsIGFyZ3MpO1xuICAgICAgfVxuICAgIH1cbiAgfVxuXG4gIHJldHVybiB0cnVlO1xufTtcblxuLyoqXG4gKiBSZWdpc3RlciBhIG5ldyBFdmVudExpc3RlbmVyIGZvciB0aGUgZ2l2ZW4gZXZlbnQuXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IGV2ZW50IE5hbWUgb2YgdGhlIGV2ZW50LlxuICogQHBhcmFtIHtGdW5jdG9ufSBmbiBDYWxsYmFjayBmdW5jdGlvbi5cbiAqIEBwYXJhbSB7TWl4ZWR9IGNvbnRleHQgVGhlIGNvbnRleHQgb2YgdGhlIGZ1bmN0aW9uLlxuICogQGFwaSBwdWJsaWNcbiAqL1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5vbiA9IGZ1bmN0aW9uIG9uKGV2ZW50LCBmbiwgY29udGV4dCkge1xuICB2YXIgbGlzdGVuZXIgPSBuZXcgRUUoZm4sIGNvbnRleHQgfHwgdGhpcylcbiAgICAsIGV2dCA9IHByZWZpeCA/IHByZWZpeCArIGV2ZW50IDogZXZlbnQ7XG5cbiAgaWYgKCF0aGlzLl9ldmVudHMpIHRoaXMuX2V2ZW50cyA9IHByZWZpeCA/IHt9IDogT2JqZWN0LmNyZWF0ZShudWxsKTtcbiAgaWYgKCF0aGlzLl9ldmVudHNbZXZ0XSkgdGhpcy5fZXZlbnRzW2V2dF0gPSBsaXN0ZW5lcjtcbiAgZWxzZSB7XG4gICAgaWYgKCF0aGlzLl9ldmVudHNbZXZ0XS5mbikgdGhpcy5fZXZlbnRzW2V2dF0ucHVzaChsaXN0ZW5lcik7XG4gICAgZWxzZSB0aGlzLl9ldmVudHNbZXZ0XSA9IFtcbiAgICAgIHRoaXMuX2V2ZW50c1tldnRdLCBsaXN0ZW5lclxuICAgIF07XG4gIH1cblxuICByZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICogQWRkIGFuIEV2ZW50TGlzdGVuZXIgdGhhdCdzIG9ubHkgY2FsbGVkIG9uY2UuXG4gKlxuICogQHBhcmFtIHtTdHJpbmd9IGV2ZW50IE5hbWUgb2YgdGhlIGV2ZW50LlxuICogQHBhcmFtIHtGdW5jdGlvbn0gZm4gQ2FsbGJhY2sgZnVuY3Rpb24uXG4gKiBAcGFyYW0ge01peGVkfSBjb250ZXh0IFRoZSBjb250ZXh0IG9mIHRoZSBmdW5jdGlvbi5cbiAqIEBhcGkgcHVibGljXG4gKi9cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub25jZSA9IGZ1bmN0aW9uIG9uY2UoZXZlbnQsIGZuLCBjb250ZXh0KSB7XG4gIHZhciBsaXN0ZW5lciA9IG5ldyBFRShmbiwgY29udGV4dCB8fCB0aGlzLCB0cnVlKVxuICAgICwgZXZ0ID0gcHJlZml4ID8gcHJlZml4ICsgZXZlbnQgOiBldmVudDtcblxuICBpZiAoIXRoaXMuX2V2ZW50cykgdGhpcy5fZXZlbnRzID0gcHJlZml4ID8ge30gOiBPYmplY3QuY3JlYXRlKG51bGwpO1xuICBpZiAoIXRoaXMuX2V2ZW50c1tldnRdKSB0aGlzLl9ldmVudHNbZXZ0XSA9IGxpc3RlbmVyO1xuICBlbHNlIHtcbiAgICBpZiAoIXRoaXMuX2V2ZW50c1tldnRdLmZuKSB0aGlzLl9ldmVudHNbZXZ0XS5wdXNoKGxpc3RlbmVyKTtcbiAgICBlbHNlIHRoaXMuX2V2ZW50c1tldnRdID0gW1xuICAgICAgdGhpcy5fZXZlbnRzW2V2dF0sIGxpc3RlbmVyXG4gICAgXTtcbiAgfVxuXG4gIHJldHVybiB0aGlzO1xufTtcblxuLyoqXG4gKiBSZW1vdmUgZXZlbnQgbGlzdGVuZXJzLlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBldmVudCBUaGUgZXZlbnQgd2Ugd2FudCB0byByZW1vdmUuXG4gKiBAcGFyYW0ge0Z1bmN0aW9ufSBmbiBUaGUgbGlzdGVuZXIgdGhhdCB3ZSBuZWVkIHRvIGZpbmQuXG4gKiBAcGFyYW0ge01peGVkfSBjb250ZXh0IE9ubHkgcmVtb3ZlIGxpc3RlbmVycyBtYXRjaGluZyB0aGlzIGNvbnRleHQuXG4gKiBAcGFyYW0ge0Jvb2xlYW59IG9uY2UgT25seSByZW1vdmUgb25jZSBsaXN0ZW5lcnMuXG4gKiBAYXBpIHB1YmxpY1xuICovXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUxpc3RlbmVyID0gZnVuY3Rpb24gcmVtb3ZlTGlzdGVuZXIoZXZlbnQsIGZuLCBjb250ZXh0LCBvbmNlKSB7XG4gIHZhciBldnQgPSBwcmVmaXggPyBwcmVmaXggKyBldmVudCA6IGV2ZW50O1xuXG4gIGlmICghdGhpcy5fZXZlbnRzIHx8ICF0aGlzLl9ldmVudHNbZXZ0XSkgcmV0dXJuIHRoaXM7XG5cbiAgdmFyIGxpc3RlbmVycyA9IHRoaXMuX2V2ZW50c1tldnRdXG4gICAgLCBldmVudHMgPSBbXTtcblxuICBpZiAoZm4pIHtcbiAgICBpZiAobGlzdGVuZXJzLmZuKSB7XG4gICAgICBpZiAoXG4gICAgICAgICAgIGxpc3RlbmVycy5mbiAhPT0gZm5cbiAgICAgICAgfHwgKG9uY2UgJiYgIWxpc3RlbmVycy5vbmNlKVxuICAgICAgICB8fCAoY29udGV4dCAmJiBsaXN0ZW5lcnMuY29udGV4dCAhPT0gY29udGV4dClcbiAgICAgICkge1xuICAgICAgICBldmVudHMucHVzaChsaXN0ZW5lcnMpO1xuICAgICAgfVxuICAgIH0gZWxzZSB7XG4gICAgICBmb3IgKHZhciBpID0gMCwgbGVuZ3RoID0gbGlzdGVuZXJzLmxlbmd0aDsgaSA8IGxlbmd0aDsgaSsrKSB7XG4gICAgICAgIGlmIChcbiAgICAgICAgICAgICBsaXN0ZW5lcnNbaV0uZm4gIT09IGZuXG4gICAgICAgICAgfHwgKG9uY2UgJiYgIWxpc3RlbmVyc1tpXS5vbmNlKVxuICAgICAgICAgIHx8IChjb250ZXh0ICYmIGxpc3RlbmVyc1tpXS5jb250ZXh0ICE9PSBjb250ZXh0KVxuICAgICAgICApIHtcbiAgICAgICAgICBldmVudHMucHVzaChsaXN0ZW5lcnNbaV0pO1xuICAgICAgICB9XG4gICAgICB9XG4gICAgfVxuICB9XG5cbiAgLy9cbiAgLy8gUmVzZXQgdGhlIGFycmF5LCBvciByZW1vdmUgaXQgY29tcGxldGVseSBpZiB3ZSBoYXZlIG5vIG1vcmUgbGlzdGVuZXJzLlxuICAvL1xuICBpZiAoZXZlbnRzLmxlbmd0aCkge1xuICAgIHRoaXMuX2V2ZW50c1tldnRdID0gZXZlbnRzLmxlbmd0aCA9PT0gMSA/IGV2ZW50c1swXSA6IGV2ZW50cztcbiAgfSBlbHNlIHtcbiAgICBkZWxldGUgdGhpcy5fZXZlbnRzW2V2dF07XG4gIH1cblxuICByZXR1cm4gdGhpcztcbn07XG5cbi8qKlxuICogUmVtb3ZlIGFsbCBsaXN0ZW5lcnMgb3Igb25seSB0aGUgbGlzdGVuZXJzIGZvciB0aGUgc3BlY2lmaWVkIGV2ZW50LlxuICpcbiAqIEBwYXJhbSB7U3RyaW5nfSBldmVudCBUaGUgZXZlbnQgd2FudCB0byByZW1vdmUgYWxsIGxpc3RlbmVycyBmb3IuXG4gKiBAYXBpIHB1YmxpY1xuICovXG5FdmVudEVtaXR0ZXIucHJvdG90eXBlLnJlbW92ZUFsbExpc3RlbmVycyA9IGZ1bmN0aW9uIHJlbW92ZUFsbExpc3RlbmVycyhldmVudCkge1xuICBpZiAoIXRoaXMuX2V2ZW50cykgcmV0dXJuIHRoaXM7XG5cbiAgaWYgKGV2ZW50KSBkZWxldGUgdGhpcy5fZXZlbnRzW3ByZWZpeCA/IHByZWZpeCArIGV2ZW50IDogZXZlbnRdO1xuICBlbHNlIHRoaXMuX2V2ZW50cyA9IHByZWZpeCA/IHt9IDogT2JqZWN0LmNyZWF0ZShudWxsKTtcblxuICByZXR1cm4gdGhpcztcbn07XG5cbi8vXG4vLyBBbGlhcyBtZXRob2RzIG5hbWVzIGJlY2F1c2UgcGVvcGxlIHJvbGwgbGlrZSB0aGF0LlxuLy9cbkV2ZW50RW1pdHRlci5wcm90b3R5cGUub2ZmID0gRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5yZW1vdmVMaXN0ZW5lcjtcbkV2ZW50RW1pdHRlci5wcm90b3R5cGUuYWRkTGlzdGVuZXIgPSBFdmVudEVtaXR0ZXIucHJvdG90eXBlLm9uO1xuXG4vL1xuLy8gVGhpcyBmdW5jdGlvbiBkb2Vzbid0IGFwcGx5IGFueW1vcmUuXG4vL1xuRXZlbnRFbWl0dGVyLnByb3RvdHlwZS5zZXRNYXhMaXN0ZW5lcnMgPSBmdW5jdGlvbiBzZXRNYXhMaXN0ZW5lcnMoKSB7XG4gIHJldHVybiB0aGlzO1xufTtcblxuLy9cbi8vIEV4cG9zZSB0aGUgcHJlZml4LlxuLy9cbkV2ZW50RW1pdHRlci5wcmVmaXhlZCA9IHByZWZpeDtcblxuLy9cbi8vIEV4cG9zZSB0aGUgbW9kdWxlLlxuLy9cbmlmICgndW5kZWZpbmVkJyAhPT0gdHlwZW9mIG1vZHVsZSkge1xuICBtb2R1bGUuZXhwb3J0cyA9IEV2ZW50RW1pdHRlcjtcbn1cbiIsIi8qIVxyXG4gKiBqcy1sb2dnZXIgLSBodHRwOi8vZ2l0aHViLmNvbS9qb25ueXJlZXZlcy9qcy1sb2dnZXJcclxuICogSm9ubnkgUmVldmVzLCBodHRwOi8vam9ubnlyZWV2ZXMuY28udWsvXHJcbiAqIGpzLWxvZ2dlciBtYXkgYmUgZnJlZWx5IGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBNSVQgbGljZW5zZS5cclxuICovXHJcbihmdW5jdGlvbiAoZ2xvYmFsKSB7XHJcblx0XCJ1c2Ugc3RyaWN0XCI7XHJcblxyXG5cdC8vIFRvcCBsZXZlbCBtb2R1bGUgZm9yIHRoZSBnbG9iYWwsIHN0YXRpYyBsb2dnZXIgaW5zdGFuY2UuXHJcblx0dmFyIExvZ2dlciA9IHsgfTtcclxuXHJcblx0Ly8gRm9yIHRob3NlIHRoYXQgYXJlIGF0IGhvbWUgdGhhdCBhcmUga2VlcGluZyBzY29yZS5cclxuXHRMb2dnZXIuVkVSU0lPTiA9IFwiMS4yLjBcIjtcclxuXHJcblx0Ly8gRnVuY3Rpb24gd2hpY2ggaGFuZGxlcyBhbGwgaW5jb21pbmcgbG9nIG1lc3NhZ2VzLlxyXG5cdHZhciBsb2dIYW5kbGVyO1xyXG5cclxuXHQvLyBNYXAgb2YgQ29udGV4dHVhbExvZ2dlciBpbnN0YW5jZXMgYnkgbmFtZTsgdXNlZCBieSBMb2dnZXIuZ2V0KCkgdG8gcmV0dXJuIHRoZSBzYW1lIG5hbWVkIGluc3RhbmNlLlxyXG5cdHZhciBjb250ZXh0dWFsTG9nZ2Vyc0J5TmFtZU1hcCA9IHt9O1xyXG5cclxuXHQvLyBQb2x5ZmlsbCBmb3IgRVM1J3MgRnVuY3Rpb24uYmluZC5cclxuXHR2YXIgYmluZCA9IGZ1bmN0aW9uKHNjb3BlLCBmdW5jKSB7XHJcblx0XHRyZXR1cm4gZnVuY3Rpb24oKSB7XHJcblx0XHRcdHJldHVybiBmdW5jLmFwcGx5KHNjb3BlLCBhcmd1bWVudHMpO1xyXG5cdFx0fTtcclxuXHR9O1xyXG5cclxuXHQvLyBTdXBlciBleGNpdGluZyBvYmplY3QgbWVyZ2VyLW1hdHJvbiA5MDAwIGFkZGluZyBhbm90aGVyIDEwMCBieXRlcyB0byB5b3VyIGRvd25sb2FkLlxyXG5cdHZhciBtZXJnZSA9IGZ1bmN0aW9uICgpIHtcclxuXHRcdHZhciBhcmdzID0gYXJndW1lbnRzLCB0YXJnZXQgPSBhcmdzWzBdLCBrZXksIGk7XHJcblx0XHRmb3IgKGkgPSAxOyBpIDwgYXJncy5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRmb3IgKGtleSBpbiBhcmdzW2ldKSB7XHJcblx0XHRcdFx0aWYgKCEoa2V5IGluIHRhcmdldCkgJiYgYXJnc1tpXS5oYXNPd25Qcm9wZXJ0eShrZXkpKSB7XHJcblx0XHRcdFx0XHR0YXJnZXRba2V5XSA9IGFyZ3NbaV1ba2V5XTtcclxuXHRcdFx0XHR9XHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHRcdHJldHVybiB0YXJnZXQ7XHJcblx0fTtcclxuXHJcblx0Ly8gSGVscGVyIHRvIGRlZmluZSBhIGxvZ2dpbmcgbGV2ZWwgb2JqZWN0OyBoZWxwcyB3aXRoIG9wdGltaXNhdGlvbi5cclxuXHR2YXIgZGVmaW5lTG9nTGV2ZWwgPSBmdW5jdGlvbih2YWx1ZSwgbmFtZSkge1xyXG5cdFx0cmV0dXJuIHsgdmFsdWU6IHZhbHVlLCBuYW1lOiBuYW1lIH07XHJcblx0fTtcclxuXHJcblx0Ly8gUHJlZGVmaW5lZCBsb2dnaW5nIGxldmVscy5cclxuXHRMb2dnZXIuREVCVUcgPSBkZWZpbmVMb2dMZXZlbCgxLCAnREVCVUcnKTtcclxuXHRMb2dnZXIuSU5GTyA9IGRlZmluZUxvZ0xldmVsKDIsICdJTkZPJyk7XHJcblx0TG9nZ2VyLlRJTUUgPSBkZWZpbmVMb2dMZXZlbCgzLCAnVElNRScpO1xyXG5cdExvZ2dlci5XQVJOID0gZGVmaW5lTG9nTGV2ZWwoNCwgJ1dBUk4nKTtcclxuXHRMb2dnZXIuRVJST1IgPSBkZWZpbmVMb2dMZXZlbCg4LCAnRVJST1InKTtcclxuXHRMb2dnZXIuT0ZGID0gZGVmaW5lTG9nTGV2ZWwoOTksICdPRkYnKTtcclxuXHJcblx0Ly8gSW5uZXIgY2xhc3Mgd2hpY2ggcGVyZm9ybXMgdGhlIGJ1bGsgb2YgdGhlIHdvcms7IENvbnRleHR1YWxMb2dnZXIgaW5zdGFuY2VzIGNhbiBiZSBjb25maWd1cmVkIGluZGVwZW5kZW50bHlcclxuXHQvLyBvZiBlYWNoIG90aGVyLlxyXG5cdHZhciBDb250ZXh0dWFsTG9nZ2VyID0gZnVuY3Rpb24oZGVmYXVsdENvbnRleHQpIHtcclxuXHRcdHRoaXMuY29udGV4dCA9IGRlZmF1bHRDb250ZXh0O1xyXG5cdFx0dGhpcy5zZXRMZXZlbChkZWZhdWx0Q29udGV4dC5maWx0ZXJMZXZlbCk7XHJcblx0XHR0aGlzLmxvZyA9IHRoaXMuaW5mbzsgIC8vIENvbnZlbmllbmNlIGFsaWFzLlxyXG5cdH07XHJcblxyXG5cdENvbnRleHR1YWxMb2dnZXIucHJvdG90eXBlID0ge1xyXG5cdFx0Ly8gQ2hhbmdlcyB0aGUgY3VycmVudCBsb2dnaW5nIGxldmVsIGZvciB0aGUgbG9nZ2luZyBpbnN0YW5jZS5cclxuXHRcdHNldExldmVsOiBmdW5jdGlvbiAobmV3TGV2ZWwpIHtcclxuXHRcdFx0Ly8gRW5zdXJlIHRoZSBzdXBwbGllZCBMZXZlbCBvYmplY3QgbG9va3MgdmFsaWQuXHJcblx0XHRcdGlmIChuZXdMZXZlbCAmJiBcInZhbHVlXCIgaW4gbmV3TGV2ZWwpIHtcclxuXHRcdFx0XHR0aGlzLmNvbnRleHQuZmlsdGVyTGV2ZWwgPSBuZXdMZXZlbDtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHQvLyBJcyB0aGUgbG9nZ2VyIGNvbmZpZ3VyZWQgdG8gb3V0cHV0IG1lc3NhZ2VzIGF0IHRoZSBzdXBwbGllZCBsZXZlbD9cclxuXHRcdGVuYWJsZWRGb3I6IGZ1bmN0aW9uIChsdmwpIHtcclxuXHRcdFx0dmFyIGZpbHRlckxldmVsID0gdGhpcy5jb250ZXh0LmZpbHRlckxldmVsO1xyXG5cdFx0XHRyZXR1cm4gbHZsLnZhbHVlID49IGZpbHRlckxldmVsLnZhbHVlO1xyXG5cdFx0fSxcclxuXHJcblx0XHRkZWJ1ZzogZnVuY3Rpb24gKCkge1xyXG5cdFx0XHR0aGlzLmludm9rZShMb2dnZXIuREVCVUcsIGFyZ3VtZW50cyk7XHJcblx0XHR9LFxyXG5cclxuXHRcdGluZm86IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0dGhpcy5pbnZva2UoTG9nZ2VyLklORk8sIGFyZ3VtZW50cyk7XHJcblx0XHR9LFxyXG5cclxuXHRcdHdhcm46IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0dGhpcy5pbnZva2UoTG9nZ2VyLldBUk4sIGFyZ3VtZW50cyk7XHJcblx0XHR9LFxyXG5cclxuXHRcdGVycm9yOiBmdW5jdGlvbiAoKSB7XHJcblx0XHRcdHRoaXMuaW52b2tlKExvZ2dlci5FUlJPUiwgYXJndW1lbnRzKTtcclxuXHRcdH0sXHJcblxyXG5cdFx0dGltZTogZnVuY3Rpb24gKGxhYmVsKSB7XHJcblx0XHRcdGlmICh0eXBlb2YgbGFiZWwgPT09ICdzdHJpbmcnICYmIGxhYmVsLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0XHR0aGlzLmludm9rZShMb2dnZXIuVElNRSwgWyBsYWJlbCwgJ3N0YXJ0JyBdKTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHR0aW1lRW5kOiBmdW5jdGlvbiAobGFiZWwpIHtcclxuXHRcdFx0aWYgKHR5cGVvZiBsYWJlbCA9PT0gJ3N0cmluZycgJiYgbGFiZWwubGVuZ3RoID4gMCkge1xyXG5cdFx0XHRcdHRoaXMuaW52b2tlKExvZ2dlci5USU1FLCBbIGxhYmVsLCAnZW5kJyBdKTtcclxuXHRcdFx0fVxyXG5cdFx0fSxcclxuXHJcblx0XHQvLyBJbnZva2VzIHRoZSBsb2dnZXIgY2FsbGJhY2sgaWYgaXQncyBub3QgYmVpbmcgZmlsdGVyZWQuXHJcblx0XHRpbnZva2U6IGZ1bmN0aW9uIChsZXZlbCwgbXNnQXJncykge1xyXG5cdFx0XHRpZiAobG9nSGFuZGxlciAmJiB0aGlzLmVuYWJsZWRGb3IobGV2ZWwpKSB7XHJcblx0XHRcdFx0bG9nSGFuZGxlcihtc2dBcmdzLCBtZXJnZSh7IGxldmVsOiBsZXZlbCB9LCB0aGlzLmNvbnRleHQpKTtcclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdH07XHJcblxyXG5cdC8vIFByb3RlY3RlZCBpbnN0YW5jZSB3aGljaCBhbGwgY2FsbHMgdG8gdGhlIHRvIGxldmVsIGBMb2dnZXJgIG1vZHVsZSB3aWxsIGJlIHJvdXRlZCB0aHJvdWdoLlxyXG5cdHZhciBnbG9iYWxMb2dnZXIgPSBuZXcgQ29udGV4dHVhbExvZ2dlcih7IGZpbHRlckxldmVsOiBMb2dnZXIuT0ZGIH0pO1xyXG5cclxuXHQvLyBDb25maWd1cmUgdGhlIGdsb2JhbCBMb2dnZXIgaW5zdGFuY2UuXHJcblx0KGZ1bmN0aW9uKCkge1xyXG5cdFx0Ly8gU2hvcnRjdXQgZm9yIG9wdGltaXNlcnMuXHJcblx0XHR2YXIgTCA9IExvZ2dlcjtcclxuXHJcblx0XHRMLmVuYWJsZWRGb3IgPSBiaW5kKGdsb2JhbExvZ2dlciwgZ2xvYmFsTG9nZ2VyLmVuYWJsZWRGb3IpO1xyXG5cdFx0TC5kZWJ1ZyA9IGJpbmQoZ2xvYmFsTG9nZ2VyLCBnbG9iYWxMb2dnZXIuZGVidWcpO1xyXG5cdFx0TC50aW1lID0gYmluZChnbG9iYWxMb2dnZXIsIGdsb2JhbExvZ2dlci50aW1lKTtcclxuXHRcdEwudGltZUVuZCA9IGJpbmQoZ2xvYmFsTG9nZ2VyLCBnbG9iYWxMb2dnZXIudGltZUVuZCk7XHJcblx0XHRMLmluZm8gPSBiaW5kKGdsb2JhbExvZ2dlciwgZ2xvYmFsTG9nZ2VyLmluZm8pO1xyXG5cdFx0TC53YXJuID0gYmluZChnbG9iYWxMb2dnZXIsIGdsb2JhbExvZ2dlci53YXJuKTtcclxuXHRcdEwuZXJyb3IgPSBiaW5kKGdsb2JhbExvZ2dlciwgZ2xvYmFsTG9nZ2VyLmVycm9yKTtcclxuXHJcblx0XHQvLyBEb24ndCBmb3JnZXQgdGhlIGNvbnZlbmllbmNlIGFsaWFzIVxyXG5cdFx0TC5sb2cgPSBMLmluZm87XHJcblx0fSgpKTtcclxuXHJcblx0Ly8gU2V0IHRoZSBnbG9iYWwgbG9nZ2luZyBoYW5kbGVyLiAgVGhlIHN1cHBsaWVkIGZ1bmN0aW9uIHNob3VsZCBleHBlY3QgdHdvIGFyZ3VtZW50cywgdGhlIGZpcnN0IGJlaW5nIGFuIGFyZ3VtZW50c1xyXG5cdC8vIG9iamVjdCB3aXRoIHRoZSBzdXBwbGllZCBsb2cgbWVzc2FnZXMgYW5kIHRoZSBzZWNvbmQgYmVpbmcgYSBjb250ZXh0IG9iamVjdCB3aGljaCBjb250YWlucyBhIGhhc2ggb2Ygc3RhdGVmdWxcclxuXHQvLyBwYXJhbWV0ZXJzIHdoaWNoIHRoZSBsb2dnaW5nIGZ1bmN0aW9uIGNhbiBjb25zdW1lLlxyXG5cdExvZ2dlci5zZXRIYW5kbGVyID0gZnVuY3Rpb24gKGZ1bmMpIHtcclxuXHRcdGxvZ0hhbmRsZXIgPSBmdW5jO1xyXG5cdH07XHJcblxyXG5cdC8vIFNldHMgdGhlIGdsb2JhbCBsb2dnaW5nIGZpbHRlciBsZXZlbCB3aGljaCBhcHBsaWVzIHRvICphbGwqIHByZXZpb3VzbHkgcmVnaXN0ZXJlZCwgYW5kIGZ1dHVyZSBMb2dnZXIgaW5zdGFuY2VzLlxyXG5cdC8vIChub3RlIHRoYXQgbmFtZWQgbG9nZ2VycyAocmV0cmlldmVkIHZpYSBgTG9nZ2VyLmdldGApIGNhbiBiZSBjb25maWd1cmVkIGluZGVwZW5kZW50bHkgaWYgcmVxdWlyZWQpLlxyXG5cdExvZ2dlci5zZXRMZXZlbCA9IGZ1bmN0aW9uKGxldmVsKSB7XHJcblx0XHQvLyBTZXQgdGhlIGdsb2JhbExvZ2dlcidzIGxldmVsLlxyXG5cdFx0Z2xvYmFsTG9nZ2VyLnNldExldmVsKGxldmVsKTtcclxuXHJcblx0XHQvLyBBcHBseSB0aGlzIGxldmVsIHRvIGFsbCByZWdpc3RlcmVkIGNvbnRleHR1YWwgbG9nZ2Vycy5cclxuXHRcdGZvciAodmFyIGtleSBpbiBjb250ZXh0dWFsTG9nZ2Vyc0J5TmFtZU1hcCkge1xyXG5cdFx0XHRpZiAoY29udGV4dHVhbExvZ2dlcnNCeU5hbWVNYXAuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG5cdFx0XHRcdGNvbnRleHR1YWxMb2dnZXJzQnlOYW1lTWFwW2tleV0uc2V0TGV2ZWwobGV2ZWwpO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblx0fTtcclxuXHJcblx0Ly8gUmV0cmlldmUgYSBDb250ZXh0dWFsTG9nZ2VyIGluc3RhbmNlLiAgTm90ZSB0aGF0IG5hbWVkIGxvZ2dlcnMgYXV0b21hdGljYWxseSBpbmhlcml0IHRoZSBnbG9iYWwgbG9nZ2VyJ3MgbGV2ZWwsXHJcblx0Ly8gZGVmYXVsdCBjb250ZXh0IGFuZCBsb2cgaGFuZGxlci5cclxuXHRMb2dnZXIuZ2V0ID0gZnVuY3Rpb24gKG5hbWUpIHtcclxuXHRcdC8vIEFsbCBsb2dnZXIgaW5zdGFuY2VzIGFyZSBjYWNoZWQgc28gdGhleSBjYW4gYmUgY29uZmlndXJlZCBhaGVhZCBvZiB1c2UuXHJcblx0XHRyZXR1cm4gY29udGV4dHVhbExvZ2dlcnNCeU5hbWVNYXBbbmFtZV0gfHxcclxuXHRcdFx0KGNvbnRleHR1YWxMb2dnZXJzQnlOYW1lTWFwW25hbWVdID0gbmV3IENvbnRleHR1YWxMb2dnZXIobWVyZ2UoeyBuYW1lOiBuYW1lIH0sIGdsb2JhbExvZ2dlci5jb250ZXh0KSkpO1xyXG5cdH07XHJcblxyXG5cdC8vIENvbmZpZ3VyZSBhbmQgZXhhbXBsZSBhIERlZmF1bHQgaW1wbGVtZW50YXRpb24gd2hpY2ggd3JpdGVzIHRvIHRoZSBgd2luZG93LmNvbnNvbGVgIChpZiBwcmVzZW50KS4gIFRoZVxyXG5cdC8vIGBvcHRpb25zYCBoYXNoIGNhbiBiZSB1c2VkIHRvIGNvbmZpZ3VyZSB0aGUgZGVmYXVsdCBsb2dMZXZlbCBhbmQgcHJvdmlkZSBhIGN1c3RvbSBtZXNzYWdlIGZvcm1hdHRlci5cclxuXHRMb2dnZXIudXNlRGVmYXVsdHMgPSBmdW5jdGlvbihvcHRpb25zKSB7XHJcblx0XHRvcHRpb25zID0gb3B0aW9ucyB8fCB7fTtcclxuXHJcblx0XHRvcHRpb25zLmZvcm1hdHRlciA9IG9wdGlvbnMuZm9ybWF0dGVyIHx8IGZ1bmN0aW9uIGRlZmF1bHRNZXNzYWdlRm9ybWF0dGVyKG1lc3NhZ2VzLCBjb250ZXh0KSB7XHJcblx0XHRcdC8vIFByZXBlbmQgdGhlIGxvZ2dlcidzIG5hbWUgdG8gdGhlIGxvZyBtZXNzYWdlIGZvciBlYXN5IGlkZW50aWZpY2F0aW9uLlxyXG5cdFx0XHRpZiAoY29udGV4dC5uYW1lKSB7XHJcblx0XHRcdFx0bWVzc2FnZXMudW5zaGlmdChcIltcIiArIGNvbnRleHQubmFtZSArIFwiXVwiKTtcclxuXHRcdFx0fVxyXG5cdFx0fTtcclxuXHJcblx0XHQvLyBDaGVjayBmb3IgdGhlIHByZXNlbmNlIG9mIGEgbG9nZ2VyLlxyXG5cdFx0aWYgKHR5cGVvZiBjb25zb2xlID09PSBcInVuZGVmaW5lZFwiKSB7XHJcblx0XHRcdHJldHVybjtcclxuXHRcdH1cclxuXHJcblx0XHQvLyBNYXAgb2YgdGltZXN0YW1wcyBieSB0aW1lciBsYWJlbHMgdXNlZCB0byB0cmFjayBgI3RpbWVgIGFuZCBgI3RpbWVFbmQoKWAgaW52b2NhdGlvbnMgaW4gZW52aXJvbm1lbnRzXHJcblx0XHQvLyB0aGF0IGRvbid0IG9mZmVyIGEgbmF0aXZlIGNvbnNvbGUgbWV0aG9kLlxyXG5cdFx0dmFyIHRpbWVyU3RhcnRUaW1lQnlMYWJlbE1hcCA9IHt9O1xyXG5cclxuXHRcdC8vIFN1cHBvcnQgZm9yIElFOCsgKGFuZCBvdGhlciwgc2xpZ2h0bHkgbW9yZSBzYW5lIGVudmlyb25tZW50cylcclxuXHRcdHZhciBpbnZva2VDb25zb2xlTWV0aG9kID0gZnVuY3Rpb24gKGhkbHIsIG1lc3NhZ2VzKSB7XHJcblx0XHRcdEZ1bmN0aW9uLnByb3RvdHlwZS5hcHBseS5jYWxsKGhkbHIsIGNvbnNvbGUsIG1lc3NhZ2VzKTtcclxuXHRcdH07XHJcblxyXG5cdFx0TG9nZ2VyLnNldExldmVsKG9wdGlvbnMuZGVmYXVsdExldmVsIHx8IExvZ2dlci5ERUJVRyk7XHJcblx0XHRMb2dnZXIuc2V0SGFuZGxlcihmdW5jdGlvbihtZXNzYWdlcywgY29udGV4dCkge1xyXG5cdFx0XHQvLyBDb252ZXJ0IGFyZ3VtZW50cyBvYmplY3QgdG8gQXJyYXkuXHJcblx0XHRcdG1lc3NhZ2VzID0gQXJyYXkucHJvdG90eXBlLnNsaWNlLmNhbGwobWVzc2FnZXMpO1xyXG5cclxuXHRcdFx0dmFyIGhkbHIgPSBjb25zb2xlLmxvZztcclxuXHRcdFx0dmFyIHRpbWVyTGFiZWw7XHJcblxyXG5cdFx0XHRpZiAoY29udGV4dC5sZXZlbCA9PT0gTG9nZ2VyLlRJTUUpIHtcclxuXHRcdFx0XHR0aW1lckxhYmVsID0gKGNvbnRleHQubmFtZSA/ICdbJyArIGNvbnRleHQubmFtZSArICddICcgOiAnJykgKyBtZXNzYWdlc1swXTtcclxuXHJcblx0XHRcdFx0aWYgKG1lc3NhZ2VzWzFdID09PSAnc3RhcnQnKSB7XHJcblx0XHRcdFx0XHRpZiAoY29uc29sZS50aW1lKSB7XHJcblx0XHRcdFx0XHRcdGNvbnNvbGUudGltZSh0aW1lckxhYmVsKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGVsc2Uge1xyXG5cdFx0XHRcdFx0XHR0aW1lclN0YXJ0VGltZUJ5TGFiZWxNYXBbdGltZXJMYWJlbF0gPSBuZXcgRGF0ZSgpLmdldFRpbWUoKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9XHJcblx0XHRcdFx0ZWxzZSB7XHJcblx0XHRcdFx0XHRpZiAoY29uc29sZS50aW1lRW5kKSB7XHJcblx0XHRcdFx0XHRcdGNvbnNvbGUudGltZUVuZCh0aW1lckxhYmVsKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHRcdGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRpbnZva2VDb25zb2xlTWV0aG9kKGhkbHIsIFsgdGltZXJMYWJlbCArICc6ICcgK1xyXG5cdFx0XHRcdFx0XHRcdChuZXcgRGF0ZSgpLmdldFRpbWUoKSAtIHRpbWVyU3RhcnRUaW1lQnlMYWJlbE1hcFt0aW1lckxhYmVsXSkgKyAnbXMnIF0pO1xyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0XHRlbHNlIHtcclxuXHRcdFx0XHQvLyBEZWxlZ2F0ZSB0aHJvdWdoIHRvIGN1c3RvbSB3YXJuL2Vycm9yIGxvZ2dlcnMgaWYgcHJlc2VudCBvbiB0aGUgY29uc29sZS5cclxuXHRcdFx0XHRpZiAoY29udGV4dC5sZXZlbCA9PT0gTG9nZ2VyLldBUk4gJiYgY29uc29sZS53YXJuKSB7XHJcblx0XHRcdFx0XHRoZGxyID0gY29uc29sZS53YXJuO1xyXG5cdFx0XHRcdH0gZWxzZSBpZiAoY29udGV4dC5sZXZlbCA9PT0gTG9nZ2VyLkVSUk9SICYmIGNvbnNvbGUuZXJyb3IpIHtcclxuXHRcdFx0XHRcdGhkbHIgPSBjb25zb2xlLmVycm9yO1xyXG5cdFx0XHRcdH0gZWxzZSBpZiAoY29udGV4dC5sZXZlbCA9PT0gTG9nZ2VyLklORk8gJiYgY29uc29sZS5pbmZvKSB7XHJcblx0XHRcdFx0XHRoZGxyID0gY29uc29sZS5pbmZvO1xyXG5cdFx0XHRcdH1cclxuXHJcblx0XHRcdFx0b3B0aW9ucy5mb3JtYXR0ZXIobWVzc2FnZXMsIGNvbnRleHQpO1xyXG5cdFx0XHRcdGludm9rZUNvbnNvbGVNZXRob2QoaGRsciwgbWVzc2FnZXMpO1xyXG5cdFx0XHR9XHJcblx0XHR9KTtcclxuXHR9O1xyXG5cclxuXHQvLyBFeHBvcnQgdG8gcG9wdWxhciBlbnZpcm9ubWVudHMgYm9pbGVycGxhdGUuXHJcblx0aWYgKHR5cGVvZiBkZWZpbmUgPT09ICdmdW5jdGlvbicgJiYgZGVmaW5lLmFtZCkge1xyXG5cdFx0ZGVmaW5lKExvZ2dlcik7XHJcblx0fVxyXG5cdGVsc2UgaWYgKHR5cGVvZiBtb2R1bGUgIT09ICd1bmRlZmluZWQnICYmIG1vZHVsZS5leHBvcnRzKSB7XHJcblx0XHRtb2R1bGUuZXhwb3J0cyA9IExvZ2dlcjtcclxuXHR9XHJcblx0ZWxzZSB7XHJcblx0XHRMb2dnZXIuX3ByZXZMb2dnZXIgPSBnbG9iYWwuTG9nZ2VyO1xyXG5cclxuXHRcdExvZ2dlci5ub0NvbmZsaWN0ID0gZnVuY3Rpb24gKCkge1xyXG5cdFx0XHRnbG9iYWwuTG9nZ2VyID0gTG9nZ2VyLl9wcmV2TG9nZ2VyO1xyXG5cdFx0XHRyZXR1cm4gTG9nZ2VyO1xyXG5cdFx0fTtcclxuXHJcblx0XHRnbG9iYWwuTG9nZ2VyID0gTG9nZ2VyO1xyXG5cdH1cclxufSh0aGlzKSk7XHJcbiIsIid1c2Ugc3RyaWN0JztcblxudmFyIHN0dWIgPSByZXF1aXJlKCcuL3N0dWInKTtcbnZhciB0cmFja2luZyA9IHJlcXVpcmUoJy4vdHJhY2tpbmcnKTtcbnZhciBscyA9ICdsb2NhbFN0b3JhZ2UnIGluIGdsb2JhbCAmJiBnbG9iYWwubG9jYWxTdG9yYWdlID8gZ2xvYmFsLmxvY2FsU3RvcmFnZSA6IHN0dWI7XG5cbmZ1bmN0aW9uIGFjY2Vzc29yIChrZXksIHZhbHVlKSB7XG4gIGlmIChhcmd1bWVudHMubGVuZ3RoID09PSAxKSB7XG4gICAgcmV0dXJuIGdldChrZXkpO1xuICB9XG4gIHJldHVybiBzZXQoa2V5LCB2YWx1ZSk7XG59XG5cbmZ1bmN0aW9uIGdldCAoa2V5KSB7XG4gIHJldHVybiBKU09OLnBhcnNlKGxzLmdldEl0ZW0oa2V5KSk7XG59XG5cbmZ1bmN0aW9uIHNldCAoa2V5LCB2YWx1ZSkge1xuICB0cnkge1xuICAgIGxzLnNldEl0ZW0oa2V5LCBKU09OLnN0cmluZ2lmeSh2YWx1ZSkpO1xuICAgIHJldHVybiB0cnVlO1xuICB9IGNhdGNoIChlKSB7XG4gICAgcmV0dXJuIGZhbHNlO1xuICB9XG59XG5cbmZ1bmN0aW9uIHJlbW92ZSAoa2V5KSB7XG4gIHJldHVybiBscy5yZW1vdmVJdGVtKGtleSk7XG59XG5cbmZ1bmN0aW9uIGNsZWFyICgpIHtcbiAgcmV0dXJuIGxzLmNsZWFyKCk7XG59XG5cbmFjY2Vzc29yLnNldCA9IHNldDtcbmFjY2Vzc29yLmdldCA9IGdldDtcbmFjY2Vzc29yLnJlbW92ZSA9IHJlbW92ZTtcbmFjY2Vzc29yLmNsZWFyID0gY2xlYXI7XG5hY2Nlc3Nvci5vbiA9IHRyYWNraW5nLm9uO1xuYWNjZXNzb3Iub2ZmID0gdHJhY2tpbmcub2ZmO1xuXG5tb2R1bGUuZXhwb3J0cyA9IGFjY2Vzc29yO1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgbXMgPSB7fTtcblxuZnVuY3Rpb24gZ2V0SXRlbSAoa2V5KSB7XG4gIHJldHVybiBrZXkgaW4gbXMgPyBtc1trZXldIDogbnVsbDtcbn1cblxuZnVuY3Rpb24gc2V0SXRlbSAoa2V5LCB2YWx1ZSkge1xuICBtc1trZXldID0gdmFsdWU7XG4gIHJldHVybiB0cnVlO1xufVxuXG5mdW5jdGlvbiByZW1vdmVJdGVtIChrZXkpIHtcbiAgdmFyIGZvdW5kID0ga2V5IGluIG1zO1xuICBpZiAoZm91bmQpIHtcbiAgICByZXR1cm4gZGVsZXRlIG1zW2tleV07XG4gIH1cbiAgcmV0dXJuIGZhbHNlO1xufVxuXG5mdW5jdGlvbiBjbGVhciAoKSB7XG4gIG1zID0ge307XG4gIHJldHVybiB0cnVlO1xufVxuXG5tb2R1bGUuZXhwb3J0cyA9IHtcbiAgZ2V0SXRlbTogZ2V0SXRlbSxcbiAgc2V0SXRlbTogc2V0SXRlbSxcbiAgcmVtb3ZlSXRlbTogcmVtb3ZlSXRlbSxcbiAgY2xlYXI6IGNsZWFyXG59O1xuIiwiJ3VzZSBzdHJpY3QnO1xuXG52YXIgbGlzdGVuZXJzID0ge307XG52YXIgbGlzdGVuaW5nID0gZmFsc2U7XG5cbmZ1bmN0aW9uIGxpc3RlbiAoKSB7XG4gIGlmIChnbG9iYWwuYWRkRXZlbnRMaXN0ZW5lcikge1xuICAgIGdsb2JhbC5hZGRFdmVudExpc3RlbmVyKCdzdG9yYWdlJywgY2hhbmdlLCBmYWxzZSk7XG4gIH0gZWxzZSBpZiAoZ2xvYmFsLmF0dGFjaEV2ZW50KSB7XG4gICAgZ2xvYmFsLmF0dGFjaEV2ZW50KCdvbnN0b3JhZ2UnLCBjaGFuZ2UpO1xuICB9IGVsc2Uge1xuICAgIGdsb2JhbC5vbnN0b3JhZ2UgPSBjaGFuZ2U7XG4gIH1cbn1cblxuZnVuY3Rpb24gY2hhbmdlIChlKSB7XG4gIGlmICghZSkge1xuICAgIGUgPSBnbG9iYWwuZXZlbnQ7XG4gIH1cbiAgdmFyIGFsbCA9IGxpc3RlbmVyc1tlLmtleV07XG4gIGlmIChhbGwpIHtcbiAgICBhbGwuZm9yRWFjaChmaXJlKTtcbiAgfVxuXG4gIGZ1bmN0aW9uIGZpcmUgKGxpc3RlbmVyKSB7XG4gICAgbGlzdGVuZXIoSlNPTi5wYXJzZShlLm5ld1ZhbHVlKSwgSlNPTi5wYXJzZShlLm9sZFZhbHVlKSwgZS51cmwgfHwgZS51cmkpO1xuICB9XG59XG5cbmZ1bmN0aW9uIG9uIChrZXksIGZuKSB7XG4gIGlmIChsaXN0ZW5lcnNba2V5XSkge1xuICAgIGxpc3RlbmVyc1trZXldLnB1c2goZm4pO1xuICB9IGVsc2Uge1xuICAgIGxpc3RlbmVyc1trZXldID0gW2ZuXTtcbiAgfVxuICBpZiAobGlzdGVuaW5nID09PSBmYWxzZSkge1xuICAgIGxpc3RlbigpO1xuICB9XG59XG5cbmZ1bmN0aW9uIG9mZiAoa2V5LCBmbikge1xuICB2YXIgbnMgPSBsaXN0ZW5lcnNba2V5XTtcbiAgaWYgKG5zLmxlbmd0aCA+IDEpIHtcbiAgICBucy5zcGxpY2UobnMuaW5kZXhPZihmbiksIDEpO1xuICB9IGVsc2Uge1xuICAgIGxpc3RlbmVyc1trZXldID0gW107XG4gIH1cbn1cblxubW9kdWxlLmV4cG9ydHMgPSB7XG4gIG9uOiBvbixcbiAgb2ZmOiBvZmZcbn07XG4iLCJ2YXIgV2lkZ2V0c0NvbW1vbiA9IChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gV2lkZ2V0c0NvbW1vbihvcHRpb25zKSB7XG4gICAgICAgIGlmIChvcHRpb25zID09PSB2b2lkIDApIHsgb3B0aW9ucyA9IHsgcmVzaXplVG9WaWV3cG9ydDogZmFsc2UgfTsgfVxuICAgICAgICB0aGlzLmxvZ2dlciA9IHJlcXVpcmUoJ2pzLWxvZ2dlcicpO1xuICAgICAgICB0aGlzLnN0b3JhZ2UgPSByZXF1aXJlKCdsb2NhbC1zdG9yYWdlJyk7XG4gICAgICAgIHRoaXMuRXZlbnRFbWl0dGVyID0gcmVxdWlyZSgnZXZlbnRlbWl0dGVyMycpO1xuICAgICAgICB0aGlzLmVtaXR0ZXIgPSBuZXcgdGhpcy5FdmVudEVtaXR0ZXIoKTtcbiAgICAgICAgdGhpcy5yY2UgPSBuZXcgUmNlSW50ZXJmYWNlKHRoaXMubG9nZ2VyLCB0aGlzLmVtaXR0ZXIpO1xuICAgICAgICB0aGlzLmluaXRMb2dnZXIoKTtcbiAgICAgICAgdGhpcy5jb25maWdEYXRhID0gbmV3IENvbmZpZ0RhdGFNb2R1bGUodGhpcy5sb2dnZXIsIHRoaXMucmNlLCB0aGlzLnN0b3JhZ2UpO1xuICAgICAgICB0aGlzLnJlc2l6ZU1vZHVsZSA9IG5ldyBSZXNpemVNb2R1bGUodGhpcy5sb2dnZXIsIHRoaXMuZW1pdHRlciwgdGhpcy5jb25maWdEYXRhKTtcbiAgICAgICAgdGhpcy5hdWRpbyA9IG5ldyBBdWRpb01vZHVsZSh0aGlzLmxvZ2dlciwgdGhpcy5yY2UsIHRoaXMuZW1pdHRlcik7XG4gICAgICAgIHRoaXMuZnVsbHNjcmVlbiA9IG5ldyBGdWxsc2NyZWVuTW9kdWxlKHRoaXMubG9nZ2VyLCB0aGlzLnJlc2l6ZU1vZHVsZSwgdGhpcy5jb25maWdEYXRhKTtcbiAgICAgICAgdGhpcy51c2VyRGF0YSA9IG5ldyBVc2VyRGF0YU1vZHVsZSh0aGlzLmxvZ2dlciwgdGhpcy5lbWl0dGVyLCB0aGlzLnJjZSk7XG4gICAgICAgIHRoaXMud2lkZ2V0RGF0YSA9IG5ldyBXaWRnZXREYXRhTW9kdWxlKHRoaXMubG9nZ2VyKTtcbiAgICAgICAgdGhpcy5sYXlvdXQgPSBuZXcgTGF5b3V0TW9kdWxlKHRoaXMubG9nZ2VyLCB0aGlzLnJjZSwgdGhpcy5lbWl0dGVyLCBvcHRpb25zKTtcbiAgICAgICAgdGhpcy5idWlsZEluZm9Nb2R1bGUgPSBuZXcgQnVpbGRJbmZvTW9kdWxlKHRoaXMubG9nZ2VyKTtcbiAgICAgICAgdGhpcy5kaWFsb2cgPSBuZXcgRGlhbG9nTW9kdWxlKHRoaXMubG9nZ2VyKTtcbiAgICAgICAgd2luZG93LndpZGdldEV2ZW50QnVzID0gdGhpcy5lbWl0dGVyO1xuICAgICAgICBpZiAodGhpcy5jb25maWdEYXRhLmlzTG9jYWwoKSkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LnN0eWxlLm92ZXJmbG93WSA9ICdhdXRvJztcbiAgICAgICAgfVxuICAgIH1cbiAgICBXaWRnZXRzQ29tbW9uLnByb3RvdHlwZS52ZXJzaW9uID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gJ19fVkVSU0lPTl9fJztcbiAgICB9O1xuICAgIFdpZGdldHNDb21tb24ucHJvdG90eXBlLm5hdmlnYXRlVG8gPSBmdW5jdGlvbiAodXJpLCBjYWxsYmFjaykge1xuICAgICAgICBpZiAoY2FsbGJhY2sgPT09IHZvaWQgMCkgeyBjYWxsYmFjayA9IG51bGw7IH1cbiAgICAgICAgaWYgKHRoaXMuY29uZmlnRGF0YS5pc1Byb2R1Y3Rpb24oKSkge1xuICAgICAgICAgICAgdXJpID0gdXJpLnJlcGxhY2UoL2h0bWwkL2ksICd4aHRtbCcpO1xuICAgICAgICB9XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ0lzIFJDRSBhdmFpbGFibGU/JywgdGhpcy5yY2UuaXNBdmFpbGFibGUoKSk7XG4gICAgICAgICAgICBpZiAodGhpcy5yY2UuaXNBdmFpbGFibGUoKSkge1xuICAgICAgICAgICAgICAgIHZhciBtYXRjaCA9IHdpbmRvdy5wYXJlbnQubG9jYXRpb24uaHJlZi5tYXRjaCgvXihodHRwLitzOW1sXFwvKSguKykkLyk7XG4gICAgICAgICAgICAgICAgdmFyIHVybCA9IG1hdGNoICYmIG1hdGNoWzFdID8gbWF0Y2hbMV0gKyB1cmkgOiB1cmk7XG4gICAgICAgICAgICAgICAgdGhpcy5yY2UubmF2aWdhdGVUbyh1cmwsIGNhbGxiYWNrKTtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICB2YXIgdXJsID0gJy4uLy4uLy4uL3M5bWwvJyArIHVyaTtcbiAgICAgICAgICAgICAgICB3aW5kb3cucGFyZW50LmxvY2F0aW9uLmhyZWYgPSB1cmw7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgY2F0Y2ggKGUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oZSk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKGNhbGxiYWNrKSB7XG4gICAgICAgICAgICBjYWxsYmFjaygpO1xuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG4gICAgfTtcbiAgICBXaWRnZXRzQ29tbW9uLnByb3RvdHlwZS5pbml0TG9nZ2VyID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB0aGlzLmxvZ2dlci51c2VEZWZhdWx0cygpO1xuICAgICAgICB0aGlzLmxvZ2dlci5vZmYgPSBmdW5jdGlvbiAoKSB7IHJldHVybiBfdGhpcy5sb2dnZXIuc2V0TGV2ZWwoX3RoaXMubG9nZ2VyLk9GRik7IH07XG4gICAgICAgIHRoaXMubG9nZ2VyLm9uID0gZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMubG9nZ2VyLnNldExldmVsKF90aGlzLmxvZ2dlci5ERUJVRyk7IH07XG4gICAgICAgIGlmICh0aGlzLnJjZS5pc0F2YWlsYWJsZSgpKSB7XG4gICAgICAgICAgICB0aGlzLmxvZ2dlci5vZmYoKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmVtaXR0ZXIub24oJ2xvZ2dlcjpvbicsIGZ1bmN0aW9uICgpIHsgcmV0dXJuIF90aGlzLmxvZ2dlci5vbigpOyB9KTtcbiAgICAgICAgdGhpcy5lbWl0dGVyLm9uKCdsb2dnZXI6b2ZmJywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMubG9nZ2VyLm9mZigpOyB9KTtcbiAgICB9O1xuICAgIHJldHVybiBXaWRnZXRzQ29tbW9uO1xufSkoKTtcbndpbmRvdy5XaWRnZXRzQ29tbW9uID0gV2lkZ2V0c0NvbW1vbjtcbm1vZHVsZS5leHBvcnRzID0gV2lkZ2V0c0NvbW1vbjtcbnZhciBSY2VJbnRlcmZhY2UgPSAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIFJjZUludGVyZmFjZShsb2dnZXIsIGVtaXR0ZXIpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgdGhpcy5pc1JDRSA9IGZhbHNlO1xuICAgICAgICB0aGlzLmxvZ2dlciA9IGxvZ2dlcjtcbiAgICAgICAgdGhpcy5lbWl0dGVyID0gZW1pdHRlcjtcbiAgICAgICAgdGhpcy5kZXRlY3RSQ0UoKTtcbiAgICAgICAgd2luZG93LnJjZUV2ZW50QnVzT25Mb2FkID0gZnVuY3Rpb24gKGJ1cywgY3NtKSB7XG4gICAgICAgICAgICBpZiAoY3NtID09PSB2b2lkIDApIHsgY3NtID0gbnVsbDsgfVxuICAgICAgICAgICAgcmV0dXJuIF90aGlzLnJjZUV2ZW50QnVzT25Mb2FkKGJ1cywgY3NtKTtcbiAgICAgICAgfTtcbiAgICB9XG4gICAgUmNlSW50ZXJmYWNlLnByb3RvdHlwZS5pc0F2YWlsYWJsZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNSQ0U7XG4gICAgfTtcbiAgICBSY2VJbnRlcmZhY2UucHJvdG90eXBlLmlzSW5pdGlhbGl6ZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0eXBlb2YgdGhpcy5idXMgIT09ICd1bmRlZmluZWQnO1xuICAgIH07XG4gICAgUmNlSW50ZXJmYWNlLnByb3RvdHlwZS5lbmFibGVBdWRpbyA9IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgICAgICB0aGlzLnRyaWdnZXIoJ21lZGlhOmVuYWJsZScsICcnLCBjYWxsYmFjayk7XG4gICAgfTtcbiAgICBSY2VJbnRlcmZhY2UucHJvdG90eXBlLmRpc2FibGVBdWRpbyA9IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgICAgICB0aGlzLnRyaWdnZXIoJ21lZGlhOmRpc2FibGUnLCAnJywgY2FsbGJhY2spO1xuICAgIH07XG4gICAgUmNlSW50ZXJmYWNlLnByb3RvdHlwZS5uYXZpZ2F0ZVRvID0gZnVuY3Rpb24gKHVyaSwgY2FsbGJhY2spIHtcbiAgICAgICAgdGhpcy50cmlnZ2VyKCduYXZpZ2F0ZTp0bycsICcnLCBjYWxsYmFjaywgdXJpKTtcbiAgICB9O1xuICAgIFJjZUludGVyZmFjZS5wcm90b3R5cGUuc2F2ZVVzZXJEYXRhID0gZnVuY3Rpb24gKHBhcmFtZXRlcnMsIGNhbGxiYWNrKSB7XG4gICAgICAgIHRoaXMudHJpZ2dlcigneGFwaVBlcnNpc3Q6c2F2ZScsICd4YXBpUGVyc2lzdDpzYXZlZCcsIGNhbGxiYWNrLCBwYXJhbWV0ZXJzKTtcbiAgICB9O1xuICAgIFJjZUludGVyZmFjZS5wcm90b3R5cGUucmV0cmlldmVVc2VyRGF0YSA9IGZ1bmN0aW9uIChwYXJhbWV0ZXJzLCBjYWxsYmFjaykge1xuICAgICAgICB0aGlzLnRyaWdnZXIoJ3hhcGlQZXJzaXN0OnJldHJpZXZlJywgJ3hhcGlQZXJzaXN0OnJldHJpZXZlZCcsIGNhbGxiYWNrLCBwYXJhbWV0ZXJzKTtcbiAgICB9O1xuICAgIFJjZUludGVyZmFjZS5wcm90b3R5cGUuZGV0ZWN0UkNFID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLmlzUkNFID0gZmFsc2U7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBpZiAoISF3aW5kb3cudG9wLmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyY2UtY29udGFpbmVyJykpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmlzUkNFID0gdHJ1ZTtcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdDb21tb24gV2lkZ2V0cyBMaWJyYXJ5JywgJ0F1ZGlvTW9kdWxlOjpkZXRlY3RSQ0UnLCBlcnIpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9O1xuICAgIFJjZUludGVyZmFjZS5wcm90b3R5cGUucmNlRXZlbnRCdXNPbkxvYWQgPSBmdW5jdGlvbiAoYnVzLCBjc20pIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgaWYgKGNzbSA9PT0gdm9pZCAwKSB7IGNzbSA9IG51bGw7IH1cbiAgICAgICAgdGhpcy5sb2dnZXIubG9nKCdSQ0UgaXMgY2FsbGluZyBtZSB0byBpbml0aWFsaXplIHRoZSBidXMsIGdyZWF0IScpO1xuICAgICAgICB0aGlzLmlzUkNFID0gdHJ1ZTtcbiAgICAgICAgdGhpcy5idXMgPSBidXM7XG4gICAgICAgIHRoaXMuY3NtID0gY3NtO1xuICAgICAgICB0aGlzLmVtaXR0ZXIub24oJ2NzbTpwYXJzZScsIGZ1bmN0aW9uIChjb250ZXh0KSB7IHJldHVybiBfdGhpcy5wYXJzZUNvbnRlbnRTY3JpcHRzKGNvbnRleHQpOyB9KTtcbiAgICAgICAgdGhpcy5lbWl0dGVyLmVtaXQoJ3JjZTpyZWFkeScpO1xuICAgIH07XG4gICAgUmNlSW50ZXJmYWNlLnByb3RvdHlwZS50cmlnZ2VyID0gZnVuY3Rpb24gKGJ1c0V2ZW50LCB3YWl0Rm9yLCBjYWxsYmFjaywgcGFyYW1ldGVycykge1xuICAgICAgICBpZiAodGhpcy5idXMpIHtcbiAgICAgICAgICAgIHRoaXMubG9nZ2VyLmxvZygnU2VuZGluZyBtZXNzYWdlIG92ZXIgdGhlIGJ1cycsIGJ1c0V2ZW50KTtcbiAgICAgICAgICAgIGlmICh3YWl0Rm9yICYmIHR5cGVvZiBjYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIHRoaXMuYnVzLm9uY2Uod2FpdEZvciwgY2FsbGJhY2spO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5idXMuZW1pdChidXNFdmVudCwgcGFyYW1ldGVycyk7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKCF0aGlzLmJ1cyB8fCAhd2FpdEZvcikge1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBjYWxsYmFjayA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgICAgIGNhbGxiYWNrKHBhcmFtZXRlcnMpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfTtcbiAgICBSY2VJbnRlcmZhY2UucHJvdG90eXBlLnBhcnNlQ29udGVudFNjcmlwdHMgPSBmdW5jdGlvbiAoY29udGV4dCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICBpZiAoY29udGV4dCA9PT0gdm9pZCAwKSB7IGNvbnRleHQgPSB7IGRvYzogZG9jdW1lbnQsIGdsb2JhbDogd2luZG93IH07IH1cbiAgICAgICAgaWYgKHRoaXMuY3NtICYmIHRoaXMuY3NtLnNjcmlwdHMpIHtcbiAgICAgICAgICAgIHRoaXMuY3NtLnNjcmlwdHMudXBkYXRlKCkudGhlbihmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMuY3NtLkhlbHBlcnMubm9GbGlja2VyKGRvY3VtZW50KTtcbiAgICAgICAgICAgICAgICBfdGhpcy5jc20ucnVuKGNvbnRleHQpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIHJldHVybiBSY2VJbnRlcmZhY2U7XG59KSgpO1xudmFyIEF1ZGlvTW9kdWxlID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBBdWRpb01vZHVsZShsb2dnZXIsIHJjZSwgZW1pdHRlcikge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB0aGlzLnJjZSA9IG51bGw7XG4gICAgICAgIHRoaXMuZGVsYXkgPSA1MDA7XG4gICAgICAgIHRoaXMuaXNXYWl0aW5nRm9yRm9jdXMgPSB0cnVlO1xuICAgICAgICB0aGlzLmF1ZGlvUmVhZHkgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5yY2UgPSByY2U7XG4gICAgICAgIHRoaXMubG9nZ2VyID0gbG9nZ2VyO1xuICAgICAgICB0aGlzLmVtaXR0ZXIgPSBlbWl0dGVyO1xuICAgICAgICB2YXIgc291cmNlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc291cmNlJyk7XG4gICAgICAgIHNvdXJjZS5zZXRBdHRyaWJ1dGUoJ3R5cGUnLCAnYXVkaW8vbXBlZycpO1xuICAgICAgICB0aGlzLmF1ZGlvRWxlbWVudCA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2F1ZGlvJyk7XG4gICAgICAgIHRoaXMuYXVkaW9FbGVtZW50LmFwcGVuZENoaWxkKHNvdXJjZSk7XG4gICAgICAgIHRoaXMuYXVkaW9FbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2VuZGVkJywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMucGF1c2UoKTsgfSwgZmFsc2UpO1xuICAgICAgICB0aGlzLmF1ZGlvRWxlbWVudC5xdWVyeVNlbGVjdG9yKCdzb3VyY2UnKS5zcmMgPSAnJztcbiAgICAgICAgdGhpcy5lbWl0dGVyLm9uKCdhdWRpbzpwbGF5JywgZnVuY3Rpb24gKHVybCkgeyByZXR1cm4gX3RoaXMucGxheSh1cmwpOyB9KTtcbiAgICAgICAgdGhpcy5lbWl0dGVyLm9uKCdhdWRpbzpwYXVzZScsIGZ1bmN0aW9uICgpIHsgcmV0dXJuIF90aGlzLnBhdXNlKCk7IH0pO1xuICAgICAgICB0aGlzLmRldGVybWluZVBsYXliYWNrUmVhZGluZXNzKCk7XG4gICAgfVxuICAgIEF1ZGlvTW9kdWxlLnByb3RvdHlwZS5yZWdpc3RlckVuZFBsYXliYWNrSGFuZGxlciA9IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgICAgICB0aGlzLmF1ZGlvRWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdlbmRlZCcsIGNhbGxiYWNrLCBmYWxzZSk7XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUuaXNQYXVzZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmF1ZGlvRWxlbWVudC5wYXVzZWQ7XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUuc2V0QXVkaW9EZWxheSA9IGZ1bmN0aW9uIChkZWxheSkge1xuICAgICAgICBpZiAoZGVsYXkgPj0gMCkge1xuICAgICAgICAgICAgdGhpcy5kZWxheSA9IGRlbGF5O1xuICAgICAgICB9XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUucGxheU9uRm9jdXMgPSBmdW5jdGlvbiAodXJsLCBub2RlLCBkZWxheSkge1xuICAgICAgICBpZiAobm9kZSA9PT0gdm9pZCAwKSB7IG5vZGUgPSB3aW5kb3c7IH1cbiAgICAgICAgaWYgKGRlbGF5ID09PSB2b2lkIDApIHsgZGVsYXkgPSB0aGlzLmRlbGF5OyB9XG4gICAgICAgIG5vZGUucmVtb3ZlRXZlbnRMaXN0ZW5lcignZm9jdXMnLCBub2RlLmZvY3VzTGlzdGVuZXIpO1xuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignYmx1cicsIHdpbmRvdy5ibHVyTGlzdGVuZXIpO1xuICAgICAgICBub2RlLmZvY3VzTGlzdGVuZXIgPSB0aGlzLmZvY3VzUGxheWJhY2tIYW5kbGVyLmJpbmQodGhpcywgdXJsLCBkZWxheSk7XG4gICAgICAgIG5vZGUuYWRkRXZlbnRMaXN0ZW5lcignZm9jdXMnLCBub2RlLmZvY3VzTGlzdGVuZXIsIGZhbHNlKTtcbiAgICAgICAgd2luZG93LmJsdXJMaXN0ZW5lciA9IHRoaXMuYmx1clBsYXliYWNrSGFuZGxlci5iaW5kKHRoaXMsIHVybCwgZGVsYXkpO1xuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignYmx1cicsIHdpbmRvdy5ibHVyTGlzdGVuZXIsIGZhbHNlKTtcbiAgICB9O1xuICAgIEF1ZGlvTW9kdWxlLnByb3RvdHlwZS5wbGF5T25DbGljayA9IGZ1bmN0aW9uICh1cmwsIG5vZGUsIGRlbGF5KSB7XG4gICAgICAgIGlmIChub2RlID09PSB2b2lkIDApIHsgbm9kZSA9IHdpbmRvdzsgfVxuICAgICAgICBpZiAoZGVsYXkgPT09IHZvaWQgMCkgeyBkZWxheSA9IHRoaXMuZGVsYXk7IH1cbiAgICAgICAgbm9kZS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIHRoaXMuY2xpY2tQbGF5YmFja0hhbmRsZXIuYmluZCh0aGlzLCB1cmwsIGRlbGF5KSwgZmFsc2UpO1xuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignYmx1cicsIHRoaXMuYmx1clBsYXliYWNrSGFuZGxlci5iaW5kKHRoaXMsIHVybCwgZGVsYXkpLCBmYWxzZSk7XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUubG9hZCA9IGZ1bmN0aW9uICh1cmwpIHtcbiAgICAgICAgaWYgKHRoaXMuYXVkaW9FbGVtZW50KSB7XG4gICAgICAgICAgICB0aGlzLmF1ZGlvRWxlbWVudC5xdWVyeVNlbGVjdG9yKCdzb3VyY2UnKS5zcmMgPSB1cmw7XG4gICAgICAgICAgICB0aGlzLmF1ZGlvRWxlbWVudC5sb2FkKCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIEF1ZGlvTW9kdWxlLnByb3RvdHlwZS5wbGF5ID0gZnVuY3Rpb24gKHVybCwgZGVsYXksIHJlcGxheSkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICBpZiAoZGVsYXkgPT09IHZvaWQgMCkgeyBkZWxheSA9IHRoaXMuZGVsYXk7IH1cbiAgICAgICAgaWYgKHJlcGxheSA9PT0gdm9pZCAwKSB7IHJlcGxheSA9IGZhbHNlOyB9XG4gICAgICAgIHRoaXMuZW1pdHRlci5lbWl0KCdhdWRpbzpzdGFydHMnKTtcbiAgICAgICAgdGhpcy5pc1dhaXRpbmdGb3JGb2N1cyA9IGZhbHNlO1xuICAgICAgICB0aGlzLnJjZS5kaXNhYmxlQXVkaW8oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKF90aGlzLmF1ZGlvRWxlbWVudCkge1xuICAgICAgICAgICAgICAgIGlmICh1cmwgJiYgKHJlcGxheSB8fCAhX3RoaXMubWF0Y2hTb3VyY2UodXJsKSkpIHtcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMubG9nZ2VyLmxvZygnPT1SRUxPQURJTkc9PScpO1xuICAgICAgICAgICAgICAgICAgICBfdGhpcy5hdWRpb0VsZW1lbnQucXVlcnlTZWxlY3Rvcignc291cmNlJykuc3JjID0gdXJsO1xuICAgICAgICAgICAgICAgICAgICBfdGhpcy5hdWRpb0VsZW1lbnQubG9hZCgpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICBlbHNlIGlmIChyZXBsYXkpIHtcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMuYXVkaW9FbGVtZW50LmxvYWQoKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLmxvZ2dlci5sb2coJz09UkVTVU1FPT0nLCByZXBsYXksICFfdGhpcy5tYXRjaFNvdXJjZSh1cmwpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgX3RoaXMuYXVkaW9FbGVtZW50LnBsYXkoKTtcbiAgICAgICAgICAgICAgICBfdGhpcy5sb2dnZXIubG9nKCcqKiogUExBWUlORycpO1xuICAgICAgICAgICAgICAgIGlmIChfdGhpcy5hdWRpb0VsZW1lbnQucGF1c2VkKSB7XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLmxvZ2dlci53YXJuKCdhdWRpbyBwbGF5YmFjayB3YXMga2lsbGVkJyk7XG4gICAgICAgICAgICAgICAgICAgIF90aGlzLmVtaXR0ZXIuZW1pdCgnYXVkaW86a2lsbGVkJyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgX3RoaXMubG9nZ2VyLmVycm9yKCdBdWRpb01vZHVsZTpwbGF5JywgJ1VuaGFuZGxlZCBjb250ZXh0Jyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0pO1xuICAgIH07XG4gICAgQXVkaW9Nb2R1bGUucHJvdG90eXBlLnBhdXNlID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB0aGlzLmVtaXR0ZXIuZW1pdCgnYXVkaW86c3RvcHMnKTtcbiAgICAgICAgdGhpcy5yY2UuZW5hYmxlQXVkaW8oZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKF90aGlzLmF1ZGlvRWxlbWVudCkge1xuICAgICAgICAgICAgICAgIF90aGlzLmF1ZGlvRWxlbWVudC5wYXVzZSgpO1xuICAgICAgICAgICAgICAgIF90aGlzLmxvZ2dlci5sb2coJyoqKiBQQVVTRUQnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUuY3VycmVudFRpbWUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLmF1ZGlvRWxlbWVudCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYXVkaW9FbGVtZW50LmN1cnJlbnRUaW1lO1xuICAgICAgICB9XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUuZHVyYXRpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLmF1ZGlvRWxlbWVudCkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYXVkaW9FbGVtZW50LmR1cmF0aW9uO1xuICAgICAgICB9XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUucmVtYWluaW5nVGltZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuYXVkaW9FbGVtZW50KSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hdWRpb0VsZW1lbnQuZHVyYXRpb24gLSB0aGlzLmF1ZGlvRWxlbWVudC5jdXJyZW50VGltZTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgQXVkaW9Nb2R1bGUucHJvdG90eXBlLnNlZWsgPSBmdW5jdGlvbiAodGltZSkge1xuICAgICAgICBpZiAodGhpcy5hdWRpb0VsZW1lbnQgJiYgdHlwZW9mIHRpbWUgIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICB0aGlzLmF1ZGlvRWxlbWVudC5jdXJyZW50VGltZSA9IHRpbWU7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIEF1ZGlvTW9kdWxlLnByb3RvdHlwZS5pc1JlYWR5ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5hdWRpb1JlYWR5O1xuICAgIH07XG4gICAgQXVkaW9Nb2R1bGUucHJvdG90eXBlLm1hdGNoU291cmNlID0gZnVuY3Rpb24gKHVybCkge1xuICAgICAgICBpZiAodXJsICYmIHRoaXMuYXVkaW9FbGVtZW50KSB7XG4gICAgICAgICAgICB2YXIgcmVnZXggPSAvXi4rXFwvLztcbiAgICAgICAgICAgIHZhciBmaWxlID0gdXJsLnJlcGxhY2UocmVnZXgsICcnKTtcbiAgICAgICAgICAgIHZhciBzcmMgPSB0aGlzLmF1ZGlvRWxlbWVudC5xdWVyeVNlbGVjdG9yKCdzb3VyY2UnKS5zcmMucmVwbGFjZShyZWdleCwgJycpO1xuICAgICAgICAgICAgcmV0dXJuIGZpbGUgPT09IHNyYztcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUuY2xpY2tQbGF5YmFja0hhbmRsZXIgPSBmdW5jdGlvbiAodXJsLCBkZWxheSkge1xuICAgICAgICBpZiAoZGVsYXkgPT09IHZvaWQgMCkgeyBkZWxheSA9IHRoaXMuZGVsYXk7IH1cbiAgICAgICAgdGhpcy5sb2dnZXIubG9nKCdDTElDSyBSRUNJRVZFRCcpO1xuICAgICAgICB0aGlzLnBsYXkodXJsLCBkZWxheSwgdHJ1ZSk7XG4gICAgfTtcbiAgICBBdWRpb01vZHVsZS5wcm90b3R5cGUuZm9jdXNQbGF5YmFja0hhbmRsZXIgPSBmdW5jdGlvbiAodXJsLCBkZWxheSkge1xuICAgICAgICBpZiAoZGVsYXkgPT09IHZvaWQgMCkgeyBkZWxheSA9IHRoaXMuZGVsYXk7IH1cbiAgICAgICAgdGhpcy5sb2dnZXIubG9nKCdSRUNJRVZJTkcgRk9DVVMnLCB0aGlzLmlzV2FpdGluZ0ZvckZvY3VzKTtcbiAgICAgICAgaWYgKHRoaXMuaXNXYWl0aW5nRm9yRm9jdXMpIHtcbiAgICAgICAgICAgIHRoaXMucGxheSh1cmwsIGRlbGF5KTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgQXVkaW9Nb2R1bGUucHJvdG90eXBlLmJsdXJQbGF5YmFja0hhbmRsZXIgPSBmdW5jdGlvbiAodXJsLCBkZWxheSkge1xuICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ0JMVVInLCB0aGlzLmlzV2FpdGluZ0ZvckZvY3VzKTtcbiAgICAgICAgaWYgKCF0aGlzLmlzV2FpdGluZ0ZvckZvY3VzKSB7XG4gICAgICAgICAgICB0aGlzLmlzV2FpdGluZ0ZvckZvY3VzID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMucGF1c2UoKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgQXVkaW9Nb2R1bGUucHJvdG90eXBlLnNldFJlYWR5ID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0aGlzLmF1ZGlvUmVhZHkgPSB0cnVlO1xuICAgICAgICB0aGlzLmVtaXR0ZXIuZW1pdCgnYXVkaW86cmVhZHknKTtcbiAgICB9O1xuICAgIEF1ZGlvTW9kdWxlLnByb3RvdHlwZS5kZXRlcm1pbmVQbGF5YmFja1JlYWRpbmVzcyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgaWYgKHRoaXMucmNlLmlzQXZhaWxhYmxlKCkpIHtcbiAgICAgICAgICAgIHRoaXMucmNlLmlzSW5pdGlhbGl6ZWQoKSA/IHRoaXMuc2V0UmVhZHkoKSA6IHRoaXMuZW1pdHRlci5vbigncmNlOnJlYWR5JywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMuc2V0UmVhZHkoKTsgfSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnNldFJlYWR5KCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIHJldHVybiBBdWRpb01vZHVsZTtcbn0pKCk7XG52YXIgQnVpbGRJbmZvTW9kdWxlID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBCdWlsZEluZm9Nb2R1bGUobG9nZ2VyKSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIHRoaXMubG9nZ2VyID0gbG9nZ2VyO1xuICAgICAgICBpZiAod2luZG93LmxvY2F0aW9uLmhyZWYuaW5kZXhPZignZWRpdG9yLmh0bWwnKSA+IC0xKSB7XG4gICAgICAgICAgICB0aGlzLmxvYWRWZXJzaW9uKGZ1bmN0aW9uIChidWlsZEluZm9zKSB7IHJldHVybiBfdGhpcy5kaXNwbGF5VmVyc2lvbihidWlsZEluZm9zKTsgfSk7XG4gICAgICAgIH1cbiAgICB9XG4gICAgQnVpbGRJbmZvTW9kdWxlLnByb3RvdHlwZS5kaXNwbGF5VmVyc2lvbiA9IGZ1bmN0aW9uIChidWlsZEluZm9zKSB7XG4gICAgICAgIHRoaXMudmVyc2lvbkRpdiA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2RpdicpO1xuICAgICAgICB0aGlzLnZlcnNpb25EaXYuaWQgPSAnd2lkZ2V0LWJ1aWxkLWluZm8nO1xuICAgICAgICB0aGlzLnZlcnNpb25EaXYuc3R5bGUucmlnaHQgPSAnMCc7XG4gICAgICAgIHRoaXMudmVyc2lvbkRpdi5zdHlsZS5ib3R0b20gPSAnMCc7XG4gICAgICAgIHRoaXMudmVyc2lvbkRpdi5zdHlsZS5wb3NpdGlvbiA9ICdmaXhlZCc7XG4gICAgICAgIHRoaXMudmVyc2lvbkRpdi5zdHlsZS5iYWNrZ3JvdW5kQ29sb3IgPSAnIzJFMzEzQic7XG4gICAgICAgIHRoaXMudmVyc2lvbkRpdi5zdHlsZS5vcGFjaXR5ID0gJzEnO1xuICAgICAgICB0aGlzLnZlcnNpb25EaXYuc3R5bGUuY29sb3IgPSAnI0ZGRkZGRic7XG4gICAgICAgIHRoaXMudmVyc2lvbkRpdi5zdHlsZS5wYWRkaW5nID0gJzVweCc7XG4gICAgICAgIHRoaXMudmVyc2lvbkRpdi5zdHlsZS5mb250U2l6ZSA9ICcxMXB4JztcbiAgICAgICAgZG9jdW1lbnQuYm9keS5zdHlsZS5wYWRkaW5nQm90dG9tID0gJzE1cHgnO1xuICAgICAgICB0aGlzLnZlcnNpb25EaXYuY2xhc3NOYW1lID0gJ3dpZGdldC12ZXJzaW9uJztcbiAgICAgICAgdmFyIGJ1aWxkTnVtYmVyID0gYnVpbGRJbmZvcy5idWlsZE51bWJlcjtcbiAgICAgICAgdmFyIHRpbWUgPSBidWlsZEluZm9zLnRpbWU7XG4gICAgICAgIHZhciBjaVVzZXIgPSBidWlsZEluZm9zLmNpVXNlcjtcbiAgICAgICAgdmFyIGdpdFJlcG8gPSBidWlsZEluZm9zLmdpdFJlcG87XG4gICAgICAgIHZhciBnaXRCcmFuY2ggPSBidWlsZEluZm9zLmdpdEJyYW5jaDtcbiAgICAgICAgdGhpcy5sb2dnZXIubG9nKCdEaXNwbGF5IEJ1aWxkSW5mbzogJyArIGJ1aWxkTnVtYmVyICsgJyAtICcgKyB0aW1lKTtcbiAgICAgICAgaWYgKHRpbWUpIHtcbiAgICAgICAgICAgIHRoaXMudmVyc2lvbkRpdi5pbm5lckhUTUwgPSAnQnVpbGQgICcgKyBidWlsZE51bWJlciArICcgLSAnICsgdGltZSArICcgJztcbiAgICAgICAgICAgIHRoaXMudmVyc2lvbkRpdi5pbm5lckhUTUwgKz0gJ0J5ICcgKyBjaVVzZXIgKyAnICcgKyBnaXRSZXBvICsgJyBicmFuY2g6ICcgKyBnaXRCcmFuY2g7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnZlcnNpb25EaXYuaW5uZXJIVE1MID0gJzxiPicgKyBidWlsZE51bWJlciArICc8L2I+JztcbiAgICAgICAgfVxuICAgICAgICBkb2N1bWVudC5ib2R5Lmluc2VydEJlZm9yZSh0aGlzLnZlcnNpb25EaXYsIGRvY3VtZW50LmJvZHkuZmlyc3RDaGlsZCk7XG4gICAgfTtcbiAgICBCdWlsZEluZm9Nb2R1bGUucHJvdG90eXBlLmxvYWRWZXJzaW9uID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgIHZhciBqc29uRmlsZSA9ICdidWlsZC1pbmZvLmpzb24nO1xuICAgICAgICB2YXIgcmVxdWVzdCA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuICAgICAgICByZXF1ZXN0Lm9wZW4oJ0dFVCcsIGpzb25GaWxlLCB0cnVlKTtcbiAgICAgICAgcmVxdWVzdC5vbmxvYWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBpZiAocmVxdWVzdC5zdGF0dXMgPj0gMjAwICYmIHJlcXVlc3Quc3RhdHVzIDwgNDAwICYmIHJlcXVlc3QucmVzcG9uc2VUZXh0KSB7XG4gICAgICAgICAgICAgICAgdmFyIGRhdGEgPSByZXF1ZXN0LnJlc3BvbnNlVGV4dDtcbiAgICAgICAgICAgICAgICB2YXIgcGFyc2VkSnNvbiA9IEpTT04ucGFyc2UoZGF0YSk7XG4gICAgICAgICAgICAgICAgY2FsbGJhY2socGFyc2VkSnNvbik7XG4gICAgICAgICAgICB9XG4gICAgICAgIH07XG4gICAgICAgIHJlcXVlc3Quc2VuZCgpO1xuICAgIH07XG4gICAgcmV0dXJuIEJ1aWxkSW5mb01vZHVsZTtcbn0pKCk7XG52YXIgQ29uZmlnRGF0YU1vZHVsZSA9IChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gQ29uZmlnRGF0YU1vZHVsZShsb2dnZXIsIHJjZSwgc3RvcmFnZSkge1xuICAgICAgICB0aGlzLl9fc2ltdWxhdGVQcm9kdWN0aW9uID0gZmFsc2U7XG4gICAgICAgIHRoaXMuZGVmYXVsdEtleSA9ICdjb25maWc6JyArIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ2hlYWQgdGl0bGUnKS5pbm5lckhUTUw7XG4gICAgICAgIHRoaXMuYXR0ZW1wdHMgPSAwO1xuICAgICAgICB0aGlzLnBpbmdUaW1lb3V0ID0gMjAwO1xuICAgICAgICB0aGlzLmxvZ2dlciA9IGxvZ2dlcjtcbiAgICAgICAgdGhpcy5yY2UgPSByY2U7XG4gICAgICAgIHRoaXMuc3RvcmFnZSA9IHN0b3JhZ2U7XG4gICAgfVxuICAgIENvbmZpZ0RhdGFNb2R1bGUucHJvdG90eXBlLmlzTG9jYWwgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLl9fc2ltdWxhdGVQcm9kdWN0aW9uKSB7XG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMucmNlICYmIHRoaXMucmNlLmlzQXZhaWxhYmxlKCkpIHtcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAod2luZG93LmxvY2F0aW9uLmhvc3RuYW1lICYmIHdpbmRvdy5sb2NhdGlvbi5ob3N0bmFtZS5pbmRleE9mKCdsb2NhbGhvc3QnKSA+IC0xKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICBpZiAod2luZG93LmxvY2F0aW9uLmhyZWYgJiYgd2luZG93LmxvY2F0aW9uLmhyZWYuaW5kZXhPZignX190ZXN0X18nKSA+IC0xKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfTtcbiAgICBDb25maWdEYXRhTW9kdWxlLnByb3RvdHlwZS5pc0hhYml0YXQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh3aW5kb3cubG9jYXRpb24uaG9zdG5hbWUgJiYgd2luZG93LmxvY2F0aW9uLmhvc3RuYW1lLmluZGV4T2YoJ2lua2xpbmcnKSA+IC0xKSB7XG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfTtcbiAgICBDb25maWdEYXRhTW9kdWxlLnByb3RvdHlwZS5pc1Byb2R1Y3Rpb24gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiAhdGhpcy5pc0xvY2FsKCkgJiYgIXRoaXMuaXNIYWJpdGF0KCk7XG4gICAgfTtcbiAgICBDb25maWdEYXRhTW9kdWxlLnByb3RvdHlwZS5zZXREZWZhdWx0RGF0YSA9IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIHRoaXMuZGVmYXVsdERhdGEgPSBkYXRhO1xuICAgIH07XG4gICAgQ29uZmlnRGF0YU1vZHVsZS5wcm90b3R5cGUuZ2V0RGVmYXVsdERhdGEgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHREYXRhO1xuICAgIH07XG4gICAgQ29uZmlnRGF0YU1vZHVsZS5wcm90b3R5cGUuc2V0RGVmYXVsdEtleSA9IGZ1bmN0aW9uIChrZXkpIHtcbiAgICAgICAgdGhpcy5kZWZhdWx0S2V5ID0ga2V5O1xuICAgIH07XG4gICAgQ29uZmlnRGF0YU1vZHVsZS5wcm90b3R5cGUubG9hZCA9IGZ1bmN0aW9uIChjYWxsYmFjaywga2V5KSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnTE9BRElORyBDT05GSUdVUkFUSU9OIC4uLiAnLCB7XG4gICAgICAgICAgICBwcm9kdWN0aW9uOiB0aGlzLmlzUHJvZHVjdGlvbigpLFxuICAgICAgICAgICAgaGFiaXRhdDogdGhpcy5pc0hhYml0YXQoKSxcbiAgICAgICAgICAgIGxvY2FsOiB0aGlzLmlzTG9jYWwoKVxuICAgICAgICB9KTtcbiAgICAgICAgaWYgKCsrdGhpcy5hdHRlbXB0cyA+IDUpIHtcbiAgICAgICAgICAgIHRoaXMubG9nZ2VyLmVycm9yKCdHZW5lcmFsIE5ldHdvcmsgRmFpbHVyZScpO1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVDb25maWdFcnJvcihjYWxsYmFjaywga2V5KTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICB2YXIganNvbkZpbGUgPSB0aGlzLmdldEhhYml0YXRDb25maWcoKTtcbiAgICAgICAgaWYgKHRoaXMuaXNMb2NhbCgpIHx8IGpzb25GaWxlID09PSAnJykge1xuICAgICAgICAgICAgY2FsbGJhY2sodGhpcy5sb2FkQ29uZmlnTG9jYWwoa2V5KSk7XG4gICAgICAgIH1cbiAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ0pTT04gRklMRSBuYW1lIGlzICcsIGpzb25GaWxlKTtcbiAgICAgICAgICAgIHZhciByZXF1ZXN0ID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgICAgICByZXF1ZXN0Lm9wZW4oJ0dFVCcsIGpzb25GaWxlLCB0cnVlKTtcbiAgICAgICAgICAgIHJlcXVlc3Qub25sb2FkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIGlmIChyZXF1ZXN0LnN0YXR1cyA+PSAyMDAgJiYgcmVxdWVzdC5zdGF0dXMgPCA0MDAgJiYgcmVxdWVzdC5yZXNwb25zZVRleHQpIHtcbiAgICAgICAgICAgICAgICAgICAgX3RoaXMubG9nZ2VyLmxvZygnQ09ORklHLURBVEEnLCAnTG9hZCBzdWNjZXNzJywgcmVxdWVzdCk7XG4gICAgICAgICAgICAgICAgICAgIHZhciBwYXJzZWRKc29uID0gSlNPTi5wYXJzZShyZXF1ZXN0LnJlc3BvbnNlVGV4dCk7XG4gICAgICAgICAgICAgICAgICAgIGlmIChwYXJzZWRKc29uLmNvbmZpZykge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2socGFyc2VkSnNvbi5jb25maWcpO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIGVsc2UgaWYgKHBhcnNlZEpzb24uanNvbikge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2socGFyc2VkSnNvbi5qc29uKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHBhcnNlZEpzb24pO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBpZiAoX3RoaXMuaXNIYWJpdGF0KCkpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKF90aGlzLmxvYWRDb25maWdMb2NhbChrZXkpKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoY2FsbGJhY2soX3RoaXMubG9hZChjYWxsYmFjaywga2V5KSksIF90aGlzLnBpbmdUaW1lb3V0KTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH07XG4gICAgICAgICAgICByZXF1ZXN0Lm9uZXJyb3IgPSBmdW5jdGlvbiAoZXJyKSB7XG4gICAgICAgICAgICAgICAgX3RoaXMubG9nZ2VyLmxvZygnUkVRVUVTVCBFUlJPUicsIGVycik7XG4gICAgICAgICAgICAgICAgaWYgKF90aGlzLmlzSGFiaXRhdCgpKSB7XG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKF90aGlzLmxvYWRDb25maWdMb2NhbChrZXkpKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoY2FsbGJhY2soX3RoaXMubG9hZChjYWxsYmFjaywga2V5KSksIF90aGlzLnBpbmdUaW1lb3V0KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9O1xuICAgICAgICAgICAgcmVxdWVzdC5zZW5kKCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIENvbmZpZ0RhdGFNb2R1bGUucHJvdG90eXBlLnNhdmVBc1BhcmFtZXRlcnMgPSBmdW5jdGlvbiAoZGF0YSwga2V5KSB7XG4gICAgICAgIHRoaXMuZ2VuZXJpY1NhdmUoZGF0YSwgJ3NldCcsIGtleSk7XG4gICAgfTtcbiAgICBDb25maWdEYXRhTW9kdWxlLnByb3RvdHlwZS5zYXZlID0gZnVuY3Rpb24gKGRhdGEsIGtleSkge1xuICAgICAgICB0aGlzLmdlbmVyaWNTYXZlKGRhdGEsICdmaWxlJywga2V5KTtcbiAgICB9O1xuICAgIENvbmZpZ0RhdGFNb2R1bGUucHJvdG90eXBlLnVwbG9hZEFzc2V0ID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgIGlmICh0aGlzLmlzTG9jYWwoKSkge1xuICAgICAgICAgICAgdGhpcy5sb2dnZXIud2FybignVHJ5IHRvIG9wZW4gYXNzZXQgcGlja2VyIGluIGxvY2FsIGNvbnRleHQnKTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdtZXNzYWdlJywgY2FsbGJhY2spO1xuICAgICAgICAgICAgd2luZG93LnBhcmVudC5wb3N0TWVzc2FnZSh7XG4gICAgICAgICAgICAgICAgJ3R5cGUnOiAnYXNzZXQnLFxuICAgICAgICAgICAgICAgICdtZXRob2QnOiAnaW1hZ2UnLFxuICAgICAgICAgICAgICAgICdwYXlsb2FkJzoge31cbiAgICAgICAgICAgIH0sICcqJyk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIENvbmZpZ0RhdGFNb2R1bGUucHJvdG90eXBlLm9wZW5Bc3NldFBpY2tlciA9IGZ1bmN0aW9uIChjYWxsYmFjaykge1xuICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdERVBSRUNUQVRFRDogcGxlYXNlIHVzZSB1cGxvYWRBc3NldCgpIEFQSSBpbnN0ZWFkJyk7XG4gICAgICAgIHRoaXMudXBsb2FkQXNzZXQoY2FsbGJhY2spO1xuICAgIH07XG4gICAgQ29uZmlnRGF0YU1vZHVsZS5wcm90b3R5cGUudXBsb2FkSW1hZ2UgPSBmdW5jdGlvbiAoaW1hZ2UsIHByb2dyZXNzLCBjYWxsYmFjaykge1xuICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdERVBSRUNUQVRFRDogcGxlYXNlIHVzZSB1cGxvYWRBc3NldCgpIEFQSSBpbnN0ZWFkJyk7XG4gICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ1VOVVNFRCcsIGltYWdlLCBwcm9ncmVzcyk7XG4gICAgICAgIHRoaXMudXBsb2FkQXNzZXQoY2FsbGJhY2spO1xuICAgIH07XG4gICAgQ29uZmlnRGF0YU1vZHVsZS5wcm90b3R5cGUuY29tcGFyZSA9IGZ1bmN0aW9uIChkYXRhLCByZWZlcmVuY2UpIHtcbiAgICAgICAgdGhpcy5sb2dnZXIud2FybignREVQUkVDVEFURUQ6IHBsZWFzZSBkbyBub3QgdXNlJyk7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH07XG4gICAgQ29uZmlnRGF0YU1vZHVsZS5wcm90b3R5cGUuZ2V0SGFiaXRhdENvbmZpZyA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHM5ICYmIHM5LmluaXRpYWxQYXJhbXMgJiYgczkuaW5pdGlhbFBhcmFtcy5jb25maWdGaWxlKSB7XG4gICAgICAgICAgICByZXR1cm4gczkuaW5pdGlhbFBhcmFtcy5jb25maWdGaWxlO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAnJztcbiAgICB9O1xuICAgIENvbmZpZ0RhdGFNb2R1bGUucHJvdG90eXBlLnNhdmVDb25maWdIYWJpdGF0ID0gZnVuY3Rpb24gKHBheWxvYWQsIG1ldGhvZCkge1xuICAgICAgICBpZiAobWV0aG9kID09PSB2b2lkIDApIHsgbWV0aG9kID0gJ2ZpbGUnOyB9XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnQ0FMTElORyBNRScsICdzYXZlQ29uZmlnSGFiaXRhdCcsIHBheWxvYWQsIG1ldGhvZCk7XG4gICAgICAgIHdpbmRvdy5wYXJlbnQucG9zdE1lc3NhZ2Uoe1xuICAgICAgICAgICAgJ3R5cGUnOiAnY29uZmlndXJhdGlvbicsXG4gICAgICAgICAgICAnbWV0aG9kJzogbWV0aG9kLFxuICAgICAgICAgICAgJ3BheWxvYWQnOiBwYXlsb2FkXG4gICAgICAgIH0sICcqJyk7XG4gICAgfTtcbiAgICBDb25maWdEYXRhTW9kdWxlLnByb3RvdHlwZS5zYXZlQ29uZmlnTG9jYWwgPSBmdW5jdGlvbiAoZGF0YSwga2V5KSB7XG4gICAgICAgIGlmICh0aGlzLmlzTG9jYWwoKSkge1xuICAgICAgICAgICAgdGhpcy5zdG9yYWdlLnNldChrZXkgfHwgdGhpcy5kZWZhdWx0S2V5LCBKU09OLnN0cmluZ2lmeShkYXRhKSk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIENvbmZpZ0RhdGFNb2R1bGUucHJvdG90eXBlLmdlbmVyaWNTYXZlID0gZnVuY3Rpb24gKGRhdGEsIG1ldGhvZCwga2V5KSB7XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnU2F2ZSBkYXRhOlxcbicgKyBKU09OLnN0cmluZ2lmeShkYXRhKSk7XG4gICAgICAgIGlmICh0aGlzLmlzTG9jYWwoKSkge1xuICAgICAgICAgICAgdGhpcy5zYXZlQ29uZmlnTG9jYWwoZGF0YSwga2V5KTtcbiAgICAgICAgfVxuICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgIGlmIChtZXRob2QgJiYgbWV0aG9kID09PSAnc2V0Jykge1xuICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUNvbmZpZ0hhYml0YXQoZGF0YSwgbWV0aG9kKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUNvbmZpZ0hhYml0YXQoeyBjb25maWc6IGRhdGEgfSwgbWV0aG9kKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH07XG4gICAgQ29uZmlnRGF0YU1vZHVsZS5wcm90b3R5cGUubG9hZENvbmZpZ0xvY2FsID0gZnVuY3Rpb24gKGtleSkge1xuICAgICAgICBpZiAoa2V5ID09PSB2b2lkIDApIHsga2V5ID0gdGhpcy5kZWZhdWx0S2V5OyB9XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnQ2FsbGluZyBsb2FkQ29uZmlnTG9jYWwnLCBrZXksIHRoaXMuc3RvcmFnZS5nZXQoa2V5KSk7XG4gICAgICAgIHZhciByYXdEYXRhID0gdGhpcy5zdG9yYWdlLmdldChrZXkpO1xuICAgICAgICB2YXIgZGF0YSA9IHR5cGVvZiByYXdEYXRhID09PSAnc3RyaW5nJyA/IEpTT04ucGFyc2UocmF3RGF0YSkgOiByYXdEYXRhO1xuICAgICAgICByZXR1cm4gZGF0YSB8fCB0aGlzLmRlZmF1bHREYXRhO1xuICAgIH07XG4gICAgQ29uZmlnRGF0YU1vZHVsZS5wcm90b3R5cGUuaGFuZGxlQ29uZmlnRXJyb3IgPSBmdW5jdGlvbiAoY2FsbGJhY2ssIGtleSkge1xuICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdDT05GSUctREFUQScsICdDb25maWd1cmF0aW9uIERhdGEgY291bGQgbm90IGJlIGxvYWRlZCcpO1xuICAgICAgICBjYWxsYmFjayhudWxsKTtcbiAgICB9O1xuICAgIHJldHVybiBDb25maWdEYXRhTW9kdWxlO1xufSkoKTtcbnZhciBEaWFsb2dNb2R1bGUgPSAoZnVuY3Rpb24gKCkge1xuICAgIGZ1bmN0aW9uIERpYWxvZ01vZHVsZShsb2dnZXIpIHtcbiAgICAgICAgdGhpcy5sb2dnZXIgPSBsb2dnZXI7XG4gICAgfVxuICAgIERpYWxvZ01vZHVsZS5wcm90b3R5cGUuYWxlcnQgPSBmdW5jdGlvbiAoc3RyKSB7XG4gICAgfTtcbiAgICBEaWFsb2dNb2R1bGUucHJvdG90eXBlLmNvbmZpcm0gPSBmdW5jdGlvbiAoc3RyKSB7XG4gICAgICAgIHJldHVybiB0cnVlO1xuICAgIH07XG4gICAgcmV0dXJuIERpYWxvZ01vZHVsZTtcbn0pKCk7XG52YXIgUmVzaXplTW9kdWxlID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBSZXNpemVNb2R1bGUobG9nZ2VyLCBlbWl0dGVyLCBjb25maWcpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgdGhpcy5kZWxheSA9IDEwMDtcbiAgICAgICAgdGhpcy5kZWJvdW5jaW5nUmF0ZSA9IDI1MDtcbiAgICAgICAgdGhpcy5sb2dnZXIgPSBsb2dnZXI7XG4gICAgICAgIHRoaXMuZW1pdHRlciA9IGVtaXR0ZXI7XG4gICAgICAgIHRoaXMuY29uZmlnID0gY29uZmlnO1xuICAgICAgICB2YXIgaGFuZGxlciA9IHRoaXMuZGVib3VuY2UodGhpcy5vblJlc2l6ZSwgdGhpcyk7XG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdyZXNpemUnLCBoYW5kbGVyKTtcbiAgICAgICAgd2luZG93LmFkZEV2ZW50TGlzdGVuZXIoJ29yaWVudGF0aW9uY2hhbmdlJywgaGFuZGxlcik7XG4gICAgICAgIHRoaXMuZW1pdHRlci5vbigncmVzaXplJywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMucmVzaXplKCk7IH0pO1xuICAgIH1cbiAgICBSZXNpemVNb2R1bGUucHJvdG90eXBlLnNldEZpeGVkRGltZW5zaW9ucyA9IGZ1bmN0aW9uIChoZWlnaHQsIHdpZHRoKSB7XG4gICAgICAgIGlmIChoZWlnaHQgPT09IHZvaWQgMCkgeyBoZWlnaHQgPSAwOyB9XG4gICAgICAgIGlmICh3aWR0aCA9PT0gdm9pZCAwKSB7IHdpZHRoID0gMDsgfVxuICAgICAgICB0aGlzLmZpeGVkSGVpZ2h0ID0gaGVpZ2h0O1xuICAgICAgICB0aGlzLmZpeGVkV2lkdGggPSB3aWR0aDtcbiAgICB9O1xuICAgIFJlc2l6ZU1vZHVsZS5wcm90b3R5cGUucmVzaXplT25FdmVudCA9IGZ1bmN0aW9uIChldmVudE5hbWUpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmRhdGFzZXQud2lkZ2V0SGVpZ2h0ID0gU3RyaW5nKHRoaXMucmVzaXplKCkpO1xuICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ3Jlc2l6ZU1vZHVsZScsICdDdXJyZW50bHkgc2F2ZWQgaGVpZ2h0JywgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmRhdGFzZXQud2lkZ2V0SGVpZ2h0KTtcbiAgICAgICAgdGhpcy5lbWl0dGVyLm9uY2UoZXZlbnROYW1lLCBmdW5jdGlvbiAoKSB7IHJldHVybiBfdGhpcy5yZWNvdmVyU2l6ZSgpOyB9KTtcbiAgICB9O1xuICAgIFJlc2l6ZU1vZHVsZS5wcm90b3R5cGUucmVzaXplID0gZnVuY3Rpb24gKGhlaWdodCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICBpZiAoczkgJiYgczkudmlldykge1xuICAgICAgICAgICAgdGhpcy5lbWl0dGVyLmVtaXQoJ3Jlc2l6ZTpiZWZvcmUnKTtcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgICAgIHZhciBkaW1lbnNpb25zID0ge1xuICAgICAgICAgICAgICAgICAgICBoZWlnaHQ6IF90aGlzLmZpeGVkSGVpZ2h0IHx8IGhlaWdodCB8fCBfdGhpcy5nZXRIZWlnaHQoKVxuICAgICAgICAgICAgICAgIH07XG4gICAgICAgICAgICAgICAgaWYgKF90aGlzLmZpeGVkV2lkdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgZGltZW5zaW9ucy53aWR0aCA9IF90aGlzLmZpeGVkV2lkdGg7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIF90aGlzLmxvZ2dlci5sb2coJ1JFU0laSU5HLi4uJywgZGltZW5zaW9ucyk7XG4gICAgICAgICAgICAgICAgczkudmlldy5zaXplKGRpbWVuc2lvbnMpO1xuICAgICAgICAgICAgICAgIF90aGlzLmVtaXR0ZXIuZW1pdCgncmVzaXplOmFmdGVyJyk7XG4gICAgICAgICAgICB9LCB0aGlzLmRlbGF5KTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgUmVzaXplTW9kdWxlLnByb3RvdHlwZS5nZXRIZWlnaHQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBib2R5ID0gZG9jdW1lbnQuYm9keTtcbiAgICAgICAgdmFyIGh0bWwgPSBkb2N1bWVudC5kb2N1bWVudEVsZW1lbnQ7XG4gICAgICAgIHJldHVybiBNYXRoLm1heChib2R5LmNsaWVudEhlaWdodCwgYm9keS5vZmZzZXRIZWlnaHQsIGh0bWwub2Zmc2V0SGVpZ2h0KTtcbiAgICB9O1xuICAgIFJlc2l6ZU1vZHVsZS5wcm90b3R5cGUub25SZXNpemUgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMucmVzaXplKCk7XG4gICAgfTtcbiAgICBSZXNpemVNb2R1bGUucHJvdG90eXBlLnJlY292ZXJTaXplID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB2YXIgaGVpZ2h0ID0gcGFyc2VJbnQoZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmRhdGFzZXQud2lkZ2V0SGVpZ2h0LCAxMCk7XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnUmVjb3ZlcmluZyBzaXplJywgaGVpZ2h0KTtcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICBfdGhpcy5yZXNpemUoaGVpZ2h0KTtcbiAgICAgICAgfSwgdGhpcy5kZWxheSk7XG4gICAgICAgIHJldHVybiBoZWlnaHQ7XG4gICAgfTtcbiAgICBSZXNpemVNb2R1bGUucHJvdG90eXBlLmRlYm91bmNlID0gZnVuY3Rpb24gKGZ1bmMsIGNvbnRleHQsIHdhaXQsIGltbWVkaWF0ZSkge1xuICAgICAgICBpZiAod2FpdCA9PT0gdm9pZCAwKSB7IHdhaXQgPSB0aGlzLmRlYm91bmNpbmdSYXRlOyB9XG4gICAgICAgIGlmIChpbW1lZGlhdGUgPT09IHZvaWQgMCkgeyBpbW1lZGlhdGUgPSBmYWxzZTsgfVxuICAgICAgICB2YXIgdGltZW91dDtcbiAgICAgICAgcmV0dXJuIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgIHZhciBhcmdzID0gW107XG4gICAgICAgICAgICBmb3IgKHZhciBfaSA9IDA7IF9pIDwgYXJndW1lbnRzLmxlbmd0aDsgX2krKykge1xuICAgICAgICAgICAgICAgIGFyZ3NbX2kgLSAwXSA9IGFyZ3VtZW50c1tfaV07XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB2YXIgY2FsbE5vdyA9IGltbWVkaWF0ZSAmJiAhdGltZW91dDtcbiAgICAgICAgICAgIGNsZWFyVGltZW91dCh0aW1lb3V0KTtcbiAgICAgICAgICAgIHRpbWVvdXQgPSBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICB0aW1lb3V0ID0gbnVsbDtcbiAgICAgICAgICAgICAgICBpZiAoIWltbWVkaWF0ZSkge1xuICAgICAgICAgICAgICAgICAgICBmdW5jLmFwcGx5LmFwcGx5KGZ1bmMsIFtjb250ZXh0XS5jb25jYXQoYXJncykpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIHdhaXQpO1xuICAgICAgICAgICAgaWYgKGNhbGxOb3cpIHtcbiAgICAgICAgICAgICAgICBmdW5jLmFwcGx5KGNvbnRleHQsIGFyZ3MpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9O1xuICAgIH07XG4gICAgcmV0dXJuIFJlc2l6ZU1vZHVsZTtcbn0pKCk7XG52YXIgRnVsbHNjcmVlbk1vZHVsZSA9IChmdW5jdGlvbiAoKSB7XG4gICAgZnVuY3Rpb24gRnVsbHNjcmVlbk1vZHVsZShsb2dnZXIsIHJlc2l6ZXIsIGNvbmZpZykge1xuICAgICAgICB0aGlzLnN1cHBvcnRzRnVsbFNjcmVlbiA9IGZhbHNlO1xuICAgICAgICB0aGlzLmZ1bGxTY3JlZW5FdmVudE5hbWUgPSAnJztcbiAgICAgICAgdGhpcy5wcmVmaXggPSAnJztcbiAgICAgICAgdGhpcy5icm93c2VyUHJlZml4ZXMgPSAnd2Via2l0LG1veixvLG1zLGtodG1sJy5zcGxpdCgnLCcpO1xuICAgICAgICB0aGlzLmxvZ2dlciA9IGxvZ2dlcjtcbiAgICAgICAgdGhpcy5yZXNpemVyID0gcmVzaXplcjtcbiAgICAgICAgdGhpcy5jb25maWcgPSBjb25maWc7XG4gICAgICAgIHRoaXMuZW5hYmxlRnVsbFNjcmVlbk9uSWZyYW1lKCk7XG4gICAgICAgIHRoaXMuZGV0ZWN0RnVsbFNjcmVlblN1cHBvcnQoKTtcbiAgICAgICAgdGhpcy51cGRhdGVGdWxsU2NyZWVuRXZlbnROYW1lKCk7XG4gICAgfVxuICAgIEZ1bGxzY3JlZW5Nb2R1bGUucHJvdG90eXBlLmdldEV2ZW50TmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZnVsbFNjcmVlbkV2ZW50TmFtZTtcbiAgICB9O1xuICAgIEZ1bGxzY3JlZW5Nb2R1bGUucHJvdG90eXBlLmlzRnVsbFNjcmVlblN1cHBvcnRlZCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3VwcG9ydHNGdWxsU2NyZWVuO1xuICAgIH07XG4gICAgRnVsbHNjcmVlbk1vZHVsZS5wcm90b3R5cGUuaXNGdWxsU2NyZWVuID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBzd2l0Y2ggKHRoaXMucHJlZml4KSB7XG4gICAgICAgICAgICBjYXNlICcnOlxuICAgICAgICAgICAgICAgIHJldHVybiB0eXBlb2YgZG9jdW1lbnQuZnVsbHNjcmVlbkVsZW1lbnQgIT09ICd1bmRlZmluZWQnICYmIGRvY3VtZW50LmZ1bGxzY3JlZW5FbGVtZW50ICE9PSBudWxsO1xuICAgICAgICAgICAgZGVmYXVsdDpcbiAgICAgICAgICAgICAgICByZXR1cm4gZG9jdW1lbnRbdGhpcy5wcmVmaXggKyAnRnVsbHNjcmVlbkVsZW1lbnQnXSB8fCBkb2N1bWVudFt0aGlzLnByZWZpeCArICdGdWxsU2NyZWVuRWxlbWVudCddO1xuICAgICAgICB9XG4gICAgfTtcbiAgICBGdWxsc2NyZWVuTW9kdWxlLnByb3RvdHlwZS5yZXF1ZXN0RnVsbFNjcmVlbiA9IGZ1bmN0aW9uIChlbCkge1xuICAgICAgICBpZiAoZWwgPT09IHZvaWQgMCkgeyBlbCA9IGRvY3VtZW50LmRvY3VtZW50RWxlbWVudDsgfVxuICAgICAgICBpZiAoZWwgPT09IG51bGwpIHtcbiAgICAgICAgICAgIGVsID0gZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50O1xuICAgICAgICB9XG4gICAgICAgIGlmICh0aGlzLnN1cHBvcnRzRnVsbFNjcmVlbikge1xuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QuYWRkKCdmdWxsc2NyZWVuJyk7XG4gICAgICAgICAgICB0aGlzLnJlc2l6ZXIucmVzaXplT25FdmVudCh0aGlzLmZ1bGxTY3JlZW5FdmVudE5hbWUpO1xuICAgICAgICAgICAgaWYgKHR5cGVvZiBlbC5yZXF1ZXN0RnVsbFNjcmVlbiAhPT0gJ3VuZGVmaW5lZCcgfHxcbiAgICAgICAgICAgICAgICB0eXBlb2YgZWxbdGhpcy5wcmVmaXggKyAnUmVxdWVzdEZ1bGxTY3JlZW4nXSAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAodGhpcy5wcmVmaXggPT09ICcnKSA/IGVsLnJlcXVlc3RGdWxsU2NyZWVuKCkgOiBlbFt0aGlzLnByZWZpeCArICdSZXF1ZXN0RnVsbFNjcmVlbiddKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICh0eXBlb2YgZWwucmVxdWVzdEZ1bGxzY3JlZW4gIT09ICd1bmRlZmluZWQnIHx8XG4gICAgICAgICAgICAgICAgdHlwZW9mIGVsW3RoaXMucHJlZml4ICsgJ1JlcXVlc3RGdWxsc2NyZWVuJ10gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgKHRoaXMucHJlZml4ID09PSAnJykgPyBlbC5yZXF1ZXN0RnVsbHNjcmVlbigpIDogZWxbdGhpcy5wcmVmaXggKyAnUmVxdWVzdEZ1bGxzY3JlZW4nXSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH07XG4gICAgRnVsbHNjcmVlbk1vZHVsZS5wcm90b3R5cGUuZXhpdEZ1bGxTY3JlZW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0aGlzLnN1cHBvcnRzRnVsbFNjcmVlbikge1xuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5jbGFzc0xpc3QucmVtb3ZlKCdmdWxsc2NyZWVuJyk7XG4gICAgICAgICAgICBpZiAodGhpcy5wcmVmaXggPT09ICcnKSB7XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBkb2N1bWVudC5jYW5jZWxGdWxsU2NyZWVuICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICBkb2N1bWVudC5jYW5jZWxGdWxsU2NyZWVuKCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2UgaWYgKHR5cGVvZiBkb2N1bWVudC5leGl0RnVsbHNjcmVlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgICAgICAgICAgZG9jdW1lbnQuZXhpdEZ1bGxzY3JlZW4oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIGlmICh0aGlzLnByZWZpeCA9PT0gJ21zJykge1xuICAgICAgICAgICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ0ZVTExTQ1JFRU4nLCAnRXhpdGluZycpO1xuICAgICAgICAgICAgICAgIGRvY3VtZW50Lm1zRXhpdEZ1bGxzY3JlZW4oKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgZG9jdW1lbnRbdGhpcy5wcmVmaXggKyAnQ2FuY2VsRnVsbFNjcmVlbiddICE9PSAndW5kZWZpbmVkJykge1xuICAgICAgICAgICAgICAgICAgICBkb2N1bWVudFt0aGlzLnByZWZpeCArICdDYW5jZWxGdWxsU2NyZWVuJ10oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIGRvY3VtZW50W3RoaXMucHJlZml4ICsgJ0V4aXRGdWxsc2NyZWVuJ10oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9O1xuICAgIEZ1bGxzY3JlZW5Nb2R1bGUucHJvdG90eXBlLmNhbmNlbEZ1bGxTY3JlZW4gPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRoaXMubG9nZ2VyLndhcm4oJ0RFUFJFQ0FURUQ6IHVzZSBleGl0RnVsbFNjcmVlbigpIGluc3RlYWQnKTtcbiAgICAgICAgdGhpcy5leGl0RnVsbFNjcmVlbigpO1xuICAgIH07XG4gICAgRnVsbHNjcmVlbk1vZHVsZS5wcm90b3R5cGUuZGV0ZWN0RnVsbFNjcmVlblN1cHBvcnQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIGlmICh0eXBlb2YgZG9jdW1lbnQuY2FuY2VsRnVsbFNjcmVlbiAhPT0gJ3VuZGVmaW5lZCcgfHxcbiAgICAgICAgICAgIHR5cGVvZiBkb2N1bWVudC5leGl0RnVsbHNjcmVlbiAhPT0gJ3VuZGVmaW5lZCcpIHtcbiAgICAgICAgICAgIHRoaXMuc3VwcG9ydHNGdWxsU2NyZWVuID0gIXRoaXMuY29uZmlnLmlzSGFiaXRhdCgpO1xuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDAsIGlsID0gdGhpcy5icm93c2VyUHJlZml4ZXMubGVuZ3RoOyBpIDwgaWw7IGkrKykge1xuICAgICAgICAgICAgICAgIHRoaXMucHJlZml4ID0gdGhpcy5icm93c2VyUHJlZml4ZXNbaV07XG4gICAgICAgICAgICAgICAgaWYgKHR5cGVvZiBkb2N1bWVudFt0aGlzLnByZWZpeCArICdDYW5jZWxGdWxsU2NyZWVuJ10gIT09ICd1bmRlZmluZWQnIHx8XG4gICAgICAgICAgICAgICAgICAgIHR5cGVvZiBkb2N1bWVudFt0aGlzLnByZWZpeCArICdFeGl0RnVsbHNjcmVlbiddICE9PSAndW5kZWZpbmVkJyB8fFxuICAgICAgICAgICAgICAgICAgICB0eXBlb2YgZG9jdW1lbnQuZXhpdEZ1bGxzY3JlZW4gIT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc3VwcG9ydHNGdWxsU2NyZWVuID0gIXRoaXMuY29uZmlnLmlzSGFiaXRhdCgpO1xuICAgICAgICAgICAgICAgICAgICBicmVhaztcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuc3VwcG9ydHNGdWxsU2NyZWVuID09PSBmYWxzZSkge1xuICAgICAgICAgICAgZG9jdW1lbnQuZG9jdW1lbnRFbGVtZW50LmNsYXNzTGlzdC5hZGQoJ3Vuc3VwcG9ydGVkLWZ1bGxzY3JlZW4nKTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgRnVsbHNjcmVlbk1vZHVsZS5wcm90b3R5cGUudXBkYXRlRnVsbFNjcmVlbkV2ZW50TmFtZSA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuc3VwcG9ydHNGdWxsU2NyZWVuKSB7XG4gICAgICAgICAgICB0aGlzLmZ1bGxTY3JlZW5FdmVudE5hbWUgPSB0aGlzLnByZWZpeCArICdmdWxsc2NyZWVuY2hhbmdlJztcbiAgICAgICAgfVxuICAgIH07XG4gICAgRnVsbHNjcmVlbk1vZHVsZS5wcm90b3R5cGUuZW5hYmxlRnVsbFNjcmVlbk9uSWZyYW1lID0gZnVuY3Rpb24gKCkge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgdmFyIGlmcmFtZU5vZGUgPSB3aW5kb3cuZnJhbWVFbGVtZW50IHx8IHdpbmRvdy5lbGVtZW50O1xuICAgICAgICAgICAgaWYgKGlmcmFtZU5vZGUpIHtcbiAgICAgICAgICAgICAgICBpZnJhbWVOb2RlLnNldEF0dHJpYnV0ZSgnYWxsb3dmdWxsc2NyZWVuJywgJ3RydWUnKTtcbiAgICAgICAgICAgICAgICBpZnJhbWVOb2RlLnNldEF0dHJpYnV0ZSgnd2Via2l0YWxsb3dmdWxsc2NyZWVuJywgJ3RydWUnKTtcbiAgICAgICAgICAgICAgICBpZnJhbWVOb2RlLnNldEF0dHJpYnV0ZSgnbW96YWxsb3dmdWxsc2NyZWVuJywgJ3RydWUnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdlbmFibGVGdWxsU2NyZWVuT25JZnJhbWUnLCBlcnIpO1xuICAgICAgICB9XG4gICAgfTtcbiAgICByZXR1cm4gRnVsbHNjcmVlbk1vZHVsZTtcbn0pKCk7XG52YXIgTGF5b3V0TW9kdWxlID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBMYXlvdXRNb2R1bGUobG9nZ2VyLCByY2UsIGVtaXR0ZXIsIG9wdGlvbnMpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgaWYgKG9wdGlvbnMgPT09IHZvaWQgMCkgeyBvcHRpb25zID0geyByZXNpemVUb1ZpZXdwb3J0OiBmYWxzZSB9OyB9XG4gICAgICAgIHRoaXMuaW5pdGlhbGl6ZWQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5leHBhbmQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5sb2dnZXIgPSBsb2dnZXI7XG4gICAgICAgIHRoaXMucmNlID0gcmNlO1xuICAgICAgICB0aGlzLmVtaXR0ZXIgPSBlbWl0dGVyO1xuICAgICAgICB0aGlzLmV4cGFuZCA9IG9wdGlvbnMucmVzaXplVG9WaWV3cG9ydDtcbiAgICAgICAgaWYgKHRoaXMucmNlLmlzQXZhaWxhYmxlKCkpIHtcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsaW5nRWxlbWVudCA9IHdpbmRvdy5wYXJlbnQucGFyZW50LmRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdzY3JvbGxlZC1jb250ZW50LWZyYW1lJyk7XG4gICAgICAgICAgICBpZiAoIXRoaXMuc2Nyb2xsaW5nRWxlbWVudCkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2Nyb2xsaW5nQ29udGFpbmVyID0gd2luZG93LnBhcmVudDtcbiAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbGluZ0VsZW1lbnQgPSB3aW5kb3cucGFyZW50LmRvY3VtZW50LmJvZHk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNjcm9sbGluZ0NvbnRhaW5lciA9IHRoaXMuc2Nyb2xsaW5nRWxlbWVudDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuZW1pdHRlci5vbmNlKCdyY2U6cmVhZHknLCBmdW5jdGlvbiAoKSB7IHJldHVybiBfdGhpcy5wYXJzZUNvbnRlbnQoKTsgfSk7XG4gICAgICAgICAgICB0aGlzLmluaXRpYWxpemVkID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuZXhwYW5kV2lkZ2V0VG9WaWV3cG9ydCgpO1xuICAgICAgICAgICAgdGhpcy5lbWl0dGVyLm9uKCdyZXNpemU6YmVmb3JlJywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMuZXhwYW5kV2lkZ2V0VG9WaWV3cG9ydCgpOyB9KTtcbiAgICAgICAgICAgIHRoaXMuZW1pdHRlci5vbigncmVzaXplOmFmdGVyJywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMuYnViYmxlUmVzaXplVXBUb1JDRSgpOyB9KTtcbiAgICAgICAgfVxuICAgIH1cbiAgICBMYXlvdXRNb2R1bGUucHJvdG90eXBlLnBhcnNlQ29udGVudCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgdmFyIF90aGlzID0gdGhpcztcbiAgICAgICAgdGhpcy5sb2dnZXIubG9nKCdMQVlPVVQnLCAnUGFzaW5nIHRoZSBjb250ZW50IGZvciBzcGVjaWZpYyBkYXRhIGF0dHJpYnV0ZXMnKTtcbiAgICAgICAgdmFyIGhlYWRlcnMgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yQWxsKCdbZGF0YS1oZWFkZXJdJyk7XG4gICAgICAgIEFycmF5LnByb3RvdHlwZS5mb3JFYWNoLmNhbGwoaGVhZGVycywgZnVuY3Rpb24gKGVsKSB7IHJldHVybiBfdGhpcy5zZXRGaXhlZEhlYWRlcihlbCk7IH0pO1xuICAgICAgICB0aGlzLnNldEZpeGVkRm9vdGVyKGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJ1tkYXRhLWZvb3Rlcl0nKSk7XG4gICAgfTtcbiAgICBMYXlvdXRNb2R1bGUucHJvdG90eXBlLmlzSW5pdGlhbGl6ZWQgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmluaXRpYWxpemVkICYmIHRoaXMuc2Nyb2xsaW5nRWxlbWVudCAhPT0gbnVsbDtcbiAgICB9O1xuICAgIExheW91dE1vZHVsZS5wcm90b3R5cGUuZXhwYW5kV2lkZ2V0VG9WaWV3cG9ydCA9IGZ1bmN0aW9uICgpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNJbml0aWFsaXplZCgpICYmIHRoaXMuZXhwYW5kKSB7XG4gICAgICAgICAgICB0aGlzLmJ1YmJsZVJlc2l6ZVVwVG9SQ0UoKTtcbiAgICAgICAgICAgIHZhciB2aWV3cG9ydEhlaWdodCA9IGdldENvbXB1dGVkU3R5bGUodGhpcy5zY3JvbGxpbmdFbGVtZW50KS5oZWlnaHQ7XG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LnN0eWxlLm1pbkhlaWdodCA9IHZpZXdwb3J0SGVpZ2h0O1xuICAgICAgICAgICAgdmFyIG5vZGVzID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvckFsbCgnW2RhdGEtZnVsbC1oZWlnaHRdJyk7XG4gICAgICAgICAgICBBcnJheS5wcm90b3R5cGUuZm9yRWFjaC5jYWxsKG5vZGVzLCBmdW5jdGlvbiAoZWwpIHsgcmV0dXJuIGVsLnN0eWxlLm1pbkhlaWdodCA9IHZpZXdwb3J0SGVpZ2h0OyB9KTtcbiAgICAgICAgfVxuICAgIH07XG4gICAgTGF5b3V0TW9kdWxlLnByb3RvdHlwZS5idWJibGVSZXNpemVVcFRvUkNFID0gZnVuY3Rpb24gKCkge1xuICAgICAgICBpZiAodGhpcy5pc0luaXRpYWxpemVkKCkpIHtcbiAgICAgICAgICAgIHZhciBldnQ7XG4gICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgIGV2dCA9IG5ldyBFdmVudCgncmVzaXplJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBjYXRjaCAoZSkge1xuICAgICAgICAgICAgICAgIGV2dCA9IHdpbmRvdy5wYXJlbnQucGFyZW50LmRvY3VtZW50LmNyZWF0ZUV2ZW50KCdVSUV2ZW50cycpO1xuICAgICAgICAgICAgICAgIGV2dC5pbml0VUlFdmVudCgncmVzaXplJywgdHJ1ZSwgZmFsc2UsIHdpbmRvdywgMCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB3aW5kb3cucGFyZW50LnBhcmVudC5kaXNwYXRjaEV2ZW50KGV2dCk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIExheW91dE1vZHVsZS5wcm90b3R5cGUuc2V0Rml4ZWRIZWFkZXIgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ0xBWU9VVCcsICdzZXRGaXhlZEhlYWRlcicsIGVsZW1lbnQpO1xuICAgICAgICBpZiAodGhpcy5pc0luaXRpYWxpemVkKCkgJiYgZWxlbWVudCkge1xuICAgICAgICAgICAgZWxlbWVudC5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XG4gICAgICAgICAgICB0aGlzLnJlcG9zaXRpb25IZWFkZXJPblJlc2l6ZShlbGVtZW50KTtcbiAgICAgICAgICAgIHRoaXMuZW1pdHRlci5vbigncmVzaXplOmFmdGVyJywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMucmVwb3NpdGlvbkhlYWRlck9uUmVzaXplKGVsZW1lbnQpOyB9KTtcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsaW5nQ29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIGZ1bmN0aW9uICgpIHsgcmV0dXJuIF90aGlzLnJlcG9zaXRpb25IZWFkZXJPblNjcm9sbChlbGVtZW50KTsgfSk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIExheW91dE1vZHVsZS5wcm90b3R5cGUuc2V0Rml4ZWRGb290ZXIgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ0xBWU9VVCcsICdzZXRGaXhlZEZvb3RlcicsIGVsZW1lbnQpO1xuICAgICAgICBpZiAodGhpcy5pc0luaXRpYWxpemVkKCkgJiYgZWxlbWVudCkge1xuICAgICAgICAgICAgZWxlbWVudC5zdHlsZS5wb3NpdGlvbiA9ICdhYnNvbHV0ZSc7XG4gICAgICAgICAgICB0aGlzLnJlcG9zaXRpb25Gb290ZXJPblJlc2l6ZShlbGVtZW50KTtcbiAgICAgICAgICAgIHRoaXMuZW1pdHRlci5vbigncmVzaXplOmFmdGVyJywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMucmVwb3NpdGlvbkZvb3Rlck9uUmVzaXplKGVsZW1lbnQpOyB9KTtcbiAgICAgICAgICAgIHRoaXMuc2Nyb2xsaW5nQ29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIGZ1bmN0aW9uICgpIHsgcmV0dXJuIF90aGlzLnJlcG9zaXRpb25Gb290ZXJPblNjcm9sbChlbGVtZW50KTsgfSk7XG4gICAgICAgIH1cbiAgICB9O1xuICAgIExheW91dE1vZHVsZS5wcm90b3R5cGUucmVwb3NpdGlvbkZvb3Rlck9uUmVzaXplID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgICAgZWxlbWVudC5zdHlsZS5ib3R0b20gPSAnMCc7XG4gICAgICAgIHRoaXMuZXhwYW5kV2lkZ2V0VG9WaWV3cG9ydCgpO1xuICAgICAgICB0aGlzLmJvdHRvbVBvc2l0aW9uID0gZWxlbWVudC5nZXRCb3VuZGluZ0NsaWVudFJlY3QoKS5ib3R0b20gLSB3aW5kb3cucGFyZW50LnBhcmVudC5kb2N1bWVudC5kb2N1bWVudEVsZW1lbnQuY2xpZW50SGVpZ2h0O1xuICAgICAgICB0aGlzLmxvZ2dlci5sb2coJ0xBWU9VVCcsICdib3R0b20gcG9zaXRpb24nLCB0aGlzLmJvdHRvbVBvc2l0aW9uKTtcbiAgICAgICAgZWxlbWVudC5zdHlsZS5ib3R0b20gPSBNYXRoLm1heCh0aGlzLmJvdHRvbVBvc2l0aW9uLCAwKSArICdweCc7XG4gICAgICAgIHRoaXMucmVwb3NpdGlvbkZvb3Rlck9uU2Nyb2xsKGVsZW1lbnQpO1xuICAgIH07XG4gICAgTGF5b3V0TW9kdWxlLnByb3RvdHlwZS5yZXBvc2l0aW9uSGVhZGVyT25SZXNpemUgPSBmdW5jdGlvbiAoZWxlbWVudCkge1xuICAgICAgICB0aGlzLnNjcm9sbGluZ0VsZW1lbnQuc2Nyb2xsVG9wID0gMDtcbiAgICAgICAgZWxlbWVudC5zdHlsZS50b3AgPSAnMCc7XG4gICAgICAgIHRoaXMuZXhwYW5kV2lkZ2V0VG9WaWV3cG9ydCgpO1xuICAgICAgICB0aGlzLnRvcFBvc2l0aW9uID0gMDtcbiAgICAgICAgdGhpcy5sb2dnZXIubG9nKCdMQVlPVVQnLCAndG9wIHBvc2l0aW9uJywgdGhpcy50b3BQb3NpdGlvbik7XG4gICAgICAgIGVsZW1lbnQuc3R5bGUudG9wID0gTWF0aC5tYXgodGhpcy50b3BQb3NpdGlvbiwgMCkgKyAncHgnO1xuICAgICAgICB0aGlzLnJlcG9zaXRpb25IZWFkZXJPblNjcm9sbChlbGVtZW50KTtcbiAgICB9O1xuICAgIExheW91dE1vZHVsZS5wcm90b3R5cGUucmVwb3NpdGlvbkhlYWRlck9uU2Nyb2xsID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgICAgdmFyIGRlbHRhID0gdGhpcy5zY3JvbGxpbmdFbGVtZW50LnNjcm9sbFRvcCAtIHRoaXMudG9wUG9zaXRpb247XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnTEFZT1VUJywgJ2RlbHRhIHBvc2l0aW9uJywgZGVsdGEpO1xuICAgICAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoZnVuY3Rpb24gKCkgeyByZXR1cm4gZWxlbWVudC5zdHlsZS50b3AgPSBNYXRoLm1heChkZWx0YSwgMCkgKyAncHgnOyB9KTtcbiAgICB9O1xuICAgIExheW91dE1vZHVsZS5wcm90b3R5cGUucmVwb3NpdGlvbkZvb3Rlck9uU2Nyb2xsID0gZnVuY3Rpb24gKGVsZW1lbnQpIHtcbiAgICAgICAgdmFyIGRlbHRhID0gdGhpcy5ib3R0b21Qb3NpdGlvbiAtIHRoaXMuc2Nyb2xsaW5nRWxlbWVudC5zY3JvbGxUb3A7XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnTEFZT1VUJywgJ2RlbHRhIHBvc2l0aW9uJywgZGVsdGEpO1xuICAgICAgICByZXF1ZXN0QW5pbWF0aW9uRnJhbWUoZnVuY3Rpb24gKCkgeyByZXR1cm4gZWxlbWVudC5zdHlsZS5ib3R0b20gPSBNYXRoLm1heChkZWx0YSwgMCkgKyAncHgnOyB9KTtcbiAgICB9O1xuICAgIHJldHVybiBMYXlvdXRNb2R1bGU7XG59KSgpO1xudmFyIFVzZXJEYXRhTW9kdWxlID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBVc2VyRGF0YU1vZHVsZShsb2dnZXIsIGVtaXR0ZXIsIHJjZSkge1xuICAgICAgICB2YXIgX3RoaXMgPSB0aGlzO1xuICAgICAgICB0aGlzLmlzSW5pdGlhbGl6ZWQgPSBmYWxzZTtcbiAgICAgICAgdGhpcy53YXJuaW5nID0gJ01pc3NpbmcgZGF0YSBhY2Nlc3Mga2V5OiBjYWxsIHNldFRva2VuIHdpdGggYXBwcm9wcmlhdGUgcGFyYW1ldGVycyc7XG4gICAgICAgIHRoaXMuc3RvcmFnZSA9IHJlcXVpcmUoJ2xvY2FsLXN0b3JhZ2UnKTtcbiAgICAgICAgdGhpcy5sb2dnZXIgPSBsb2dnZXI7XG4gICAgICAgIHRoaXMuZW1pdHRlciA9IGVtaXR0ZXI7XG4gICAgICAgIHRoaXMucmNlID0gcmNlO1xuICAgICAgICB0aGlzLmluaXRLZXkoKTtcbiAgICAgICAgdGhpcy5lbWl0dGVyLm9uKCdkYXRhOnNhdmUnLCBmdW5jdGlvbiAoZGF0YSkgeyByZXR1cm4gX3RoaXMuc2F2ZShkYXRhKTsgfSk7XG4gICAgfVxuICAgIFVzZXJEYXRhTW9kdWxlLnByb3RvdHlwZS5sb2FkID0gZnVuY3Rpb24gKGNhbGxiYWNrKSB7XG4gICAgICAgIHZhciBfdGhpcyA9IHRoaXM7XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnR0VUIHVzZXIgZGF0YScpO1xuICAgICAgICBpZiAodGhpcy5pc0luaXRpYWxpemVkKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5yY2UuaXNBdmFpbGFibGUoKSkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnJjZS5pc0luaXRpYWxpemVkKCkpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yY2UucmV0cmlldmVVc2VyRGF0YSh7IG9iamVjdF9pZDogdGhpcy5rZXkgfSwgZnVuY3Rpb24gKG9iaikge1xuICAgICAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2sob2JqICYmIG9iai5kYXRhID8gb2JqLmRhdGEgOiBudWxsKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmVtaXR0ZXIub25jZSgncmNlOnJlYWR5JywgZnVuY3Rpb24gKCkgeyByZXR1cm4gX3RoaXMubG9hZChjYWxsYmFjayk7IH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgIHZhciBvYmogPSB0aGlzLnN0b3JhZ2UuZ2V0KHRoaXMua2V5KTtcbiAgICAgICAgICAgICAgICBjYWxsYmFjayhvYmogJiYgb2JqLmRhdGEgPyBvYmouZGF0YSA6IG51bGwpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5sb2dnZXIuZXJyb3IodGhpcy53YXJuaW5nKTtcbiAgICAgICAgICAgIGNhbGxiYWNrKG51bGwpO1xuICAgICAgICB9XG4gICAgfTtcbiAgICBVc2VyRGF0YU1vZHVsZS5wcm90b3R5cGUuc2F2ZSA9IGZ1bmN0aW9uIChkYXRhKSB7XG4gICAgICAgIHRoaXMubG9nZ2VyLmxvZygnU0VUIHVzZXIgZGF0YScsIGRhdGEpO1xuICAgICAgICBpZiAodHlwZW9mIGRhdGEgPT09ICd1bmRlZmluZWQnKSB7XG4gICAgICAgICAgICB0aGlzLmxvZ2dlci53YXJuKCdEYXRhIGlzIHVuZGVmaW5lZCwgc2VuZCBudWxsIGluc3RlYWQgaWYgeW91IHdhbnQgdG8gY2xlYXIgdXNlciBkYXRhJyk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMuaXNJbml0aWFsaXplZCkge1xuICAgICAgICAgICAgaWYgKHRoaXMucmNlLmlzSW5pdGlhbGl6ZWQoKSkge1xuICAgICAgICAgICAgICAgIHRoaXMucmNlLnNhdmVVc2VyRGF0YSh7IG9iamVjdF9pZDogdGhpcy5rZXksIGRhdGE6IGRhdGEgfSwgZnVuY3Rpb24gKCkgeyByZXR1cm47IH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zdG9yYWdlLnNldCh0aGlzLmtleSwgeyBvYmplY3RfaWQ6IHRoaXMua2V5LCBkYXRhOiBkYXRhIH0pO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5sb2dnZXIuZXJyb3IodGhpcy53YXJuaW5nKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgIH07XG4gICAgVXNlckRhdGFNb2R1bGUucHJvdG90eXBlLmluaXRLZXkgPSBmdW5jdGlvbiAoKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBpZiAoczkgJiYgczkuaW5pdGlhbFBhcmFtcyAmJiBzOS5pbml0aWFsUGFyYW1zLmNvbmZpZ0ZpbGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmtleSA9IHM5LmluaXRpYWxQYXJhbXMuY29uZmlnRmlsZS5zcGxpdCgnLycpLnBvcCgpLnJlcGxhY2UoJy5qc29uJywgJycpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5rZXkgPSBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKCdoZWFkIHRpdGxlJykuaW5uZXJIVE1MO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgIHRoaXMubG9nZ2VyLmVycm9yKCdXaWRnZXRzQ29tbW9uJywgJ1VzZXJEYXRhTW9kdWxlJywgJ1VuYWJsZSB0byBzZXQgdXNlciBkYXRhIHBlcnNpc3RlbmNlIGtleScpO1xuICAgICAgICAgICAgdGhpcy5rZXkgPSAndW5pdC10ZXN0LWtleSc7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5rZXkgPSAndXNlcmRhdGE6JyArIHRoaXMua2V5O1xuICAgICAgICB0aGlzLmlzSW5pdGlhbGl6ZWQgPSB0cnVlO1xuICAgIH07XG4gICAgcmV0dXJuIFVzZXJEYXRhTW9kdWxlO1xufSkoKTtcbnZhciBXaWRnZXREYXRhTW9kdWxlID0gKGZ1bmN0aW9uICgpIHtcbiAgICBmdW5jdGlvbiBXaWRnZXREYXRhTW9kdWxlKGxvZ2dlcikge1xuICAgICAgICB0aGlzLndpZGdldERhdGFQYXRoID0gJy4uLy4uL3dpZGdldF9kYXRhLyc7XG4gICAgICAgIHRoaXMubG9nZ2VyID0gbG9nZ2VyO1xuICAgIH1cbiAgICBXaWRnZXREYXRhTW9kdWxlLnByb3RvdHlwZS5zZXRXaWRnZXREYXRhUGF0aCA9IGZ1bmN0aW9uIChwYXRoKSB7XG4gICAgICAgIHRoaXMud2lkZ2V0RGF0YVBhdGggPSBwYXRoO1xuICAgIH07XG4gICAgV2lkZ2V0RGF0YU1vZHVsZS5wcm90b3R5cGUubG9hZCA9IGZ1bmN0aW9uICh1cmksIGNhbGxiYWNrLCBsb2FkQXNQbGFpblRleHQpIHtcbiAgICAgICAgaWYgKGxvYWRBc1BsYWluVGV4dCA9PT0gdm9pZCAwKSB7IGxvYWRBc1BsYWluVGV4dCA9IGZhbHNlOyB9XG4gICAgICAgIHZhciByZXF1ZXN0ID0gbmV3IFhNTEh0dHBSZXF1ZXN0KCk7XG4gICAgICAgIHJlcXVlc3Qub3BlbignR0VUJywgdGhpcy53aWRnZXREYXRhUGF0aCArIHVyaSwgdHJ1ZSk7XG4gICAgICAgIHJlcXVlc3Qub25sb2FkID0gZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgaWYgKHJlcXVlc3Quc3RhdHVzIDwgNDAwICYmIHJlcXVlc3QucmVzcG9uc2VUZXh0KSB7XG4gICAgICAgICAgICAgICAgaWYgKGxvYWRBc1BsYWluVGV4dCkge1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhyZXF1ZXN0LnJlc3BvbnNlVGV4dCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB2YXIgcGFyc2VkSnNvbiA9IEpTT04ucGFyc2UocmVxdWVzdC5yZXNwb25zZVRleHQpO1xuICAgICAgICAgICAgICAgICAgICBjYWxsYmFjayhwYXJzZWRKc29uKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICBlbHNlIHtcbiAgICAgICAgICAgICAgICBjYWxsYmFjayhudWxsKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfTtcbiAgICAgICAgcmVxdWVzdC5vbmVycm9yID0gZnVuY3Rpb24gKGVycikgeyByZXR1cm4gY2FsbGJhY2sobnVsbCk7IH07XG4gICAgICAgIHJlcXVlc3Quc2VuZCgpO1xuICAgIH07XG4gICAgcmV0dXJuIFdpZGdldERhdGFNb2R1bGU7XG59KSgpO1xuLy8jIHNvdXJjZU1hcHBpbmdVUkw9aGFiLW10bC13aWRnZXRzLWNvbW1vbi5qcy5tYXAiXX0=
